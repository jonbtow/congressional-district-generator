package com.pumas.algorithm;

public enum CalculationMessageType {

    LOADING("LOADING"),
    UPDATE("UPDATE"),
    ERROR("ERROR"),
    SAVED("SAVED"),
    DONE("DONE");

    private final String name;

    private CalculationMessageType(String attributeName) {
        this.name = attributeName;
    }

    @Override
    public String toString() {
        return this.name;
    }

}
