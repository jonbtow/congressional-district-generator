package com.pumas.algorithm;

public enum CalculationRequestType {

    START("START"),
    RESUME("RESUME"),
    PAUSE("PAUSE"),
    STOP("STOP"),
    SAVE("SAVE");

    private final String name;

    private CalculationRequestType(String attributeName) {
        this.name = attributeName;
    }

    @Override
    public String toString() {
        return this.name;
    }


}
