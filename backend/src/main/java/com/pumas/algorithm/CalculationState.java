package com.pumas.algorithm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;

public class CalculationState {

    private Future<?> task;

    private final AtomicBoolean isPaused;

    private final AtomicBoolean saveRequested;

    private final CalculationUpdateMessage queuedMessages;

    private final String user;

    private SimpMessagingTemplate template;

    public static final int FLUSH_THRESHOLD = 5;

    private static final Logger LOGGER = LoggerFactory.getLogger(CalculationState.class);


    public CalculationState(String user, Future<?> task, AtomicBoolean isPaused, AtomicBoolean saveRequested) {
        this.task = task;
        this.isPaused = isPaused;
        this.saveRequested = saveRequested;
        this.queuedMessages = new CalculationUpdateMessage();
        this.user = user;
    }

    public Future<?> getTask() {
        return task;
    }

    public CalculationState setTask(Future<?> task) {
        this.task = task;
        return this;
    }

    public AtomicBoolean getIsPaused() {
        return isPaused;
    }

    public AtomicBoolean getSaveRequested() {
        return saveRequested;
    }

    public CalculationUpdateMessage getQueuedMessages() {
        return queuedMessages;
    }

    private <E> void sendMessageToUser(E message) {
        template.convertAndSendToUser(user,"/queue/calculations",  message);
    }

    public void flushCalculationMessages() {
        if(this.getQueuedMessages().getPrecinctGeoIdToDistrictFipsMap().isEmpty()) {
            return;
        }
        this.sendMessageToUser(this.getQueuedMessages());
        this.getQueuedMessages().getPrecinctGeoIdToDistrictFipsMap().clear();
    }

    public void sendMessage(CalculationUpdateMessage message) {
        if(!message.getMessageType().equals(CalculationMessageType.UPDATE)) {
            this.sendMessageToUser(message);
            return;
        }
        this.getQueuedMessages().setDistrictFipsToScoreMap(message.getDistrictFipsToScoreMap());
        this.getQueuedMessages().setOriginalDistrictFipsToScoreMap(message.getOriginalDistrictFipsToScoreMap());
        for(Map.Entry<String, String> entry : message.getPrecinctGeoIdToDistrictFipsMap().entrySet()) {
            this.getQueuedMessages().getPrecinctGeoIdToDistrictFipsMap().put(entry.getKey(), entry.getValue());
        }
        if(this.getQueuedMessages().getPrecinctGeoIdToDistrictFipsMap().size() > FLUSH_THRESHOLD) {
            this.flushCalculationMessages();
        }
    }

    public String getUser() {
        return user;
    }

    public void setMessageTemplate(SimpMessagingTemplate template) {
        this.template = template;
    }
}
