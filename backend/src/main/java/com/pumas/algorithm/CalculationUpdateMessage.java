package com.pumas.algorithm;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class CalculationUpdateMessage implements Serializable {

    private static final long serialVersionUID = -4248324933981187192L;

    private Map<String, String> precinctGeoIdToDistrictFipsMap;

    private CalculationMessageType messageType;

    private Map<String, Double> districtFipsToScoreMap;

    private Map<String, Double> originalDistrictFipsToScoreMap;

    public CalculationUpdateMessage() {
        this.districtFipsToScoreMap = new HashMap<>();
        this.precinctGeoIdToDistrictFipsMap = new HashMap<>();
        this.originalDistrictFipsToScoreMap = new HashMap<>();
        this.messageType = CalculationMessageType.UPDATE;
    }

    public CalculationUpdateMessage(CalculationMessageType messageType) {
        this();
        this.messageType = messageType;
    }

    public CalculationUpdateMessage(Map<String, String> precinctGeoIdToDistrictFipsMap) {
        this();
        this.precinctGeoIdToDistrictFipsMap = precinctGeoIdToDistrictFipsMap;
    }

    public Map<String, String> getPrecinctGeoIdToDistrictFipsMap() {
        return precinctGeoIdToDistrictFipsMap;
    }

    public CalculationUpdateMessage setPrecinctGeoIdToDistrictFipsMap(Map<String, String> precinctGeoIdToDistrictFipsMap) {
        this.precinctGeoIdToDistrictFipsMap = precinctGeoIdToDistrictFipsMap;
        return this;
    }

    public CalculationMessageType getMessageType() {
        return messageType;
    }

    public CalculationUpdateMessage setMessageType(CalculationMessageType messageType) {
        this.messageType = messageType;
        return this;
    }

    public Map<String, Double> getDistrictFipsToScoreMap() {
        return districtFipsToScoreMap;
    }

    public CalculationUpdateMessage setDistrictFipsToScoreMap(Map<String, Double> districtFipsToScoreMap) {
        this.districtFipsToScoreMap = districtFipsToScoreMap;
        return this;
    }

    public Map<String, Double> getOriginalDistrictFipsToScoreMap() {
        return originalDistrictFipsToScoreMap;
    }

    public CalculationUpdateMessage setOriginalDistrictFipsToScoreMap(Map<String, Double> originalDistrictFipsToScoreMap) {
        this.originalDistrictFipsToScoreMap = originalDistrictFipsToScoreMap;
        return this;
    }

}
