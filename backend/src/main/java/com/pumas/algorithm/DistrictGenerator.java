package com.pumas.algorithm;

import ch.qos.logback.classic.pattern.ClassNameOnlyAbbreviator;
import com.pumas.model.*;
import com.pumas.repositories.GeneralUserRepository;
import com.pumas.repositories.SavedMapRepository;
import com.pumas.repositories.StateRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Jaspreet on 4/8/2018.
 */
@Service
@EnableAsync
public class DistrictGenerator {

    private static final int INITIAL_MOVES = 0;
    private static final int THRESHOLD = 300;

    private static final Logger LOGGER = LoggerFactory.getLogger(DistrictGenerator.class);

    @Autowired
    private StateRepository stateRepository;

    @Autowired
    private GeneralUserRepository generalUserRepository;

    @Autowired
    private SavedMapRepository savedMapRepository;




//    @PersistenceContext(type = PersistenceContextType.EXTENDED)
//    private EntityManager entityManager;

    /**
     * Creates a new redistricting plan for a user specified state,
     * based on some user specified measures and constraints.
     */
    @Async
    public ListenableFuture<?> redistrict(UserPreferences userPreferences, CalculationState calculationState) {
        State currentSolution;              // redistricting currentSolution state
        CongressionalDistrict district;     // holds district
        Precinct randomPrecinct;            // precinct within district
        Precinct neighboringPrecinct = null;       // precinct to be absorbed into district (randomPrecinct's district)
        double solutionCost;                // cost value of the currentSolution state's redistricting
        double absorbCost;                  // the cost value after swapping precincts in the currentSolution state
        int totalMovesSinceSolutionChanged; // number of moves since the currentSolution has changed
        MoveCommand moveCmd;                // responsible for moving precincts in and out of district
        Map<String, Double> districtFipsToCostsMap = new HashMap<>();
        Map<String, Double> originalScores = new HashMap<>();
        AtomicInteger gradientTimeout = new AtomicInteger(10);
        long goodMoves = 0;

        totalMovesSinceSolutionChanged = INITIAL_MOVES;
        moveCmd = new MoveCommand();

        CalculationUpdateMessage loadingMessage = new CalculationUpdateMessage(CalculationMessageType.LOADING);
        calculationState.sendMessage(loadingMessage);
        currentSolution = initializeStateSolution(userPreferences);

        if(currentSolution == null) {
            CalculationUpdateMessage updateMessage = new CalculationUpdateMessage(CalculationMessageType.ERROR);
            calculationState.sendMessage(updateMessage);
            return new AsyncResult<>(0);
        }
        solutionCost = calculateCost(currentSolution, userPreferences, districtFipsToCostsMap);
        for(Map.Entry<String, Double> entry : districtFipsToCostsMap.entrySet()) {
            originalScores.put(entry.getKey(), entry.getValue());
        }

        currentSolution.setUnmovableEntities(userPreferences.getUnmovableDistricts(), userPreferences.getUnmovablePrecincts());

        LOGGER.info("Initial calculated solution cost: " + solutionCost);

        boolean firstIteration = true;

        while (totalMovesSinceSolutionChanged < THRESHOLD) {
            // check if this thread has been asked to die
            if(Thread.interrupted()) {
                return new AsyncResult<>(0);
            }

            pauseAlgorithmIfRequested(currentSolution, calculationState, solutionCost);
            saveStateIfRequested(currentSolution, calculationState, solutionCost);

            if(userPreferences.getWeights().isEmpty()) {
                break;
            }

            if(totalMovesSinceSolutionChanged > 20) {
                calculationState.flushCalculationMessages();
            }

            List<Precinct> precinctPair = currentSolution.getMovableValidRandomBorderPrecinctPair(neighboringPrecinct, gradientTimeout);
            if(precinctPair.isEmpty()) {
                break;
            } else if (precinctPair.size() != 2) {
                LOGGER.error("PRECINCT PAIR SIZE IS " + precinctPair.size() + "!");
            }
            randomPrecinct = precinctPair.get(0);
            neighboringPrecinct = precinctPair.get(1);
            district = randomPrecinct.getCongressionalDistrict();

            LOGGER.info("Executing move on neighboring precinct " + neighboringPrecinct.getGeoId());
            CongressionalDistrict losingDistrict = neighboringPrecinct.getCongressionalDistrict();
            moveCmd.executeMove(district, neighboringPrecinct);

            int totalPrecincts = 0;
            for(CongressionalDistrict district1 : currentSolution.getCongressionalDistricts()) {
                for(Precinct precinct : district1.getPrecincts()) {
                    totalPrecincts++;
                }
            }

            if (!district.satisfiesConstraints(userPreferences.getConstraints(), losingDistrict) || !losingDistrict.satisfiesConstraints(userPreferences.getConstraints(), district)) {
                LOGGER.info("Move did not satisfy constraints");
                moveCmd.undoMove();
                totalMovesSinceSolutionChanged++;

                neighboringPrecinct = null;
                gradientTimeout.set(10);
            } else {
                LOGGER.info("Calculating cost");
                absorbCost = calculateCost(currentSolution, userPreferences, districtFipsToCostsMap);
                LOGGER.info("Calculated cost: " + absorbCost + " CURRENT BEST SOLUTION COST: " + solutionCost);
                if (absorbCost >= solutionCost) {
                    /* Accept absorption */
                    solutionCost = absorbCost;
                    totalMovesSinceSolutionChanged = 0;

                    CalculationUpdateMessage updateMessage = new CalculationUpdateMessage(
                            Collections.singletonMap(neighboringPrecinct.getGeoId(), district.getFipsCode())
                    );
                    updateMessage.setDistrictFipsToScoreMap(districtFipsToCostsMap);
                    calculationState.sendMessage(updateMessage);
                    if(firstIteration) {
                        calculationState.flushCalculationMessages();
                        firstIteration = false;
                    }
                    gradientTimeout.set(gradientTimeout.get() - 1);

                    goodMoves++;
                    LOGGER.info("Accepting new absorption. Num good moves: " + goodMoves);
                } else {
                    /* Reject absorption */
                    moveCmd.undoMove();
                    totalMovesSinceSolutionChanged++;
                    LOGGER.info("Rejecting absorption");

                    neighboringPrecinct = null;
                    gradientTimeout.set(10);
                }
            }
            LOGGER.info("Total moves since last good move: " + totalMovesSinceSolutionChanged);
        }

        calculationState.flushCalculationMessages();

        CalculationUpdateMessage doneMessage = new CalculationUpdateMessage(CalculationMessageType.DONE);
        doneMessage.setOriginalDistrictFipsToScoreMap(originalScores);
        doneMessage.setDistrictFipsToScoreMap(districtFipsToCostsMap);
        calculationState.sendMessage(doneMessage);

        LOGGER.info("Saving COMPLETED saved map for user " + calculationState.getUser());
        this.saveStateMap(calculationState, currentSolution, solutionCost);
        LOGGER.info("Thread completed.");
        return new AsyncResult<>(69);
    }

    private void pauseAlgorithmIfRequested(State changedState, CalculationState calculationState, double goodnessScore) {
        while (calculationState.getIsPaused().get()) {
            Thread.yield();
            saveStateIfRequested(changedState, calculationState, goodnessScore);
        }
    }

    protected void saveStateIfRequested(State changedState, CalculationState calculationState, double goodnessScore) {
        if(calculationState.getSaveRequested().compareAndSet(true, false)) {
            this.saveStateMap(calculationState, changedState, goodnessScore);
        }
    }

    private void saveStateMap(CalculationState calculationState, State changedState, double goodnessScore) {
        LOGGER.info("Saving map for user " + calculationState.getUser() + " for state " + changedState.getName() + " year " + changedState.getYear());
        SavedMap savedMap = new SavedMap(changedState)
                .setState(changedState)
                .setGeneralUser(generalUserRepository.findByEmail(calculationState.getUser()))
                .setGoodnessScore(goodnessScore);
        savedMapRepository.save(savedMap);
        calculationState.sendMessage(new CalculationUpdateMessage(CalculationMessageType.SAVED));
    }



    /**
     * Calculates the cost of a state's redistricting plan
     * based on user specified measures.
     *
     * C(s) = w1*state_measure_1_Score + w2*state_measure_2_score + ... + wn*state_measure_n_score
     *
     * @param s
     * @return
     */
    public double calculateCost(State s, UserPreferences userPreferences, Map<String, Double> districtToScoresMap) {
        districtToScoresMap.clear();
        Map<Measure, Double> weights = userPreferences.getWeights();
        Set<Measure> availableMeasures = weights.keySet();
        Map<String, Map<Measure, Double>> scoresByDistrict = s.getMeasureScores(availableMeasures);
        Map<Measure, Double> stateMeasureScores = new HashMap<>(); // accumulation of measure scores for each district

        // initialize stateMeasureScores map
        for (Measure m : availableMeasures) {
            stateMeasureScores.put(m, 0.0);
        }

        List<Map<Measure, Double>> measureScoresByDistrict = new ArrayList<>(scoresByDistrict.size());
        for(Map.Entry<String, Map<Measure, Double>> entry : scoresByDistrict.entrySet()) {
            measureScoresByDistrict.add(entry.getValue());

            double districtCost = 0.0;
            for(Measure m : availableMeasures) {
                districtCost = weights.get(m) * entry.getValue().get(m);
            }
            districtToScoresMap.put(entry.getKey(), districtCost);
        }

        // vector addition to accumulate statewide measure scores:
        // eg.g [measure1: 10.2, measure2: 12.3] + [stateMeasure1: 3.3, stateMeasure2: 4.5]
        for (Map<Measure, Double> districtMeasureScores : measureScoresByDistrict) {
            districtMeasureScores.forEach((k, v) -> stateMeasureScores.merge(k, v, Double::sum));
        }

        // dot product:
        // [weights vector] . [stateMeasureScores]
        double cost = 0.0;
        for (Measure m : availableMeasures) {
            cost += weights.get(m) * stateMeasureScores.get(m);
        }
        return cost;
    }

    /**
     * Returns a clone copy of a user chosen state to be used
     * in the redistrict method.
     *
     * @return initialSolution
     */
    protected State initializeStateSolution(UserPreferences userPreferences) {
        String stateName = userPreferences.getStateName();
        int year = userPreferences.getYear();
        State initialSolution = stateRepository.findByNameAndYear(stateName, year);
        if(initialSolution == null) {
            return null;
        }
        return initialSolution.clone();
    }

    /**
     *  Inner class for handling moving of precincts to districts
     */
    class MoveCommand {
        CongressionalDistrict prevDistrict;
        Precinct prevNeighborPrecinct;


        private void executeMove(CongressionalDistrict cd, Precinct neighborPrecinct) {
            cd.addPrecinct(neighborPrecinct);
            prevDistrict = neighborPrecinct.getCongressionalDistrict();
            prevNeighborPrecinct = neighborPrecinct;
            cd.addPrecinct(prevNeighborPrecinct);
        }

        private void undoMove() {
            prevDistrict.addPrecinct(prevNeighborPrecinct);
        }
    }

    @Bean
    public TaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(5);
        executor.setMaxPoolSize(10);
        executor.setQueueCapacity(25);

        return executor;
    }

}