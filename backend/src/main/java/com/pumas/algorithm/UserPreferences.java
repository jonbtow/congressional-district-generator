package com.pumas.algorithm;

import com.pumas.model.Constraint;
import com.pumas.model.Measure;

import java.util.*;

/**
 * Created by Jaspreet on 4/8/2018.
 */
public class UserPreferences {

    private String stateName;

    private Set<Constraint> constraints;

    private Map<Measure, Double> weights;

    private Set<String> unmovablePrecincts;
    private Set<String> unmovableDistricts;

    private int year;

    public UserPreferences(String stateName) {
        this.stateName = stateName;
        this.constraints = new HashSet<>();
        this.weights = new HashMap<>();
        this.unmovablePrecincts = new HashSet<>();
        this.unmovableDistricts = new HashSet<>();
    }

    public UserPreferences addConstraint(Constraint constraint, boolean useConstraint) {
        if(!useConstraint) {
            return this;
        }
        this.constraints.add(constraint);
        return this;
    }

    public UserPreferences addWeight(Measure measure, double weight) {
        if(weight == 0) {
            return this;
        }
        this.weights.put(measure, weight);
        return this;
    }

    public String getStateName() {
        return stateName;
    }

    public UserPreferences setStateName(String stateName) {
        this.stateName = stateName;
        return this;
    }

    public Set<Constraint> getConstraints() {
        return constraints;
    }

    public UserPreferences setConstraints(Set<Constraint> constraints) {
        this.constraints = constraints;
        return this;
    }

    public Map<Measure, Double> getWeights() {
        return weights;
    }

    public UserPreferences setWeights(Map<Measure, Double> weights) {
        this.weights = weights;
        return this;
    }

    public Set<String> getUnmovablePrecincts() {
        return unmovablePrecincts;
    }

    public UserPreferences setUnmovablePrecincts(Set<String> unmovablePrecincts) {
        this.unmovablePrecincts = unmovablePrecincts;
        return this;
    }

    public Set<String> getUnmovableDistricts() {
        return unmovableDistricts;
    }

    public UserPreferences setUnmovableDistricts(Set<String> unmovableDistricts) {
        this.unmovableDistricts = unmovableDistricts;
        return this;
    }

    public int getYear() {
        return year;
    }

    public UserPreferences setYear(int year) {
        this.year = year;
        return this;
    }
}
