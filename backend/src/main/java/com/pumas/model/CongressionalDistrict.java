package com.pumas.model;


import com.pumas.model.converters.SimpleFeatureImplToStringConverter;
import com.vividsolutions.jts.geom.*;
import com.vividsolutions.jts.operation.valid.IsValidOp;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.geotools.feature.simple.SimpleFeatureImpl;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.referencing.operation.DefaultOperationMethod;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Jaspreet on 4/8/2018.
 */
@Entity
@Table(name = "CongressionalDistrict")
public class CongressionalDistrict implements GeographicEntity, Cloneable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String fipsCode;

    private String geoId;

    private String name;

    private String congressionalSession;

    private double area;

    private boolean isMovable;

    private double originalRacialScore=-1;

    private double originalDemScore=-1;

    private double originalRepScore=-1;

    private double originalIndScore=-1;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "election_fk")
    private Election election;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "demographic_fk")
    private Demographic demographic;

    @Lob
    @Convert(converter = SimpleFeatureImplToStringConverter.class)
    private SimpleFeatureImpl boundary;

    @Transient
    private Map<Measure, Double> cachedMeasureScores;

    @Transient
    private DefaultFeatureCollection featureCollection;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_state_id")
    private State state;

    @OneToMany(mappedBy = "congressionalDistrict", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<Precinct> precincts;

    @OneToMany(mappedBy = "owner", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<DistrictNeighbor> districtNeighbors;

    private static final Logger LOGGER = LoggerFactory.getLogger(CongressionalDistrict.class);

    public CongressionalDistrict() {
        this.cachedMeasureScores = new HashMap<>();
        this.precincts = new HashSet<>();
        this.districtNeighbors = new HashSet<>();
        this.isMovable = true;
        this.area = 0.0;
        this.originalRacialScore = 0;
        this.election = new Election();
        this.demographic = new Demographic();
    }

    public CongressionalDistrict(SimpleFeatureImpl districtFeature) {
        this();

        this.fipsCode = (String) districtFeature.getAttribute(GeoJsonAttribute.DISTRICT_FIPS_CODE.toString());
        this.name = (String) districtFeature.getAttribute(GeoJsonAttribute.DISTRICT_NAME.toString());
        this.congressionalSession = (String) districtFeature.getAttribute(GeoJsonAttribute.DISTRICT_SESSION.toString());
        this.geoId = (String) districtFeature.getAttribute(GeoJsonAttribute.GEO_ID.toString());
        this.boundary = districtFeature;
    }

    public Precinct getRandomBorderPrecinct() {
        List<Precinct> borderPrecincts = this.getBorderPrecincts();

        if(borderPrecincts.isEmpty()) {
            return null;
        }
        int randomIndex = new Random().nextInt(borderPrecincts.size());

        return borderPrecincts.get(randomIndex);
    }

    public Precinct getRandomMovableBorderPrecinct() {
        List<Precinct> borderPrecincts = this.getMovableBorderPrecincts();

        if(borderPrecincts.isEmpty()) {
            return null;
        }
        int randomIndex = new Random().nextInt(borderPrecincts.size());

        return borderPrecincts.get(randomIndex);
    }

    public List<Precinct> getBorderPrecincts() {
        return this.getPrecincts().stream()
                                  .filter(Precinct::isBorderPrecinct)
                                  .collect(Collectors.toList());
    }

    public List<Precinct> getMovableBorderPrecincts() {
        return this.getPrecincts().stream()
                .filter(precinct -> precinct.isMovable() && precinct.isBorderPrecinct())
                .collect(Collectors.toList());
    }

    public boolean satisfiesConstraints(Set<Constraint> constraints, CongressionalDistrict otherDistrict) {
        boolean isSatisfied = true;

        for(Constraint constraint : constraints) {
            switch (constraint) {
                case PRESERVE_POLITICAL_COMMUNITY:
                    isSatisfied = this.isPoliticalCommunityPreserved();
                    break;
                case CONTIGUITY:
                    isSatisfied = this.isContiguous(otherDistrict);
                    break;
                /*case RACIAL_FAIRNESS:
                    isSatisfied = this.isRaciallyFair();
                    break;*/

                case EQUAL_POPULATION:
                    isSatisfied = this.isPopulationEqual();
                    break;
                default:
                    return false;
            }

            if (!isSatisfied) {
                return false;
            }
        }

        return isSatisfied;
    }

    public Map<Measure, Double> getMeasureScores(Set<Measure> measures) {
        Map<Measure, Double> scores = new HashMap<>(measures.size());

        for(Measure measure : measures) {
            switch (measure) {
                /*case EQUAL_POPULATION:
                    scores.put(measure, this.measureEqualPopulation());
                    break;*/
                case RACIAL_FAIRNESS:
                    scores.put(measure, this.measureRacialFairness());
                    break;
                case SCHWARTZBERG_COMPACTNESS:
                    scores.put(measure, this.measureSchwarzbergCompactness());
                    break;
                case REOCK_COMPACTNESS:
                    scores.put(measure, this.measureReockCompactness());
                    break;
                case POLSBY_POPPER_COMPACTNESS:
                    scores.put(measure, this.measurePolsbyPopperCompactness());
                    break;
                case CONVEX_HULL_COMPACTNESS:
                    scores.put(measure, this.measureConvexHullCompactness());
                    break;
                case EFFICIENCY_GAP:
                    scores.put(measure, this.measureEfficiencyGap());
                    break;
                case PARTISAN_FAIRNESS:
                    scores.put(measure, this.measurePartisanFairness());
                    break;
                default:
                    scores.put(measure, 0.0);
            }
        }

        return scores;
    }

    public DefaultFeatureCollection getFeatureCollection() {
        if(featureCollection == null) {
            featureCollection = new DefaultFeatureCollection();
            for(Precinct precinct : this.getPrecincts()) {
                featureCollection.add(precinct.getBoundary());
            }
        }
        return featureCollection;
    }

    //NB MAY HAVE TO MODIFY HOW WE CALL THE METRIC METHODS

    private double measureReockCompactness() {
        FeatureCollection featureCollection = getFeatureCollection();

        double max_lon = featureCollection.getBounds().getMaximum(0);
        double min_lon = featureCollection.getBounds().getMinimum(0);
        double max_lat = featureCollection.getBounds().getMaximum(1);
        double min_lat = featureCollection.getBounds().getMinimum(1);

        double lon_length = this.getActualCoordinateDistance(max_lon, 0, min_lon, 0);
        double lat_length = this.getActualCoordinateDistance(0, max_lat, 0, min_lat);

        double radius = Math.max(lon_length, lat_length) / 2;
        double circleArea = Math.PI * radius * radius;

        double districtArea = this.getArea();

        LOGGER.info("REOCK: circle area: " + circleArea);
        LOGGER.info("REOCK district area: " + districtArea);
        LOGGER.info("REOCK COMPACTNESS: " + (districtArea/circleArea));

        return districtArea / circleArea;
    }

    private double measurePolsbyPopperCompactness() {
        //DistrictInfoGetter infoGetter = new DistrictInfoGetter();

        double area = getArea();
        double perimeter = getPerimeter();

        double polsbyPopperScore = (4 * Math.PI * area) / Math.pow(perimeter, 2);

        LOGGER.info("POLSBY POPPER SCORE: " + polsbyPopperScore);
        return polsbyPopperScore;
    }

    private double measureConvexHullCompactness() {
        return 0;
    }

    private double measureSchwarzbergCompactness() {
        //DistrictInfoGetter infoGetter = new DistrictInfoGetter();

        double area = getArea();
        double perimeter = getPerimeter();

        // equalAreaRadius = r = sqrt(A/PI)
        double r = Math.sqrt(area/Math.PI);
        // equalAreaPerimeter = C = 2pi * r (Circumference)
        double equalAreaPerimeter = 2 * Math.PI * r;
        // Schwartzberg score = 1 / (Perimeter of district / C )
        double score = 1 / (perimeter/equalAreaPerimeter);

        LOGGER.info("SHWARZBERG COMPACTNESS: " + score);
        return score;
    }

    private double measureEfficiencyGap(){

        return 0;
    }

    private boolean isPopulationEqual() {
        double pop = getPopulation();
        double avgDistrictPop = this.getState().getDistrictPopulationAverage();
        double districtPopStd = this.getState().getDistrictPopulationStd();

        double zStdScore = Math.abs((pop - avgDistrictPop) / districtPopStd);
        double score;

        if(zStdScore>=2)
            score = 0;
        else if(zStdScore>=1)
            score = 1;
        else if(zStdScore>=0.5)
            score = 2;
        else if(zStdScore>= 0)
            score = 5;
        else
            score = 0;

        //score /= 5;

        return score>=2;
    }


    private double measurePartisanFairness() {
        double demTotal = this.getElection().getDemocratCount();
        double repTotal= this.getElection().getRepublicanCount();
        double demScore, repScore;

        int size = getPrecincts().size();
        demScore = demTotal/size;
        repScore = repTotal/size;

        double score = 0;
        // TODO check if still correct, removed Indepenedent from total

        score += demScore>=(0.66*originalDemScore)?1:0;
        score += repScore>=(0.66*originalRepScore)?1:0;

        return score/2;
    }

    //Original Racial Fairness Score are calculated prior.
    private double measureRacialFairness() {
        double totalPopulation = getPopulation();
        double totalMinority = 0;
        double totalWhite = 0;


        totalWhite = getDemographic().getWhitePopulation();

        totalMinority = totalPopulation>=totalWhite?totalPopulation - totalWhite: 0;

        double minorityPercentage = totalMinority / totalPopulation;
        double whitePercentage = 1 - minorityPercentage;


        return minorityPercentage>=0.5?1:minorityPercentage;
    }

    private boolean isPoliticalCommunityPreserved() {
        return false;
    }

    public long getId() {
        return id;
    }

    public List<CongressionalDistrict> getNeighborsList() {
        List<CongressionalDistrict> neighbors = new ArrayList<>(this.getDistrictNeighbors().size());
        for(DistrictNeighbor districtNeighbor : this.getDistrictNeighbors()) {
            neighbors.add(districtNeighbor.getNeighbor());
        }
        return neighbors;
    }


    public Set<CongressionalDistrict> getNeighborsSet() {
        Set<CongressionalDistrict> neighbors = new HashSet<>(this.getDistrictNeighbors().size());
        for(DistrictNeighbor districtNeighbor : this.getDistrictNeighbors()) {
            neighbors.add(districtNeighbor.getNeighbor());
        }
        return neighbors;
    }

    public void addNeighbor(CongressionalDistrict neighbor) {
        this.getDistrictNeighbors().add(new DistrictNeighbor().setOwner(this).setNeighbor(neighbor));
    }

    public void addNeighbors(String[] neighborFipsCodes, Map<String, CongressionalDistrict> fipsToDistrictMap) {
        for(String neighborFipsCode : neighborFipsCodes) {
            // if we can get the district from the map, let's avoid an exhaustive search
            if(fipsToDistrictMap != null && fipsToDistrictMap.get(neighborFipsCode) != null) {
                this.addNeighbor(fipsToDistrictMap.get(neighborFipsCode));
            } else {
                // can't find the neighbor from the map (if even given),
                // so search the owning state for the corresponding district
                for(CongressionalDistrict district : this.getState().getCongressionalDistricts()) {
                    if(district.getFipsCode().equals(neighborFipsCode)) {
                        this.addNeighbor(district);
                        break;
                    }
                }
            }
        }
    }

    public CongressionalDistrict addPrecinct(Precinct precinct) {
        CongressionalDistrict oldDistrict = precinct.getCongressionalDistrict();
        if(oldDistrict == null) {
            LOGGER.warn("oldDistrict is null for precinct " + precinct.toString());
        }


        if(oldDistrict != null) {
            oldDistrict.removePrecinct(precinct);
        }
        precinct.setCongressionalDistrict(this);

        this.getPrecincts().add(precinct);
        this.setArea(this.getArea() + precinct.getArea());

        this.getDemographic().addDemographicData(precinct.getDemographic());
        this.getElection().addElectionData(precinct.getElection());

        for(PrecinctNeighbor precinctNeighbor: precinct.getPrecinctNeighbors()) {
            Precinct neighboringPrecinct = precinctNeighbor.getNeighbor();
            neighboringPrecinct.updateBorderPrecinctStatus();
        }

        this.getFeatureCollection().add(precinct.getBoundary());

        return this;
    }

    public CongressionalDistrict removePrecinct(Precinct precinct) {
        if(!this.getPrecincts().remove(precinct)) {
            LOGGER.warn("Attempt was made to remove precinct not currently in the district.");
        }
        this.setArea(this.getArea() - precinct.getArea());

        this.getDemographic().subtractDemographicData(precinct.getDemographic());
        this.getElection().subtractElectionData(precinct.getElection());
        this.getFeatureCollection().remove(precinct.getBoundary());

        return this;
    }

    public Precinct addPrecinct(SimpleFeatureImpl precinctFeature) {
        Precinct precinct = new Precinct(precinctFeature);
        this.addPrecinct(precinct);
        return precinct;
    }

    private void addPrecinctNeighbors(Map<String, Precinct> geoIdToPrecinctsMap, FeatureCollection districtsFeatureCollection) {
        try (FeatureIterator<SimpleFeatureImpl> precinctsFeaturesIterator = districtsFeatureCollection.features()) {

            while (precinctsFeaturesIterator.hasNext()) {
                SimpleFeatureImpl precinctFeature = precinctsFeaturesIterator.next();

                String precinctGeoId = (String) precinctFeature.getAttribute(GeoJsonAttribute.GEO_ID.toString());
                String[] neighbors = ((String) precinctFeature.getAttribute(GeoJsonAttribute.NEIGHBORS.toString())).split(",");

                Precinct precinct = geoIdToPrecinctsMap.get(precinctGeoId);
                precinct.addNeighbors(neighbors, geoIdToPrecinctsMap);

            }

        }
    }

    public CongressionalDistrict setId(long id) {
        this.id = id;
        return this;
    }

    public Map<Measure, Double> getCachedMeasureScores() {
        return cachedMeasureScores;
    }

    public CongressionalDistrict setCachedMeasureScores(Map<Measure, Double> cachedMeasureScores) {
        this.cachedMeasureScores = cachedMeasureScores;
        return this;
    }

    public State getState() {
        return state;
    }

    public CongressionalDistrict setState(State state) {
        this.state = state;
        return this;
    }

    public Set<Precinct> getPrecincts() {
        return precincts;
    }

    public CongressionalDistrict setPrecincts(Set<Precinct> precincts) {
        this.precincts = precincts;
        return this;
    }

    public CongressionalDistrict setDistrictNeighbors(Set<DistrictNeighbor> districtNeighbors) {
        this.districtNeighbors = districtNeighbors;
        return this;
    }

    public SimpleFeatureImpl getBoundary() {
        return boundary;
    }

    public CongressionalDistrict setBoundary(SimpleFeatureImpl boundary) {
        this.boundary = boundary;
        return this;
    }

    public boolean isMovable() {
        return isMovable;
    }

    public CongressionalDistrict setMovable(boolean movable) {
        isMovable = movable;
        return this;
    }

    public String getFipsCode() {
        return fipsCode;
    }

    public CongressionalDistrict setFipsCode(String fipsCode) {
        this.fipsCode = fipsCode;
        return this;
    }

    public String getName() {
        return name;
    }

    public CongressionalDistrict setName(String name) {
        this.name = name;
        return this;
    }

    public String getCongressionalSession() {
        return congressionalSession;
    }

    public CongressionalDistrict setCongressionalSession(String congressionalSession) {
        this.congressionalSession = congressionalSession;
        return this;
    }

    public double getArea() {
        return this.area;
    }

    public CongressionalDistrict setArea(double area) {
        this.area = area;
        return this;
    }


    public double getPopulation(){
        return this.getDemographic().getTotalPopulation();
    }

    public boolean isContiguous(CongressionalDistrict otherDistrict) {

        List<Precinct> otherPrecincts = new ArrayList<>(otherDistrict.getPrecincts());
        List<Geometry> otherPrecinctsGeoList = this.getAllPrecinctGeometriesList(otherPrecincts);

        //Geometry otherGeo = this.combinePrecinctsIntoDistrict(otherPrecinctsGeoList);


        /*List<Precinct> currentPrecincts = new ArrayList<>(this.getPrecincts());
        List<Geometry> currentPrecinctsGeoList = this.getAllPrecinctGeometriesList(currentPrecincts);
        Geometry currentGeo = this.combinePrecinctsIntoDistrict(currentPrecinctsGeoList);*/

        /*for(Geometry geo: otherPrecinctsGeoList){
            if(currentGeo.contains(geo) || otherGeo.intersects(geo)){
                return false;
            }
        }


        for(Geometry geo: currentPrecinctsGeoList){
            if(otherGeo.contains(geo) || otherGeo.intersects(geo) || otherGeo.inte){
                return false;
            }
        }*/


        ListIterator<Geometry> iterator = otherPrecinctsGeoList.listIterator();
        Geometry finalGeo = iterator.next();
        finalGeo = finalGeo.buffer(0.0005);
        iterator.remove();
        int lastSize = 0;
        boolean noMovesMade = true;

        while(otherPrecinctsGeoList.size()>0) {
            while (iterator.hasNext()) {
                Geometry b = iterator.next();
                b = b.buffer(0.0005);
                try {
                    if (finalGeo.intersects(b) || finalGeo.touches(b) || !finalGeo.disjoint(b)) {
                        finalGeo = finalGeo.union(b);
                        iterator.remove();
                        noMovesMade = false;
                    }
                }
                catch(TopologyException ex){
                    IsValidOp isValidOp = new IsValidOp(b);
                    noMovesMade = false;
                    System.err.println("Geometry is invalid: " + isValidOp.
                            getValidationError());
                    finalGeo = finalGeo.union(b);
                    iterator.remove();
                    //break;
                }
            }

            if(noMovesMade){
                return false;
                //break;
            }
            noMovesMade = true;
            lastSize = otherPrecinctsGeoList.size();
            if(otherPrecinctsGeoList.size()>0) {
                iterator = otherPrecinctsGeoList.listIterator(0);
            }
        }


            return true;

        /*for(Precinct precinct : this.getPrecincts()) {
            precinct.markAsUnVisited();
        }

        for(Precinct precinct : this.getPrecincts()) {
            if(precinct.isVisited()) {
                continue;
            }
            bfs(precinct);
        }

        boolean isContiguous = true;

        for(Precinct precinct : this.getPrecincts()) {
            if(!precinct.isVisited()) {
                isContiguous = false;
                break;
            }
        }

        LOGGER.info("Districts' contiguity was " + isContiguous);
        return isContiguous;*/
    }

    public void bfs(Precinct startingPrecinct) {
        Queue<Precinct> precinctQueue = new ArrayDeque<>();

        startingPrecinct.markAsVisited();
        precinctQueue.add(startingPrecinct);

        while(!precinctQueue.isEmpty()) {
            Precinct precinct = precinctQueue.remove();

            for(PrecinctNeighbor precinctNeighbor : precinct.getPrecinctNeighbors()) {
                Precinct neighbor = precinctNeighbor.getNeighbor();

                // skip neighbors from different districts
                if(!neighbor.getCongressionalDistrict().equals(startingPrecinct.getCongressionalDistrict().getGeoId())) {
                    continue;
                }

                if(!neighbor.isVisited()) {
                    neighbor.markAsVisited();
                    precinctQueue.add(neighbor);
                }
            }
        }
    }

    public void updateDemographics() {
        this.setDemographic(new Demographic());
        for(Precinct precinct : this.getPrecincts()) {
            this.getDemographic().addDemographicData(precinct.getDemographic());
        }
    }

    public void setOriginalRacialScore(){
        double totalPopulation = getPopulation();
        double totalMinority = 0;
        double totalWhite = 0;

        totalWhite = getDemographic().getWhitePopulation();

        totalMinority = totalPopulation>=totalWhite?totalPopulation - totalWhite: 0;

        double minorityPercentage = totalMinority / totalPopulation;
        double whitePercentage = 1 - minorityPercentage;


        this.originalRacialScore = minorityPercentage>=0.5?0.5:minorityPercentage;
    }

    public void setOriginalPartisanFairnessScore(){
        double demTotal = this.getElection().getDemocratCount();
        double repTotal = this.getElection().getRepublicanCount();

        int size = getPrecincts().size();

        this.originalDemScore = demTotal/size;
        this.originalRepScore = repTotal/size;
    }

    /**
     * Calculate the Perimeter of a CongressionalDistrict object.
     * @return CongressionalDistrict kellyDistrict's perimeter.
     */
    public double getPerimeter(){
        CongressionalDistrict district = this;//.clone();

        Set<Precinct> precinctSet = district.getPrecincts();
        List<Precinct> precinctList = new ArrayList<>(precinctSet);

        List<Geometry>allPrecinctGeometriesList = getAllPrecinctGeometriesList(precinctList);

        Geometry finalGeo = combinePrecinctsIntoDistrict(allPrecinctGeometriesList);
        Coordinate coordinates[] = finalGeo.getCoordinates();

        int c_size = coordinates.length;
        double perimeter = 0;

        for(int index=0; index<c_size-1; index++) {
            double lon1 = coordinates[(index)].x;
            double lat1 = coordinates[(index)].y;
            double lon2 = coordinates[(index+1)].x;
            double lat2 = coordinates[(index+1)].y;

            perimeter += getActualCoordinateDistance(lon1, lat1, lon2, lat2);
        }

        return perimeter;
    }

    /**
     * Combine a List of Precinct Geometric entities into a single Geometric Entity.
     * @param geoList List of Precinct Geometric entities.
     * @return The Geometric entity encompassing all the Precincts' area.
     */
    private Geometry combinePrecinctsIntoDistrict(List<Geometry> geoList){

        GeometryFactory GEOMETRY_FACTORY = JTSFactoryFinder.getGeometryFactory();
        Geometry finalGeo;
        // GeometryCollection cannot be built from a single geometry, as buffer returns a Polygon
        // we gotta handle it separately to avoid a ClassCastException Polygon -> Geometry
        if (geoList.size() > 1) {
            GeometryCollection geometryCollection = (GeometryCollection) GEOMETRY_FACTORY.buildGeometry(geoList);
            finalGeo = geometryCollection.union();
        } else {
            finalGeo = geoList.get(0);
        }

        return finalGeo;
        /*ListIterator<Geometry> iterator = geoList.listIterator();

        Geometry finalGeo = iterator.next();
        iterator.remove();

        while(geoList.size()>0) {
            while (iterator.hasNext()) {
                Geometry b = iterator.next();
                try {
                    if (finalGeo.touches(b) || finalGeo.intersects(b)) {
                        finalGeo = finalGeo.union(b);
                        iterator.remove();
                    }
                }
                catch(TopologyException ex){
                    IsValidOp isValidOp = new IsValidOp(b);

                    System.err.println("Geometry is invalid: " + isValidOp.
                            getValidationError());
                    iterator.remove();
                    //break;
                }
            }
            iterator = geoList.listIterator();
        }*/
    }

    /**
     * Get all the input Precinct objects' geometric entities.
     * @param precinctList List of Precinct objects' whose Geometric entities are to be retrieved.
     * @return List of Geometric entities of the Precinct objects.
     */
    public List<Geometry> getAllPrecinctGeometriesList(List<Precinct> precinctList){
        List<Geometry> geometryList = new ArrayList<>();

        for(Precinct p : precinctList){
            //p = p.clone();
            Geometry g = (Geometry) p.getBoundary().getAttribute("geometry");
            g = g.buffer(0.00005);
            geometryList.add(g);
        }

        return geometryList;
    }

    /**
     * Calculates the actual distance in km between two coordinates.
     * @param lon1 Longitude of coordinate point 1
     * @param lat1 Latitude of coordinate point 1
     * @param lon2 Longitude of coordinate point 2
     * @param lat2 Latitude of coordinate point 2
     * @return Distance between coordinate (lon1, lat1) and coordinate (lon2, lat2)
     */
    private double getActualCoordinateDistance(double lon1, double lat1, double lon2, double lat2) {
        double R = 6371; // Radius of the earth in km
        double dLat = deg2rad(lat2-lat1);  // deg2rad below
        double dLon = deg2rad(lon2-lon1);
        double a =    Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
                        Math.sin(dLon/2) * Math.sin(dLon/2)
                ;
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double d = R * c; // Distance in km
        return d;
    }

    /**
     * Calculate the equivalent radian of a degree
     * @param deg Degree whose equivalent radian is to be calculated.
     * @return Equivalent radian of the degree.
     */
    private double deg2rad(double deg) {
        return deg * (Math.PI/180);
    }

    public Election getElection() {
        return election;
    }

    public CongressionalDistrict setElection(Election election) {
        this.election = election;
        return this;
    }

    public Demographic getDemographic() {
        return demographic;
    }

    public CongressionalDistrict setDemographic(Demographic demographic) {
        this.demographic = demographic;
        return this;
    }

    public Set<DistrictNeighbor> getDistrictNeighbors() {
        return districtNeighbors;
    }

    public String getGeoId() {
        return geoId;
    }

    public CongressionalDistrict setGeoId(String geoId) {
        this.geoId = geoId;
        return this;
    }

    @Override
    public CongressionalDistrict clone() {
        CongressionalDistrict congressionalDistrict = new CongressionalDistrict();
        congressionalDistrict.setDemographic(this.getDemographic().clone())
                .setElection(this.getElection().clone())
                .setArea(this.getArea())
                .setBoundary(this.getBoundary())
                .setCongressionalSession(this.getCongressionalSession())
                .setFipsCode(this.getFipsCode())
                .setName(this.getName())
                .setId(this.getId())
                .setMovable(this.isMovable())
                .setGeoId(this.getGeoId());

        Set<Precinct> clonedPrecincts = new HashSet<>(this.getPrecincts().size());
        for(Precinct origPrecinct : this.getPrecincts()) {
            Precinct prec = origPrecinct.clone();
            prec.setCongressionalDistrict(congressionalDistrict);
            clonedPrecincts.add(prec);
        }


        HashMap<Measure, Double> cachedMeasureScores = new HashMap<>();

        cachedMeasureScores.putAll(this.cachedMeasureScores);

        congressionalDistrict.setCachedMeasureScores(cachedMeasureScores)
                .setPrecincts(clonedPrecincts);

        return congressionalDistrict;
    }

}
