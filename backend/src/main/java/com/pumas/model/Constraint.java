package com.pumas.model;

/**
 * Created by Jaspreet on 4/8/2018.
 */
public enum Constraint {
    PRESERVE_POLITICAL_COMMUNITY,
    CONTIGUITY,
    EQUAL_POPULATION
}
