package com.pumas.model;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.util.Map;

/**
 * Created by Jaspreet on 4/8/2018.
 */
@Entity
@Table(name = "Demographic")
public class Demographic implements Cloneable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private int totalPopulation;

    private int whitePopulation;

    private int blackPopulation;

    private int asianPopulation;

    private int americanIndianPopulation;

    private int pacificIslanderPopulation;

    private int otherPopulation;

    private int twoOrMorePopulation;

    public Demographic() {
        this.totalPopulation = 0;
        this.whitePopulation = 0;
        this.blackPopulation = 0;
        this.asianPopulation = 0;
        this.americanIndianPopulation = 0;
        this.pacificIslanderPopulation = 0;
        this.otherPopulation = 0;
        this.twoOrMorePopulation = 0;
    }

    public long getId() {
        return id;
    }

    public Demographic setId(long id) {
        this.id = id;
        return this;
    }

    public int getTotalPopulation() {
        return totalPopulation;
    }

    public Demographic setTotalPopulation(int totalPopulation) {
        this.totalPopulation = totalPopulation;
        return this;
    }

    public Demographic addDemographicData(Demographic demographic) {
        this.setTotalPopulation(this.getTotalPopulation() + demographic.getTotalPopulation());
        this.setWhitePopulation(this.getWhitePopulation() + demographic.getWhitePopulation());
        this.setBlackPopulation(this.getBlackPopulation() + demographic.getBlackPopulation());
        this.setAsianPopulation(this.getAsianPopulation() + demographic.getAsianPopulation());
        this.setAmericanIndianPopulation(this.getAmericanIndianPopulation() + demographic.getAmericanIndianPopulation());
        this.setPacificIslanderPopulation(this.getPacificIslanderPopulation() + demographic.getPacificIslanderPopulation());
        this.setOtherPopulation(this.getOtherPopulation() + demographic.getOtherPopulation());
        this.setTwoOrMorePopulation(this.getTwoOrMorePopulation() + demographic.getTwoOrMorePopulation());
        return this;
    }

    public Demographic subtractDemographicData(Demographic demographic) {
        this.setTotalPopulation(this.getTotalPopulation() - demographic.getTotalPopulation());
        this.setWhitePopulation(this.getWhitePopulation() - demographic.getWhitePopulation());
        this.setBlackPopulation(this.getBlackPopulation() - demographic.getBlackPopulation());
        this.setAsianPopulation(this.getAsianPopulation() - demographic.getAsianPopulation());
        this.setAmericanIndianPopulation(this.getAmericanIndianPopulation() - demographic.getAmericanIndianPopulation());
        this.setPacificIslanderPopulation(this.getPacificIslanderPopulation() - demographic.getPacificIslanderPopulation());
        this.setOtherPopulation(this.getOtherPopulation() - demographic.getOtherPopulation());
        this.setTwoOrMorePopulation(this.getTwoOrMorePopulation() - demographic.getTwoOrMorePopulation());
        return this;
    }

    public int getWhitePopulation() {
        return whitePopulation;
    }

    public Demographic setWhitePopulation(int whitePopulation) {
        this.whitePopulation = whitePopulation;
        return this;
    }

    public int getBlackPopulation() {
        return blackPopulation;
    }

    public Demographic setBlackPopulation(int blackPopulation) {
        this.blackPopulation = blackPopulation;
        return this;
    }

    public int getAsianPopulation() {
        return asianPopulation;
    }

    public Demographic setAsianPopulation(int asianPopulation) {
        this.asianPopulation = asianPopulation;
        return this;
    }

    public int getAmericanIndianPopulation() {
        return americanIndianPopulation;
    }

    public Demographic setAmericanIndianPopulation(int americanIndianPopulation) {
        this.americanIndianPopulation = americanIndianPopulation;
        return this;
    }

    public int getPacificIslanderPopulation() {
        return pacificIslanderPopulation;
    }

    public Demographic setPacificIslanderPopulation(int pacificIslanderPopulation) {
        this.pacificIslanderPopulation = pacificIslanderPopulation;
        return this;
    }

    public int getOtherPopulation() {
        return otherPopulation;
    }

    public Demographic setOtherPopulation(int otherPopulation) {
        this.otherPopulation = otherPopulation;
        return this;
    }

    public int getTwoOrMorePopulation() {
        return twoOrMorePopulation;
    }

    public Demographic setTwoOrMorePopulation(int twoOrMorePopulation) {
        this.twoOrMorePopulation = twoOrMorePopulation;
        return this;
    }

    public void updateTotalPopulation() {
        this.setTotalPopulation(
                this.getAsianPopulation()
                + this.getAmericanIndianPopulation()
                + this.getBlackPopulation()
                + this.getOtherPopulation()
                + this.getPacificIslanderPopulation()
                + this.getTwoOrMorePopulation()
                + this.getWhitePopulation()
        );
    }

    @Override
    public Demographic clone() {
        Demographic demographic = new Demographic();

        demographic.setId(this.getId())
                   .setTotalPopulation(this.getTotalPopulation())
                    .setTwoOrMorePopulation(this.getTwoOrMorePopulation())
                    .setOtherPopulation(this.getOtherPopulation())
                    .setPacificIslanderPopulation(this.getPacificIslanderPopulation())
                    .setAmericanIndianPopulation(this.getAmericanIndianPopulation())
                    .setAsianPopulation(this.getAsianPopulation())
                    .setBlackPopulation(this.getBlackPopulation())
                    .setWhitePopulation(this.getWhitePopulation());

        return demographic;
    }

}
