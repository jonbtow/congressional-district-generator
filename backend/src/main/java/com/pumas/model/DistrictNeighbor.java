package com.pumas.model;

import javax.persistence.*;

@Entity
public class DistrictNeighbor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "owner_id", updatable = false, nullable = false)
    private CongressionalDistrict owner;

    @ManyToOne
    @JoinColumn(name = "neighbor_id", updatable = false, nullable = false)
    private CongressionalDistrict neighbor;

    public DistrictNeighbor(){}

    public DistrictNeighbor(CongressionalDistrict owner, CongressionalDistrict neighbor) {
        this();
        this.owner = owner;
        this.neighbor = neighbor;
    }

    public DistrictNeighbor(long id, CongressionalDistrict owner, CongressionalDistrict neighbor) {
        this(owner, neighbor);
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public DistrictNeighbor setId(long id) {
        this.id = id;
        return this;
    }

    public CongressionalDistrict getOwner() {
        return owner;
    }

    public DistrictNeighbor setOwner(CongressionalDistrict owner) {
        this.owner = owner;
        return this;
    }

    public CongressionalDistrict getNeighbor() {
        return neighbor;
    }

    public DistrictNeighbor setNeighbor(CongressionalDistrict neighbor) {
        this.neighbor = neighbor;
        return this;
    }
}
