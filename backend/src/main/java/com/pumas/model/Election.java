package com.pumas.model;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Jaspreet on 4/8/2018.
 */
@Entity
@Table(name = "Election")
public class Election implements Cloneable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private int republicanCount;

    private int democratCount;

    private int votingPopulation;

    public Election() {
        this.republicanCount = 0;
        this.democratCount = 0;
        this.votingPopulation = 0;
    }

    public long getId() {
        return id;
    }

    public Election setId(long id) {
        this.id = id;
        return this;
    }

    public int getRepublicanCount() {
        return republicanCount;
    }

    public Election setRepublicanCount(int republicanCount) {
        this.republicanCount = republicanCount;
        return this;
    }

    public int getDemocratCount() {
        return democratCount;
    }

    public Election setDemocratCount(int democratCount) {
        this.democratCount = democratCount;
        return this;
    }

    public int getVotingPopulation() {
        return votingPopulation;
    }

    public Election setVotingPopulation(int votingPopulation) {
        this.votingPopulation = votingPopulation;
        return this;
    }

    public Election addElectionData(Election election) {
        this.setVotingPopulation(this.getVotingPopulation() + election.getVotingPopulation());
        this.setDemocratCount(this.getDemocratCount() + election.getDemocratCount());
        this.setRepublicanCount(this.getRepublicanCount() + election.getRepublicanCount());
        return this;
    }

    public Election subtractElectionData(Election election) {
        this.setVotingPopulation(this.getVotingPopulation() - election.getVotingPopulation());
        this.setDemocratCount(this.getDemocratCount() - election.getDemocratCount());
        this.setRepublicanCount(this.getRepublicanCount() - election.getRepublicanCount());
        return this;
    }

    @Override
    public Election clone() {
        Election election = new Election();

        election.setId(this.getId())
                .setVotingPopulation(this.getVotingPopulation())
                .setDemocratCount(this.getDemocratCount())
                .setRepublicanCount(this.getRepublicanCount());

        return election;
    }
}
