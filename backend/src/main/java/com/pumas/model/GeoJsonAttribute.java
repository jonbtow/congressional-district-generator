package com.pumas.model;

public enum GeoJsonAttribute {

    STATE_FIPS_CODE("STATEFP"),
    STATE_NAME("NAME"),

    DISTRICT_FIPS_CODE("CDFP"),
    DISTRICT_NAME("NAMELSAD"),
    DISTRICT_SESSION("CDSESSN"),

    COUNTY_FIPS("COUNTYFP"),
    COUNTY_NAME("COUNTY"),
    VOTING_DISTRICT_CODE("VTDST"),
    PRECINCT_NAME("NAME"),
    TOTAL_POPULATION("POP"),

    VOTING_TOTAL("PRES_TOT"),
    REPUBLICAN_VOTING("PRES_D"),
    DEMOCRAT_VOTING("PRES_R"),


    GEO_ID("GEOID"),
    AREA("AREA"),
    PERIMETER("PERIMETER"),
    NEIGHBORS("NEIGHBORS"),
    GEOMETRY("geometry");

    private final String attributeName;

    private GeoJsonAttribute(String attributeName) {
        this.attributeName = attributeName;
    }

    @Override
    public String toString() {
        return this.attributeName;
    }

}
