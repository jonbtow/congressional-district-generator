package com.pumas.model;

/**
 * Created by Jaspreet on 4/8/2018.
 */
public enum Measure {
    SCHWARTZBERG_COMPACTNESS,
    REOCK_COMPACTNESS,
    POLSBY_POPPER_COMPACTNESS,
    CONVEX_HULL_COMPACTNESS,
    EFFICIENCY_GAP,
    PARTISAN_FAIRNESS,
    RACIAL_FAIRNESS
}
