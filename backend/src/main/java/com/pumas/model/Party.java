package com.pumas.model;

public enum Party {
    DEMOCRATIC, REPUBLICAN, INDEPENDENT, OTHER
}
