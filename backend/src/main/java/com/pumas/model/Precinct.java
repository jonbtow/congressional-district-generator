
package com.pumas.model;

import com.pumas.model.converters.SimpleFeatureImplToStringConverter;
import org.geotools.feature.simple.SimpleFeatureImpl;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.util.*;

/**
 * Created by Jaspreet on 4/8/2018.
 */
@Entity(name = "Precinct")
@Table(name = "Precinct")
public class Precinct implements GeographicEntity, Cloneable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private boolean isBorderPrecinct;

    private boolean isMovable;

    private double area;

    private double perimeter;

    private String votingDistrictCode;

    private String geoId;

    private String name;

    private String countyName;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinColumn(name = "fk_cong_district")
    private CongressionalDistrict congressionalDistrict;

    @OneToMany(mappedBy = "owner", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<PrecinctNeighbor> precinctNeighbors;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "election_fk")
    private Election election;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "demographic_fk")
    private Demographic demographic;

    @Lob
    @Convert(converter = SimpleFeatureImplToStringConverter.class)
    private SimpleFeatureImpl boundary;

    @Transient
    private static final Logger LOGGER = LoggerFactory.getLogger(Precinct.class);

    @Transient
    private boolean visited;

    public Precinct() {
        this.isBorderPrecinct = false;
        this.isMovable = true;
        this.precinctNeighbors = new HashSet<>();
        this.election = new Election();
        this.demographic = new Demographic();
        this.visited = false;
    }

    public Precinct(SimpleFeatureImpl precinctFeature) {
        this();

        this.votingDistrictCode = (String) precinctFeature.getAttribute(GeoJsonAttribute.VOTING_DISTRICT_CODE.toString());
        this.name = (String) precinctFeature.getAttribute(GeoJsonAttribute.PRECINCT_NAME.toString());
        this.area = (Double) precinctFeature.getAttribute(GeoJsonAttribute.AREA.toString());
        this.perimeter = (Double) precinctFeature.getAttribute(GeoJsonAttribute.PERIMETER.toString());
        this.geoId = (String) precinctFeature.getAttribute(GeoJsonAttribute.GEO_ID.toString());
        if(((String) precinctFeature.getAttribute(GeoJsonAttribute.STATE_FIPS_CODE.toString())).equals("42")) {
            this.countyName = pennCountyFipsToCountyNameMap.get(((String) precinctFeature.getAttribute(GeoJsonAttribute.COUNTY_FIPS.toString())));
        } else {
            this.countyName = (String) precinctFeature.getAttribute(GeoJsonAttribute.COUNTY_NAME.toString());
        }

        try {
            this.demographic.setTotalPopulation(new Integer((String) precinctFeature.getAttribute(GeoJsonAttribute.TOTAL_POPULATION.toString())));
        } catch (NumberFormatException e) {
            this.demographic.setTotalPopulation(0);
        }
        try {
            this.election.setRepublicanCount((int) Math.round(new Double((String) precinctFeature.getAttribute(GeoJsonAttribute.REPUBLICAN_VOTING.toString()))));
        } catch (NumberFormatException e) {
            this.election.setRepublicanCount(0);
        }
        try {
            this.election.setDemocratCount((int) Math.round(new Double((String) precinctFeature.getAttribute(GeoJsonAttribute.DEMOCRAT_VOTING.toString()))));
        } catch (NumberFormatException e) {
            this.election.setDemocratCount(0);
        }
        this.election.setVotingPopulation(this.election.getDemocratCount() + this.election.getRepublicanCount());
        this.boundary = precinctFeature;
    }

    public List<Precinct> getNeighbors() {
        List<Precinct> neighbors = new ArrayList<>(this.getPrecinctNeighbors().size());
        for(PrecinctNeighbor precinctNeighbor : this.getPrecinctNeighbors()) {
            neighbors.add(precinctNeighbor.getNeighbor());
        }
        return neighbors;
    }

    public void addNeighbor(Precinct neighbor) {
        this.getPrecinctNeighbors().add(new PrecinctNeighbor().setOwner(this).setNeighbor(neighbor));
    }

    public void addNeighbors(String[] neighborFipsCodes, Map<String, Precinct> fipsToPrecinctMap) {
        for(String neighborFipsCode : neighborFipsCodes) {
            // if we can get the district from the map, let's avoid an exhaustive search
            if(fipsToPrecinctMap != null && fipsToPrecinctMap.get(neighborFipsCode) != null) {
                this.addNeighbor(fipsToPrecinctMap.get(neighborFipsCode));
            } else {
                LOGGER.warn("Using fallback for neighbors");
                // can't find the neighbor from the map (if even given),
                // so search the owning district for the corresponding precinct
                boolean precinctFound = false;
                for (CongressionalDistrict district : this.getCongressionalDistrict().getState().getCongressionalDistricts()) {
                    for (Precinct precinct : district.getPrecincts()) {
                        if (district.getFipsCode().equals(neighborFipsCode)) {
                            this.addNeighbor(precinct);
                            precinctFound = true;
                            break;
                        }
                    }
                    if (precinctFound) {
                        break;
                    }
                }
            }
        }
    }

    public long getId() {
        return id;
    }

    public Precinct setId(long id) {
        this.id = id;
        return this;
    }

    public boolean isBorderPrecinct() {
        return isBorderPrecinct;
    }

    public Precinct setBorderPrecinct(boolean borderPrecinct) {
        isBorderPrecinct = borderPrecinct;
        return this;
    }

    public boolean isMovable() {
        return isMovable;
    }

    public Precinct setMovable(boolean movable) {
        isMovable = movable;
        return this;
    }

    public CongressionalDistrict getCongressionalDistrict() {
        return congressionalDistrict;
    }

    public Precinct setCongressionalDistrict(CongressionalDistrict congressionalDistrict) {
        this.congressionalDistrict = congressionalDistrict;
        return this;
    }

    public Precinct updateBorderPrecinctStatus() {
        for(PrecinctNeighbor precinctNeighbor : this.getPrecinctNeighbors()) {
            Precinct neighboringPrecinct = precinctNeighbor.getNeighbor();
            if(neighboringPrecinct.getCongressionalDistrict() != this.getCongressionalDistrict()) {
                this.setBorderPrecinct(true);
                return this;
            }
        }

        this.setBorderPrecinct(false);
        return this;
    }

    public Precinct getNeighboringPrecinctFromDifferentDistrict() {
        List<Precinct> differentNeighbors = new ArrayList<>();

        for(PrecinctNeighbor precinctNeighbor: this.getPrecinctNeighbors()) {
            Precinct neighboringPrecinct = precinctNeighbor.getNeighbor();
            if(neighboringPrecinct.getCongressionalDistrict() != this.getCongressionalDistrict()) {
                differentNeighbors.add(neighboringPrecinct);
            }
        }

        if(differentNeighbors.isEmpty()) {
            return null;
        }

        int randIndex = new Random().nextInt(differentNeighbors.size());

        return differentNeighbors.get(randIndex);
    }

    public Set<PrecinctNeighbor> getPrecinctNeighbors() {
        return precinctNeighbors;
    }

    public Precinct setPrecinctNeighbors(Set<PrecinctNeighbor> precinctNeighbors) {
        this.precinctNeighbors = precinctNeighbors;
        return this;
    }

    public Election getElection() {
        return election;
    }

    public Precinct setElection(Election election) {
        this.election = election;
        return this;
    }

    public Demographic getDemographic() {
        return demographic;
    }

    public Precinct setDemographic(Demographic demographic) {
        this.demographic = demographic;
        return this;
    }

    public SimpleFeatureImpl getBoundary() {
        return boundary;
    }

    public Precinct setBoundary(SimpleFeatureImpl boundary) {
        this.boundary = boundary;
        return this;
    }

    public double getArea() {
        return area;
    }

    public Precinct setArea(double area) {
        this.area = area;
        return this;
    }

    public double getPerimeter() {
        return perimeter;
    }

    public Precinct setPerimeter(double perimeter) {
        this.perimeter = perimeter;
        return this;
    }

    public String getVotingDistrictCode() {
        return votingDistrictCode;
    }

    public Precinct setVotingDistrictCode(String votingDistrictCode) {
        this.votingDistrictCode = votingDistrictCode;
        return this;
    }

    public String getName() {
        return name;
    }

    public Precinct setName(String name) {
        this.name = name;
        return this;
    }

    public String getCountyName() {
        return countyName;
    }

    public Precinct setCountyName(String countyName) {
        this.countyName = countyName;
        return this;
    }

    public String getGeoId() {
        return geoId;
    }

    public Precinct setGeoId(String geoId) {
        this.geoId = geoId;
        return this;
    }

    public void markAsUnVisited() {
        this.setVisited(false);
    }

    public void markAsVisited() {
        this.setVisited(true);
    }

    public boolean isVisited() {
        return visited;
    }

    public Precinct setVisited(boolean visited) {
        this.visited = visited;
        return this;
    }

    @Override
    public Precinct clone() {
        Precinct precinct = new Precinct();

        precinct.setId(this.getId())
                .setBorderPrecinct(this.isBorderPrecinct())
                .setMovable(this.isMovable())
                .setElection(this.getElection().clone())
                .setDemographic(this.getDemographic().clone())
                .setArea(this.getArea())
                .setCountyName(this.getCountyName())
                .setGeoId(this.getGeoId())
                .setName(this.getName())
                .setPerimeter(this.getPerimeter())
                .setVotingDistrictCode(this.getVotingDistrictCode())
                .setBoundary(this.getBoundary());

//        List<PrecinctNeighbor> precinctNeighbors = new ArrayList<>(this.getPrecinctNeighbors().size());
//        for(PrecinctNeighbor precinctNeighbor : this.getPrecinctNeighbors()) {
//            precinctNeighbors.add(new PrecinctNeighbor(precinctNeighbor.getId(), precinctNeighbor.getOwner(), precinctNeighbor.getNeighbor()));
//        }
//        precinct.setPrecinctNeighbors(precinctNeighbors);

        return precinct;
    }

     private static final Map<String, String> pennCountyFipsToCountyNameMap;
    static {
        Map<String, String> pennMap = new HashMap<>(67);
        pennMap.put("039","Crawford");
        pennMap.put("079","Luzerne ");
        pennMap.put("077","Lehigh ");
        pennMap.put("083","McKean");
        pennMap.put("089","Monroe");
        pennMap.put("027","Centre");
        pennMap.put("041","Cumberland");
        pennMap.put("043","Dauphin");
        pennMap.put("045","Delaware");
        pennMap.put("051","Fayette");
        pennMap.put("073","Lawrence ");
        pennMap.put("081","Lycoming");
        pennMap.put("091","Montgomery");
        pennMap.put("093","Montour");
        pennMap.put("095","Northampton");
        pennMap.put("047","Elk");
        pennMap.put("049","Erie");
        pennMap.put("075","Lebanon ");
        pennMap.put("007","Beaver");
        pennMap.put("055","Franklin");
        pennMap.put("057","Fulton");
        pennMap.put("071","Lancaster");
        pennMap.put("085","Mercer");
        pennMap.put("059","Greene");
        pennMap.put("061","Huntingdon");
        pennMap.put("063","Indiana");
        pennMap.put("065","Jefferson");
        pennMap.put("067","Juniata");
        pennMap.put("069","Lackawanna");
        pennMap.put("087","Mifflin");
        pennMap.put("097","Northumberland");
        pennMap.put("099","Perry");
        pennMap.put("101","Philadelphia");
        pennMap.put("103","Pike");
        pennMap.put("105","Potter");
        pennMap.put("107","Schuylkill");
        pennMap.put("109","Snyder");
        pennMap.put("111","Somerset");
        pennMap.put("113","Sullivan");
        pennMap.put("115","Susquehanna");
        pennMap.put("117","Tioga");
        pennMap.put("119","Union");
        pennMap.put("121","Venango");
        pennMap.put("123","Warren");
        pennMap.put("125","Washington");
        pennMap.put("127","Wayne");
        pennMap.put("129","Westmoreland");
        pennMap.put("131","Wyoming");
        pennMap.put("133","York");
        pennMap.put("019","Butler");
        pennMap.put("021","Cambria");
        pennMap.put("023","Cameron");
        pennMap.put("025","Carbon");
        pennMap.put("029","Chester");
        pennMap.put("031","Clarion");
        pennMap.put("033","Clearfield");
        pennMap.put("035","Clinton");
        pennMap.put("037","Columbia");
        pennMap.put("053","Forest");
        pennMap.put("001","Adams");
        pennMap.put("003","Allegheny");
        pennMap.put("005","Armstrong");
        pennMap.put("009","Bedford");
        pennMap.put("011","Berks ");
        pennMap.put("013","Blair");
        pennMap.put("015","Bradford");
        pennMap.put("017","Bucks");
        pennCountyFipsToCountyNameMap = Collections.unmodifiableMap(pennMap);
    }
}