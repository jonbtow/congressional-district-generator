package com.pumas.model;

import javax.persistence.*;

@Entity
public class PrecinctNeighbor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "owner_id", updatable = false, nullable = false)
    private Precinct owner;

    @ManyToOne
    @JoinColumn(name = "neighbor_id", updatable = false, nullable = false)
    private Precinct neighbor;

    public PrecinctNeighbor() {
    }

    public PrecinctNeighbor(Precinct owner, Precinct neighbor) {
        this();
        this.owner = owner;
        this.neighbor = neighbor;
    }

    public PrecinctNeighbor(long id, Precinct owner, Precinct neighbor) {
        this(owner, neighbor);
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public PrecinctNeighbor setId(long id) {
        this.id = id;
        return this;
    }

    public Precinct getOwner() {
        return owner;
    }

    public PrecinctNeighbor setOwner(Precinct owner) {
        this.owner = owner;
        return this;
    }

    public Precinct getNeighbor() {
        return neighbor;
    }

    public PrecinctNeighbor setNeighbor(Precinct neighbor) {
        this.neighbor = neighbor;
        return this;
    }
}
