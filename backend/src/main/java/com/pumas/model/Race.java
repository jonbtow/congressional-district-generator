package com.pumas.model;

public enum Race {

    WHITE("White Alone"),
    AFRICAN_AMERICAN("Black or African American Alone"),
    AMERICAN_INDIAN("American Indian and Alaska Native alone"),
    ASIAN("Asian alone"),
    PACIFIC_ISLANDER("Native Hawaiian and Other Pacific Islander alone"),
    OTHER("Some Other Race alone"),
    TWO_OR_MORE("Two or More Races");

    private final String description;

    private Race(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return this.description;
    }
}
