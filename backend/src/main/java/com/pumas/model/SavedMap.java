package com.pumas.model;

import com.pumas.model.auth.GeneralUser;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@Entity
public class SavedMap {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ElementCollection
    @CollectionTable(name = "precinctGeoIdToDistrictFipsMap")
    @MapKeyColumn(name = "precinctGeoId")
    private Map<String, String> precinctGeoIdToDistrictFipsMap;

    @ManyToOne(fetch = FetchType.LAZY)
    private State state;

    @ManyToOne(fetch = FetchType.LAZY)
    private GeneralUser generalUser;

    private double goodnessScore = 0;

    public SavedMap() {
        this.goodnessScore = 0.0;
        precinctGeoIdToDistrictFipsMap = new HashMap<>(2000);
    }

    public SavedMap(State state) {
        this();
        for(CongressionalDistrict district : state.getCongressionalDistricts()) {
            String districtFipsCode = district.getFipsCode();
            for(Precinct precinct : district.getPrecincts()) {
                precinctGeoIdToDistrictFipsMap.put(precinct.getGeoId(), districtFipsCode);
            }
        }
    }

    public long getId() {
        return id;
    }

    public SavedMap setId(long id) {
        this.id = id;
        return this;
    }

    public Map<String, String> getPrecinctGeoIdToDistrictFipsMap() {
        return precinctGeoIdToDistrictFipsMap;
    }

    public SavedMap setPrecinctGeoIdToDistrictFipsMap(Map<String, String> precinctGeoIdToDistrictFipsMap) {
        this.precinctGeoIdToDistrictFipsMap = precinctGeoIdToDistrictFipsMap;
        return this;
    }

    public State getState() {
        return state;
    }

    public SavedMap setState(State state) {
        this.state = state;
        return this;
    }

    public GeneralUser getGeneralUser() {
        return generalUser;
    }

    public SavedMap setGeneralUser(GeneralUser generalUser) {
        this.generalUser = generalUser;
        return this;
    }

    public double getGoodnessScore() {
        return goodnessScore;
    }

    public SavedMap setGoodnessScore(double goodnessScore) {
        this.goodnessScore = goodnessScore;
        return this;
    }
}
