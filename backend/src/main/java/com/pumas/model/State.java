package com.pumas.model;

//import com.pumas.api.input.KellyDistrict;
import com.pumas.model.converters.SimpleFeatureImplToStringConverter;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.geotools.feature.simple.SimpleFeatureImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * Created by Jaspreet on 4/8/2018.
 */
@Entity
@Table(name = "State")
public class State implements GeographicEntity, Cloneable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;

    private String name;

    private String fipsCode;

    private double area;

    private double perimeter;

    @Min(0)
    private int year;

    @Lob
    @Convert(converter = SimpleFeatureImplToStringConverter.class)
    private SimpleFeatureImpl boundary;

    @OneToMany(mappedBy = "state", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<CongressionalDistrict> congressionalDistricts;

    @OneToMany(mappedBy = "state")
    private List<SavedMap> redistrictedMaps;

    @Transient
    private List<CongressionalDistrict> movableDistricts;

    @Transient
    private Random rand;

    private static final Logger LOGGER = LoggerFactory.getLogger(State.class);

    public State() {
        this.congressionalDistricts = new ArrayList<>();
        this.redistrictedMaps = new ArrayList<>();
        this.movableDistricts = new ArrayList<>();
        this.rand = new Random();
    }

    public State(String name) {
        this();
        this.name = name;
    }

    public State(SimpleFeatureImpl stateFeature) {
        this();

        this.name = (String)stateFeature.getAttribute(GeoJsonAttribute.STATE_NAME.toString());
        this.fipsCode = (String) stateFeature.getAttribute(GeoJsonAttribute.STATE_FIPS_CODE.toString());
        this.area = (Double) stateFeature.getAttribute(GeoJsonAttribute.AREA.toString());
        this.perimeter = (Double) stateFeature.getAttribute(GeoJsonAttribute.PERIMETER.toString());
        this.boundary = stateFeature;
    }

    public Map<String, Map<Measure, Double>> getMeasureScores(Set<Measure> measures) {
        Map<String, Map<Measure, Double>> scores = new HashMap<>(this.getCongressionalDistricts().size());
        for(CongressionalDistrict district : this.getCongressionalDistricts()) {
            scores.put(district.getFipsCode(), district.getMeasureScores(measures));
        }
        return scores;
    }

    public CongressionalDistrict getRandomCongressionalDistrict() {
        int randomIndex = new Random().nextInt(this.getCongressionalDistricts().size());
        return this.getCongressionalDistricts().get(randomIndex);
    }

    public List<Precinct> getMovableValidRandomBorderPrecinctPair(Precinct gradientPrecinct, AtomicInteger gradientTimeout) {
        if(gradientPrecinct != null) {
            if(gradientTimeout.get() == 0) {
                gradientTimeout.set(9);
            } else {
                List<PrecinctNeighbor> neighbors = new ArrayList<>(gradientPrecinct.getPrecinctNeighbors());
                Collections.shuffle(neighbors);
                for (PrecinctNeighbor precinctNeighbor : neighbors) {
                    Precinct neighbor = precinctNeighbor.getNeighbor();
                    if (neighbor.isMovable()
                            && neighbor.isBorderPrecinct() &&
                            neighbor.getCongressionalDistrict() != gradientPrecinct.getCongressionalDistrict()) {
                        List<Precinct> pair = new ArrayList<>(2);
                        pair.add(gradientPrecinct);
                        pair.add(neighbor);
                        return pair;
                    }
                }
            }
        }

        List<CongressionalDistrict> congressionalDistricts = new ArrayList<>(this.getCongressionalDistricts());
        Collections.shuffle(congressionalDistricts, rand);
        for(CongressionalDistrict district : congressionalDistricts) {
            if(!district.isMovable()) {
                continue;
            }

            List<Precinct> precincts = new ArrayList<>(district.getPrecincts());
            Collections.shuffle(precincts, rand);
            Collections.shuffle(precincts, rand);
            for(Precinct precinct : district.getPrecincts()) {

                if(precinct.isBorderPrecinct() && precinct.isMovable()) {

                    List<PrecinctNeighbor> precinctNeighbors = new ArrayList<>(precinct.getPrecinctNeighbors());
                    Collections.shuffle(precinctNeighbors, rand);
                    Collections.shuffle(precinctNeighbors, rand);
                    Collections.shuffle(precinctNeighbors, rand);
                    for(PrecinctNeighbor precinctNeighbor : precinct.getPrecinctNeighbors()) {

                        if(precinctNeighbor.getNeighbor().getCongressionalDistrict() != precinct.getCongressionalDistrict()
                                && precinctNeighbor.getNeighbor().isMovable()
                                && precinctNeighbor.getNeighbor().isBorderPrecinct()) {
                            List<Precinct> precinctPair = new ArrayList<>();
                            precinctPair.add(precinct);
                            precinctPair.add(precinctNeighbor.getNeighbor());
                            return precinctPair;
                        }
                    }
                }
            }
        }


        return new ArrayList<>();
    }

    public CongressionalDistrict getRandomMovableCongressionalDistrict() {
        List<CongressionalDistrict> movableDistricts = this.getMovableCongressionalDistricts();
        int randomIndex = new Random().nextInt(movableDistricts.size());
        return movableDistricts.get(randomIndex);
    }

    public List<CongressionalDistrict> getMovableCongressionalDistricts() {
        if(this.movableDistricts.isEmpty()) {
            for(CongressionalDistrict district : this.getCongressionalDistricts()) {
                if(district.isMovable()) {
                    this.movableDistricts.add(district);
                }
            }
        }
        return this.movableDistricts;
    }

    // TODO contains might not actually check  the string
    public void setUnmovableEntities(Set<String> unMovableDistrictGeoIds, Set<String> unMovablePrecinctGeoIds) {
        for(CongressionalDistrict district : this.getCongressionalDistricts()) {
            if(unMovableDistrictGeoIds.contains(district.getGeoId())) {
                district.setMovable(false);
                for(Precinct precinct : district.getPrecincts()) {
                    precinct.setMovable(false);
                }
                continue;
            }
            if(!unMovablePrecinctGeoIds.isEmpty()) {
                for (Precinct precinct : district.getPrecincts()) {
                    if (unMovablePrecinctGeoIds.contains(precinct.getGeoId())) {
                        precinct.setMovable(false);
                    }
                }
            }
        }
    }

    public long getId() {
        return id;
    }

    public State setId(long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public State setName(String name) {
        this.name = name;
        return this;
    }

    public SimpleFeatureImpl getBoundary() {
        return boundary;
    }

    public State setBoundary(SimpleFeatureImpl boundary) {
        this.boundary = boundary;
        return this;
    }

    public List<CongressionalDistrict> getCongressionalDistricts() {
        return congressionalDistricts;
    }

    public State setCongressionalDistricts(List<CongressionalDistrict> congressionalDistricts) {
        this.congressionalDistricts = congressionalDistricts;
        return this;
    }

    public String getFipsCode() {
        return fipsCode;
    }

    public State setFipsCode(String fipsCode) {
        this.fipsCode = fipsCode;
        return this;
    }

    public double getArea() {
        return area;
    }

    public State setArea(double area) {
        this.area = area;
        return this;
    }

    public double getPerimeter() {
        return perimeter;
    }

    public State setPerimeter(double perimeter) {
        this.perimeter = perimeter;
        return this;
    }

    public int getYear() {
        return year;
    }

    public State setYear(int year) {
        this.year = year;
        return this;
    }

    public List<SavedMap> getRedistrictedMaps() {
        return redistrictedMaps;
    }

    public State setRedistrictedMaps(List<SavedMap> redistrictedMaps) {
        this.redistrictedMaps = redistrictedMaps;
        return this;
    }

    public CongressionalDistrict addCongressionalDistrict(SimpleFeatureImpl districtFeature) {
        CongressionalDistrict district = new CongressionalDistrict(districtFeature);
        this.getCongressionalDistricts().add(district);
        district.setState(this);
        return district;
    }

    public Map<String, CongressionalDistrict> addDistricts(FeatureCollection districtsFeatureCollection) {

        Map<String, CongressionalDistrict> fipsToDistrictMap = new HashMap<>(districtsFeatureCollection.size());

        try (FeatureIterator districtsFeaturesIterator = districtsFeatureCollection.features()) {
            while (districtsFeaturesIterator.hasNext()) {
                SimpleFeatureImpl districtFeature = (SimpleFeatureImpl) districtsFeaturesIterator.next();

                CongressionalDistrict district = this.addCongressionalDistrict(districtFeature);

                String stateFipsCodeFromFeature = (String) districtFeature.getAttribute(GeoJsonAttribute.STATE_FIPS_CODE.toString());
                    LOGGER.info(stateFipsCodeFromFeature);
                    LOGGER.info(this.getFipsCode());
                if (!stateFipsCodeFromFeature.equals(this.getFipsCode())) {

                    LOGGER.error("District with FIPS code " + district.getFipsCode()
                            + " has mismatching State FIPS code (found: " + stateFipsCodeFromFeature
                            + " expected: " + this.getFipsCode() + ")");
                    return null;
                }

                fipsToDistrictMap.put(district.getFipsCode(), district);
            }
        }

        addDistrictNeighbors(fipsToDistrictMap, districtsFeatureCollection);

        return fipsToDistrictMap;
    }

    private void addDistrictNeighbors(Map<String, CongressionalDistrict> fipsToDistrictMap, FeatureCollection districtsFeatureCollection) {
        try (FeatureIterator<SimpleFeatureImpl> districtsFeaturesIterator = districtsFeatureCollection.features()) {

            while (districtsFeaturesIterator.hasNext()) {
                SimpleFeatureImpl districtFeature = districtsFeaturesIterator.next();

                String districtFips = (String) districtFeature.getAttribute(GeoJsonAttribute.DISTRICT_FIPS_CODE.toString());
                String[] neighbors = ((String) districtFeature.getAttribute(GeoJsonAttribute.NEIGHBORS.toString())).split(",");

                CongressionalDistrict currentDistrict = fipsToDistrictMap.get(districtFips);
                currentDistrict.addNeighbors(neighbors, fipsToDistrictMap);

            }

        }
    }

    public Map<String, Precinct> addPrecincts(FeatureCollection precinctsFeatureCollection, Map<String, CongressionalDistrict> fipsToDistrictsMap) {
        Map<String, Precinct> geoIdToPrecinctsMap = new HashMap<>(precinctsFeatureCollection.size());

        try (FeatureIterator featureIterator = precinctsFeatureCollection.features()) {
            while (featureIterator.hasNext()) {
                SimpleFeatureImpl precinctFeature = (SimpleFeatureImpl) featureIterator.next();
                String owningDistrictFipsCode = (String) precinctFeature.getAttribute(GeoJsonAttribute.DISTRICT_FIPS_CODE.toString());

                CongressionalDistrict district = fipsToDistrictsMap.get(owningDistrictFipsCode);
                Precinct precinct = district.addPrecinct(precinctFeature);
                geoIdToPrecinctsMap.put(precinct.getGeoId(), precinct);
            }
        }

        try (FeatureIterator featureIterator = precinctsFeatureCollection.features()) {
            while (featureIterator.hasNext()) {
                SimpleFeatureImpl precinctFeature = (SimpleFeatureImpl) featureIterator.next();

                String geoId = (String) precinctFeature.getAttribute(GeoJsonAttribute.GEO_ID.toString());
                String[] neighbors = ((String) precinctFeature.getAttribute(GeoJsonAttribute.NEIGHBORS.toString())).split(",");

                Precinct currentPrecinct = geoIdToPrecinctsMap.get(geoId);

                currentPrecinct.addNeighbors(neighbors, geoIdToPrecinctsMap);
            }
        }

        return geoIdToPrecinctsMap;
    }


    /**
     * Calculates the average population of the districts
     * @return Average population of the districts.
     */
    public double getDistrictPopulationAverage(){
        double numOfDistricts = this.getCongressionalDistricts().size();
        double pop = 0;

        for(CongressionalDistrict district: this.getCongressionalDistricts()){
            pop += district.getPopulation();
        }

        return pop/numOfDistricts;
    }

    /**
     * Calculates the standard deviation of the districts population.
     * @return Standard deviation of the population of the districts.
     */
    public double getDistrictPopulationStd(){
        double std = 0;
        double sum = 0;
        double pop = 0;
        double popAvg = getDistrictPopulationAverage();

        double denominator = this.getCongressionalDistricts().size()-1;

        for(CongressionalDistrict district: this.getCongressionalDistricts()){
            pop = district.getPopulation();
            sum += Math.pow(pop - popAvg, 2);
        }

        std = Math.pow(sum/denominator, 0.5);
        return std;
    }

    @Override
    public State clone() {
        State state = new State();

        state.setId(this.getId())
                .setName(this.getName())
                .setBoundary(this.getBoundary())
                .setPerimeter(this.getPerimeter())
                .setArea(this.getArea())
                .setYear(this.getYear())
                .setFipsCode(this.getFipsCode());

        Map<String, CongressionalDistrict> geoIdToClonedDistrictMap = new HashMap<>(this.getCongressionalDistricts().size());
        Map<String, Precinct> geoIdToClonedPrecinctMap = new HashMap<>(this.getCongressionalDistricts().size() * 50);

        List<CongressionalDistrict> clonedDistrictsList = new ArrayList<>(this.getCongressionalDistricts().size());
        for(CongressionalDistrict origDistrict : this.getCongressionalDistricts()) {
            CongressionalDistrict congDist = origDistrict.clone();
            congDist.setState(state);
            geoIdToClonedDistrictMap.put(congDist.getGeoId(), congDist);
            clonedDistrictsList.add(congDist);

            for(Precinct ownedPrecinct : congDist.getPrecincts()) {
                geoIdToClonedPrecinctMap.put(ownedPrecinct.getGeoId(), ownedPrecinct);
            }
        }

        // handle congressional district neighbors
        for(CongressionalDistrict district : this.getCongressionalDistricts()) {
            Set<DistrictNeighbor> clonedNeighbors = new HashSet<>(district.getDistrictNeighbors().size());
            CongressionalDistrict clonedDistrict = geoIdToClonedDistrictMap.get(district.getGeoId());
            for(DistrictNeighbor districtNeighbor : district.getDistrictNeighbors()) {
                CongressionalDistrict clonedNeighbor = geoIdToClonedDistrictMap.get(districtNeighbor.getNeighbor().getGeoId());
                clonedNeighbors.add(new DistrictNeighbor(districtNeighbor.getId(), clonedDistrict, clonedNeighbor));
            }
            clonedDistrict.setDistrictNeighbors(clonedNeighbors);
        }

        state.setCongressionalDistricts(clonedDistrictsList);

        // handle precinct neighbors
        for(CongressionalDistrict district : this.getCongressionalDistricts()) {
            for (Precinct precinct : district.getPrecincts()) {
                Precinct clonedPrecinct = geoIdToClonedPrecinctMap.get(precinct.getGeoId());
                for(PrecinctNeighbor precinctNeighbor : precinct.getPrecinctNeighbors()) {
                    Precinct clonedNeighbor = geoIdToClonedPrecinctMap.get(precinctNeighbor.getNeighbor().getGeoId());
                    PrecinctNeighbor clonedPrecinctNeighbor = new PrecinctNeighbor(precinctNeighbor.getId(), clonedPrecinct, clonedNeighbor);
                    clonedPrecinct.getPrecinctNeighbors().add(clonedPrecinctNeighbor);
                }
            }
        }


        return state;
    }

}
