package com.pumas.model.auth;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

/**
 * Created by Jaspreet on 4/7/2018.
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Account {

   @Id
   @GeneratedValue(strategy = GenerationType.TABLE, generator = "person_gen")
   @TableGenerator(
           name = "person_gen",
           table = "GENERATOR_TABLE",
           pkColumnName = "person_key",
           valueColumnName = "person_next",
           pkColumnValue = "person_id",
           allocationSize = 1
   )
   private long id;

   @Email(message = "A valid email is required.")
   @NotEmpty(message = "An email is required.")
   @Column(unique = true, nullable = false)
   private String email;

   @NotEmpty(message = "A password is required.")
   @Column(name = "password", nullable = false)
   @JsonIgnore
   private String password;

   @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
   @JoinColumn(name = "role_id")
   private Role role;

   private boolean isEnabled;


   public Account() { }

   public Account(Account person) {
      this.id = person.getId();
      this.email = person.getEmail();
      this.password = person.getPassword();
      this.isEnabled = true;
   }

   public long getId() {
      return id;
   }

   public Account setId(long id) {
      this.id = id;
      return this;
   }

   public String getEmail() {
      return this.email;
   }

   public Account setEmail(String email) {
      this.email = email;
      return this;
   }

   public String getPassword() {
      return this.password;
   }

   public Account setPassword(String password) {
      this.password = password;
      return this;
   }

   public Role getRole() {
      return this.role;
   }

   public Account setRole(Role role) {
      this.role = role;
      return this;
   }

    public boolean isEnabled() {
        return isEnabled;
    }

    public Account setEnabled(boolean enabled) {
        isEnabled = enabled;
        return this;
    }
}
