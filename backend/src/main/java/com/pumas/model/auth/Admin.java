package com.pumas.model.auth;

import javax.persistence.Entity;
import javax.persistence.Transient;

/**
 * Created by Jaspreet on 4/7/2018.
 */
@Entity
public class Admin extends Account {

    public Admin() {
        super();
    }

    public Admin(Admin admin) {
        super(admin);
    }

    public SafeAdmin getSafeCopy() {
        return new SafeAdmin(this);
    }

    public class SafeAdmin {

        private long id;

        private boolean isEnabled;

        private String email;

        private String password;

        public SafeAdmin(Admin admin) {
            this.id = admin.getId();
            this.isEnabled = admin.isEnabled();
            this.email = admin.getEmail();
            this.password = "NA";
        }

        public long getId() {
            return id;
        }

        public SafeAdmin setId(long id) {
            this.id = id;
            return this;
        }

        public boolean isEnabled() {
            return isEnabled;
        }

        public SafeAdmin setEnabled(boolean enabled) {
            isEnabled = enabled;
            return this;
        }

        public String getEmail() {
            return email;
        }

        public SafeAdmin setEmail(String email) {
            this.email = email;
            return this;
        }

        public String getPassword() {
            return password;
        }

        public SafeAdmin setPassword(String password) {
            this.password = password;
            return this;
        }
    }

}
