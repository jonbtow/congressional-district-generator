package com.pumas.model.auth;


import com.pumas.model.CongressionalDistrict;
import com.pumas.model.Precinct;
import com.pumas.model.SavedMap;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Jaspreet on 4/7/2018.
 */
@Entity
public class GeneralUser extends Account {

    @OneToMany(mappedBy = "generalUser", fetch = FetchType.LAZY)
    private List<SavedMap> savedMaps;

    public GeneralUser() {
        super();
        savedMaps = new ArrayList<>();
    }

    public GeneralUser(GeneralUser generalUser) {
        super(generalUser);
    }

    public List<SavedMap> getSavedMaps() {
        return savedMaps;
    }

    public GeneralUser setSavedMaps(List<SavedMap> savedMaps) {
        this.savedMaps = savedMaps;
        return this;
    }

    public SafeUser getSafeCopy() {
        return new SafeUser(this);
    }

    public class SafeUser {

        private long id;

        private String email;

        private String password;

        private boolean isEnabled;

        public SafeUser(GeneralUser generalUser) {
            this.id = generalUser.getId();
            this.email = generalUser.getEmail();
            this.isEnabled = generalUser.isEnabled();
            this.password = "NA";
        }

        public String getEmail() {
            return email;
        }

        public SafeUser setEmail(String email) {
            this.email = email;
            return this;
        }

        public String getPassword() {
            return password;
        }

        public SafeUser setPassword(String password) {
            this.password = password;
            return this;
        }

        public long getId() {
            return id;
        }

        public SafeUser setId(long id) {
            this.id = id;
            return this;
        }

        public boolean isEnabled() {
            return isEnabled;
        }

        public SafeUser setEnabled(boolean enabled) {
            isEnabled = enabled;
            return this;
        }
    }
}
