package com.pumas.model.auth;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Jaspreet on 4/7/2018.
 */
@Entity
public class Role implements Cloneable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(unique = true)
    private RoleType role;

    @OneToMany(mappedBy = "role", fetch = FetchType.LAZY)
    private Set<Account> accounts;

    public Role() {
        this.accounts = new HashSet<>();
        // do not initialize roletype here since we want the client to explicitly specify it
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public RoleType getRole() {
        return this.role;
    }

    public void setRole(RoleType role) {
        this.role = role;
    }

    public Set<Account> getAccounts() {
        return accounts;
    }

    public Role setAccounts(Set<Account> accounts) {
        this.accounts = accounts;
        return this;
    }

    public enum RoleType {
        ROLE_USER,
        ROLE_ADMIN
    }
}
