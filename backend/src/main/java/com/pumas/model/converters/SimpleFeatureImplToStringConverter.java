package com.pumas.model.converters;

import org.geotools.feature.simple.SimpleFeatureImpl;
import org.geotools.geojson.feature.FeatureJSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/**
 * Created by Jaspreet on 4/8/2018.
 */
@Converter
public class SimpleFeatureImplToStringConverter implements AttributeConverter<SimpleFeatureImpl, String> {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleFeatureImplToStringConverter.class);

    private FeatureJSON featureJSON = new FeatureJSON();

    @Override
    public String convertToDatabaseColumn(SimpleFeatureImpl simpleFeature) {
        String geoJson = "";

        try {
            geoJson = featureJSON.toString(simpleFeature);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }

        return geoJson;
    }

    @Override
    public SimpleFeatureImpl convertToEntityAttribute(String data) {
        SimpleFeatureImpl simpleFeature = null;

        InputStream inputStream = new ByteArrayInputStream(data.getBytes(StandardCharsets.UTF_8));

        try {
            simpleFeature = (SimpleFeatureImpl) featureJSON.readFeature(inputStream);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
        }

        return simpleFeature;
    }

}