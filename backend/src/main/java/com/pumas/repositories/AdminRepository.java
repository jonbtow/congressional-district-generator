package com.pumas.repositories;

import com.pumas.model.auth.Admin;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Jaspreet on 4/8/2018.
 */
@Repository
public interface AdminRepository extends CrudRepository<Admin, Long> {

    Admin findByEmail(String email);

    boolean existsById(long id);

}
