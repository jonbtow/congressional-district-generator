package com.pumas.repositories;

import com.pumas.model.CongressionalDistrict;
import org.geotools.feature.simple.SimpleFeatureImpl;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DistrictRepository extends CrudRepository<CongressionalDistrict, Long> {

    @Query("select cd.boundary from CongressionalDistrict cd " +
                "where cd.state.name = :stateName " +
                "and cd.state.year = :year")
    List<SimpleFeatureImpl> findBoundaries(String stateName, int year);

}
