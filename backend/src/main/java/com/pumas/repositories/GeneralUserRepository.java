package com.pumas.repositories;

import com.pumas.model.auth.GeneralUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Jaspreet on 4/8/2018.
 */
@Repository
public interface GeneralUserRepository extends CrudRepository<GeneralUser, Long> {

    GeneralUser findByEmail(String email);

    boolean existsById(long id);

}
