package com.pumas.repositories;

import com.pumas.model.Precinct;
import org.geotools.feature.simple.SimpleFeatureImpl;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PrecinctRepository extends CrudRepository<Precinct, Long> {

    @Query("select prec.boundary from Precinct prec " +
                "where prec.congressionalDistrict.state.name = :stateName " +
                "and prec.congressionalDistrict.state.year = :year")
    List<SimpleFeatureImpl> findBoundaries(String stateName, int year);


}
