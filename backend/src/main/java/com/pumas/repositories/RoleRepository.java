package com.pumas.repositories;

import com.pumas.model.auth.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Jaspreet on 4/9/2018.
 */
@Repository
public interface RoleRepository extends CrudRepository<Role, Long> {

    Role findByRole(Role.RoleType role);

}
