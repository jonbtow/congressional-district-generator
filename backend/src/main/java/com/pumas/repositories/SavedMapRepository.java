package com.pumas.repositories;

import com.pumas.model.SavedMap;
import com.pumas.model.auth.GeneralUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SavedMapRepository extends CrudRepository<SavedMap, Long> {

    List<SavedMap> findAllByGeneralUser(GeneralUser generalUser);

    @Query("SELECT savedMap.id, savedMap.state.name, savedMap.state.year, savedMap.goodnessScore FROM SavedMap savedMap WHERE savedMap.generalUser.email=:userEmail")
    List<Object[]> findAllSavedMapsByUser(String userEmail);

    @Query("SELECT savedMap.state.name, savedMap.state.year, savedMap.goodnessScore FROM SavedMap savedMap")
    List<Object[]> findAllSavedMaps();
}
