package com.pumas.repositories;

import com.pumas.model.State;
import org.geotools.feature.simple.SimpleFeatureImpl;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by Jaspreet on 4/9/2018.
 */
@Repository
public interface StateRepository extends CrudRepository<State, Long> {

    State findByNameAndYear(String name, int year);

    @Query("SELECT state.name,state.year FROM State state")
    List<Object[]> findAllNamesAndYears();

    @Query("select state.boundary FROM State state " +
                "where state.name = :stateName " +
                "and state.year = :year")
    SimpleFeatureImpl findBoundaryByNameAndYear(String stateName, int year);

    boolean existsByNameAndYear(String name, int year);

    boolean existsByFipsCode(int fipsCode);

}
