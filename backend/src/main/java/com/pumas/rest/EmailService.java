package com.pumas.rest;

import com.pumas.model.auth.Account;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.mail.javamail.JavaMailSender;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Component
public class EmailService {

    @Autowired
    public JavaMailSender emailSender;

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailService.class);

    public void sendVerificationEmail(Account account) {
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = null;
        try {
            helper = new MimeMessageHelper(message, true);
            helper.setTo(account.getEmail());
            helper.setSubject("Registration Confirmation for DistrictGenerator");
            helper.setText("<html><body><h1>Thank you for registering!</h1><h3>Registered Email</h3>" + account.getEmail() + "</body></html>", true);
            emailSender.send(message);
        } catch (MessagingException e) {
            LOGGER.warn("Unable to send email to " + account.getEmail() + ". Error: " + e.toString());
        }
    }
}
