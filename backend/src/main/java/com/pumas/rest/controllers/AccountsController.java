package com.pumas.rest.controllers;

import com.pumas.model.GeographicEntity;
import com.pumas.model.auth.Admin;
import com.pumas.model.auth.GeneralUser;
import com.pumas.model.auth.Role;
import com.pumas.repositories.AdminRepository;
import com.pumas.repositories.GeneralUserRepository;
import com.pumas.repositories.RoleRepository;
import com.pumas.rest.EmailService;
import com.pumas.rest.payload.AdminRegistrationRequest;
import com.pumas.rest.payload.JwtAuthenticationRequest;
import com.pumas.rest.payload.UserRegistrationRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.persistence.GeneratedValue;
import javax.validation.Valid;
import java.util.*;

@RestController
public class AccountsController {

    @Autowired
    private GeneralUserRepository generalUserRepository;

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    AuthenticationController authenticationController;

    @Autowired
    private EmailService emailService;

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountsController.class);

    @GetMapping("${route.accounts.users}")
    public ResponseEntity<?> getAllUsers() {
        List<GeneralUser.SafeUser> generalUsers = new ArrayList<>(10);
        for(GeneralUser generalUser : generalUserRepository.findAll()) {
            generalUsers.add(generalUser.getSafeCopy());
        }

        return ResponseEntity.status(HttpStatus.OK).body(generalUsers);
    }

    @GetMapping("${route.accounts.admins}")
    public ResponseEntity<?> getAllAdmins() {
        List<Admin.SafeAdmin> admins = new ArrayList<>(10);
        for(Admin admin : adminRepository.findAll()) {
            admins.add(admin.getSafeCopy());
        }
        return ResponseEntity.status(HttpStatus.OK).body(admins);
    }

    @PostMapping("${route.accounts.user}")
    public ResponseEntity<?> registerGeneralUser(@RequestBody @Valid UserRegistrationRequest registerRequest) {

        if(generalUserRepository.findByEmail(registerRequest.getEmail()) != null) {
            Map<String, String> response = Collections.singletonMap("message", "An account already exists for this email");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }

        // TODO should move this to be in the database already (perhaps through data.sql or just workbench)
        Role generalUserRole;
        if((generalUserRole = roleRepository.findByRole(Role.RoleType.ROLE_USER)) == null) {
            generalUserRole = new Role();
            generalUserRole.setRole(Role.RoleType.ROLE_USER);
        }

        GeneralUser newGeneralUser = new GeneralUser();
        newGeneralUser.setEmail(registerRequest.getEmail())
                .setPassword(passwordEncoder.encode(registerRequest.getPassword()))
                .setRole(generalUserRole);

        generalUserRepository.save(newGeneralUser);

        emailService.sendVerificationEmail(newGeneralUser);

        return authenticationController.createAuthenticationToken(
                new JwtAuthenticationRequest(registerRequest.getEmail(), registerRequest.getPassword()));
    }

    @PostMapping("${route.accounts.user.modify")
    public ResponseEntity<?> modifyGeneralUser(GeneralUser generalUser) {
        if(generalUser.getEmail() != null
                && !generalUser.getEmail().isEmpty()
                && generalUser.getPassword() != null
                && !generalUser.getPassword().isEmpty()
                && generalUserRepository.existsById(generalUser.getId())) {
            generalUserRepository.save(generalUser);
            return ResponseEntity.status(HttpStatus.OK).body(generalUser);
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.singletonMap("message", "An error occurred."));
    }

    @PostMapping("/accounts/user/delete/{id}")
    public ResponseEntity<?> disableGeneralUser(@PathVariable("id") long userId) {
        Map<String, String> response = new HashMap<>(1);

        Optional<GeneralUser> generalUserToDisable = generalUserRepository.findById(userId);

        if(generalUserToDisable.isPresent()) {

            GeneralUser generalUser = generalUserToDisable.get();
            generalUser.setEnabled(false);

            generalUserRepository.save(generalUser);

            response.put("message", "User was successfully disabled.");
            return ResponseEntity.ok(response);

        } else {
            response.put("message", "Provided user id does not correspond to any existing admin.");
            return ResponseEntity.badRequest().body(response);
        }
    }

    @PostMapping("${route.accounts.admin}")
    public ResponseEntity<?> registerAdmin(@RequestBody AdminRegistrationRequest registerRequest) {
        Map<String, String> response = new HashMap<>(1);

        if(adminRepository.findByEmail(registerRequest.getEmail()) != null) {
            response.put("message", "An account already exists for this email");
            return ResponseEntity.badRequest().body(response);
        }

        Role adminRole;
        if((adminRole = roleRepository.findByRole(Role.RoleType.ROLE_ADMIN)) == null) {
            adminRole = new Role();
            adminRole.setRole(Role.RoleType.ROLE_ADMIN);
        }

        Admin newAdmin = new Admin();
        newAdmin.setEmail(registerRequest.getEmail())
                .setPassword(passwordEncoder.encode(registerRequest.getPassword()))
                .setRole(adminRole);

        adminRepository.save(newAdmin);

        emailService.sendVerificationEmail(newAdmin);

        response.put("message", "Registration was successful");

        return ResponseEntity.ok(response);
    }

    @PostMapping("${route.accounts.admin.modify}")
    public ResponseEntity<?> modifyAdmin(@RequestBody  Admin newAdmin) {
        if(newAdmin.getEmail() != null
                && !newAdmin.getEmail().isEmpty()
                && newAdmin.getPassword() != null
                && !newAdmin.getPassword().isEmpty()
                && adminRepository.existsById(newAdmin.getId())) {
            adminRepository.save(newAdmin);
            return ResponseEntity.status(HttpStatus.OK).body(newAdmin);
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.singletonMap("message", "An error occurred."));
    }

    @PostMapping("/accounts/admin/delete/{id}")
    public ResponseEntity<?> disableAdmin(@PathVariable("id") long adminId) {
        Map<String, String> response = new HashMap<>(1);

        Optional<Admin> adminToDisable = adminRepository.findById(adminId);

        if(adminToDisable.isPresent()) {

            Admin admin = adminToDisable.get();
            admin.setEnabled(false);

            adminRepository.save(admin);

            response.put("message", "Admin was successfully disabled.");
            return ResponseEntity.ok(response);

        } else {
            response.put("message", "Provided admin id does not correspond to any existing admin.");
            return ResponseEntity.badRequest().body(response);
        }
    }
}
