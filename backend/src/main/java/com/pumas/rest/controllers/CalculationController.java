package com.pumas.rest.controllers;

import com.pumas.algorithm.CalculationRequestType;
import com.pumas.algorithm.CalculationState;
import com.pumas.algorithm.DistrictGenerator;
import com.pumas.algorithm.UserPreferences;
import com.pumas.rest.payload.CalculationRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.util.concurrent.ListenableFuture;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Jaspreet on 4/9/2018.
 */
@Controller
public class CalculationController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CalculationController.class);

    private static final Map<String, CalculationState> emailToCalculationStateMap = new HashMap<>();

    @Autowired
    private DistrictGenerator districtGenerator;

    @Autowired
    private SimpMessagingTemplate template;

    @MessageMapping("/calculations")
    public void calculateRedistricting(CalculationRequest calculationRequest, Principal principal) {
        LOGGER.info("Calculations Requesting User: " + principal.getName());

        CalculationState currentCalculationState = emailToCalculationStateMap.get(principal.getName());
        if(calculationRequest.getRequestType().equals(CalculationRequestType.START)){
            // STOP any previously running calculation
           if(currentCalculationState != null
                && currentCalculationState.getTask() != null
                && !currentCalculationState.getTask().isDone()
                && !currentCalculationState.getTask().isCancelled()) {
                currentCalculationState.getTask().cancel(true);
                LOGGER.info("STOPPING previous running algorithm for user " + principal.getName());
            }
        } else {
            if(currentCalculationState == null) {
                LOGGER.warn("User " + principal.getName() + " attempted to do a non-START command "
                        + calculationRequest.getRequestType().toString() + " without first issuing START");
                return;
            }
        }

        switch (calculationRequest.getRequestType()) {
            case START:
                // TODO stop running algo if user disconnects
                LOGGER.info("User " + principal.getName() + " issued START request on district generator " + districtGenerator);

                UserPreferences userPreferences = calculationRequest.getUserPreferences();
                CalculationState calculationState = new CalculationState(principal.getName(), null, new AtomicBoolean(false), new AtomicBoolean(false));
                calculationState.setMessageTemplate(template);

                ListenableFuture<?> task = districtGenerator.redistrict(userPreferences, calculationState);
                calculationState.setTask(task);
                emailToCalculationStateMap.put(principal.getName(), calculationState);

                task.addCallback(
                        (result) -> {
                            emailToCalculationStateMap.remove(principal.getName());
                        },
                        (throwable) -> {
                            emailToCalculationStateMap.remove(principal.getName());
                            if(throwable instanceof CancellationException) {
                                LOGGER.info("Algorithm  for user " + principal.getName() + " CANCELLED.");
                            } else {
                                LOGGER.error("Future for user " + principal.getName() + " failed. error: " + throwable.toString());
                                throwable.printStackTrace();
                            }
                        }
                        );
                break;
            case RESUME:
                LOGGER.info("User " + principal.getName() + " issued RESUME request on district generator " + districtGenerator);
                boolean isAlreadyResumed = !emailToCalculationStateMap.get(principal.getName()).getIsPaused().compareAndSet(true, false);
                if(isAlreadyResumed) {
                    LOGGER.warn("User " + principal.getName() + " attempted to RESUME a non-paused district generator");
                }
                break;
            case PAUSE:
                LOGGER.info("User " + principal.getName() + " issued PAUSE request on district generator " + districtGenerator);
                boolean isAlreadyPaused = !emailToCalculationStateMap.get(principal.getName()).getIsPaused().compareAndSet(false, true);
                if(isAlreadyPaused) {
                    LOGGER.warn("User " + principal.getName() + " attempted to PAUSE an already paused district generator");
                }
                break;
            case STOP:
                LOGGER.info("User " + principal.getName() + " issued STOP request on district generator " + districtGenerator);
                Future<?> taskToStop = emailToCalculationStateMap.get(principal.getName()).getTask();
                if(taskToStop == null) {
                    LOGGER.warn("User " + principal.getName() + " attempted to STOP non-existing task.");
                    return;
                }
                taskToStop.cancel(true);
                break;
            case SAVE:
                LOGGER.info("User " + principal.getName() + " issued SAVE.");
                emailToCalculationStateMap.get(principal.getName()).getSaveRequested().compareAndSet(false, true);
                break;
            default:
                LOGGER.error("Unsupported operation type for user " + principal.getName() + " operation: " + calculationRequest.getRequestType());
        }

    }

}
