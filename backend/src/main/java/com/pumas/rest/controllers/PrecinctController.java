package com.pumas.rest.controllers;

import com.pumas.model.CongressionalDistrict;
import com.pumas.model.Precinct;
import com.pumas.repositories.DistrictRepository;
import com.pumas.repositories.PrecinctRepository;
import com.pumas.repositories.StateRepository;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.simple.SimpleFeatureImpl;
import org.geotools.geojson.feature.FeatureJSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import sun.java2d.pipe.SpanShapeRenderer;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

@RestController
public class PrecinctController {

    @Autowired
    private PrecinctRepository precinctRepository;

    @Autowired
    private StateRepository stateRepository;

    private FeatureJSON featureJSON = new FeatureJSON();

    private static final Logger LOGGER = LoggerFactory.getLogger(PrecinctController.class);

    @GetMapping(value = "${route.data.state.precincts.geometry}")
    public ResponseEntity<?> getDistrictGeometries(String stateName, int year) {
        if(!stateRepository.existsByNameAndYear(stateName, year)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    Collections.singletonMap("message", "The specified state does not exist.")
            );
        }

        List<SimpleFeatureImpl> features = precinctRepository.findBoundaries(stateName, year);

        DefaultFeatureCollection featureCollection = new DefaultFeatureCollection();
        featureCollection.addAll(features);

        String geoJson = "";
        try {
            geoJson = featureJSON.toString(featureCollection);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    Collections.singletonMap("message", "Something went wrong. Try again later.")
            );
        }
        return ResponseEntity.status(HttpStatus.OK).body(
                geoJson
        );
    }

}
