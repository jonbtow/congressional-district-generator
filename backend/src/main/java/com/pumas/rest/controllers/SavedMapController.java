package com.pumas.rest.controllers;

import com.pumas.model.SavedMap;
import com.pumas.model.auth.GeneralUser;
import com.pumas.repositories.PrecinctRepository;
import com.pumas.repositories.SavedMapRepository;
import com.sun.org.apache.xerces.internal.xs.StringList;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.simple.SimpleFeatureImpl;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.hibernate.cfg.SecondaryTableSecondPass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.web.server.ui.LoginPageGeneratingWebFilter;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.ws.Response;
import java.util.*;

@RestController
public class SavedMapController {

    @Autowired
    private SavedMapRepository savedMapRepository;

    @Autowired
    private PrecinctRepository precinctRepository;

    @GetMapping("/saved/all/redistrictings")
    public ResponseEntity<?> getAllSavedMaps() {
        List<Object[]> persistedMaps = savedMapRepository.findAllSavedMaps();

        if(persistedMaps == null) {
            return ResponseEntity.status(HttpStatus.OK).body(new ArrayList<Map<String, String>>().add(new HashMap<>()));
        }

        List<Map<String,String>> savedMaps = new ArrayList<Map<String,String>>(persistedMaps.size());
        for(Object[] obj : persistedMaps) {
            Map<String, String> currMap = new HashMap<>(3);

            currMap.put("stateName", (String)obj[0]);
            currMap.put("stateYear", Integer.toString((int)obj[1]));
            currMap.put("goodnessScore", Double.toString((double)obj[2]));

            savedMaps.add(currMap);
        }
        return ResponseEntity.status(HttpStatus.OK).body(savedMaps);
    }

    @GetMapping("${route.data.saved.redistrictings}")
    public ResponseEntity<?> getAllSavedMapsByUser(String email) {
        List<Object[]> persistedMaps = savedMapRepository.findAllSavedMapsByUser(email);

        if(persistedMaps == null) {
            return ResponseEntity.status(HttpStatus.OK).body(new ArrayList<Map<String, String>>().add(new HashMap<>()));
        }

        List<Map<String,String>> savedMaps = new ArrayList<Map<String,String>>(persistedMaps.size());
        for(Object[] obj : persistedMaps) {
            Map<String, String> currMap = new HashMap<>(4);

            if(!(obj[0] instanceof String)) {
                currMap.put("mapId", Long.toString((long)obj[0]));
            } else {
                System.out.println("Mapid was string");
                currMap.put("mapId", (String) obj[0]);
            }
            currMap.put("stateName", (String)obj[1]);

            if(!(obj[2] instanceof String)) {
                currMap.put("stateYear", Integer.toString((int)obj[2]));
            } else {
                currMap.put("stateYear", (String) obj[2]);
            }

            if(!(obj[3] instanceof String)) {
                currMap.put("goodnessScore", Double.toString((double)obj[3]));
            } else {
                currMap.put("goodnessScore", (String) obj[3]);
            }

            savedMaps.add(currMap);
        }
        return ResponseEntity.status(HttpStatus.OK).body(savedMaps);
    }

    @GetMapping("${route.data.saved.redistricting}")
    public ResponseEntity<?> getSavedMap(long savedMapId) {
        Optional<SavedMap> savedMapOptional = savedMapRepository.findById(savedMapId);

        if(!savedMapOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    Collections.singletonMap("message", "Could not find the specified saved map")
            );
        }

        SavedMap savedMap = savedMapOptional.get();

        List<SimpleFeatureImpl> features = precinctRepository.findBoundaries(savedMap.getState().getName(), savedMap.getState().getYear());
        DefaultFeatureCollection featureCollection = new DefaultFeatureCollection();
        featureCollection.addAll(features);

        List<Object> resultList = new ArrayList<>(2);
        resultList.add(featureCollection);
        resultList.add(savedMap);

        return ResponseEntity.status(HttpStatus.OK).body(resultList);
    }

    @PostMapping("/saved/redistricting/delete")
    public ResponseEntity<?> deleteSavedMap(long savedMapId) {
        boolean savedMapExists = savedMapRepository.existsById(savedMapId);

        if(!savedMapExists) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    Collections.singletonMap("message", "Could not find the specified saved map")
            );
        }

        savedMapRepository.deleteById(savedMapId);

        return ResponseEntity.status(HttpStatus.OK).body(Collections.singletonMap("message", "Saved map successfully deleted."));
    }

}
