package com.pumas.rest.controllers;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.pumas.model.CongressionalDistrict;
import com.pumas.model.Demographic;
import com.pumas.model.Precinct;
import com.pumas.model.State;
import com.pumas.repositories.StateRepository;
import com.sun.media.jai.mlib.MlibAbsoluteRIF;
import org.geotools.data.DataUtilities;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.simple.SimpleFeatureImpl;
import org.geotools.geojson.feature.FeatureJSON;
import org.omg.PortableServer.THREAD_POLICY_ID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import sun.java2d.pipe.SpanShapeRenderer;

import java.io.*;
import java.util.*;

/**
 * Created by Jaspreet on 4/9/2018.
 */
@RestController
public class StateController {

    private static final Logger LOGGER = LoggerFactory.getLogger(StateController.class);

    @Autowired
    private StateRepository stateRepository;

    private FeatureJSON featureJSON = new FeatureJSON();

    @GetMapping(value = "${route.data.states.all.names}")
    public ResponseEntity<?> getAllStates() {
        List<Object[]> results = stateRepository.findAllNamesAndYears();
        Map<String, Integer> namesToYearsMap = new HashMap<>(results.size());
        for(Object[] result : results){
            String name = (String) result[0];
            Integer year = (Integer) result[1];
            namesToYearsMap.put(name, year);
        }
//        for(State state : stateRepository.findAll()) {
//            if(namesToYearsMap.get(state.getName()) == null) {
//                namesToYearsMap.put(state.getName(), new ArrayList<>());
//            }
//            namesToYearsMap.get(state.getName()).add(state.getYear());
//        }

        return ResponseEntity.status(HttpStatus.OK).body(
                namesToYearsMap
        );
    }

    @GetMapping(value = "${route.data.states.all.geometry}")
    public ResponseEntity<?> getStateGeometry(String stateName, int year) {
        SimpleFeatureImpl stateBoundary = stateRepository.findBoundaryByNameAndYear(stateName, year);

        if(stateBoundary == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    Collections.singletonMap("message", "State not found")
            );
        } else {
            String geoJson = "";
            try {
                DefaultFeatureCollection featureCollection = new DefaultFeatureCollection();
                featureCollection.add(stateBoundary);
                geoJson = featureJSON.toString(featureCollection);
            } catch (IOException e) {
                LOGGER.error(e.getMessage());
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                        Collections.singletonMap("message", "Something went wrong. Try again later.")
                );
            }
            return ResponseEntity.status(HttpStatus.OK).body(
                    geoJson
            );
        }
    }

    @PostMapping(value = "${route.data.states}")
    public ResponseEntity<?> addState(String stateName,
                                      int year,
                                      MultipartFile stateFile,
                                      MultipartFile districtsFile,
                                      MultipartFile precinctsFile,
                                      MultipartFile demographicsFile) {
        FeatureCollection stateFeatureCollection = null;
        FeatureCollection districtsFeatureCollection = null;
        FeatureCollection precinctsFeatureCollection = null;

        try {
            InputStream stateInputStream = new BufferedInputStream(stateFile.getInputStream());
            stateFeatureCollection = featureJSON.readFeatureCollection(stateInputStream);

            InputStream districtsInputStream = new BufferedInputStream(districtsFile.getInputStream());
            districtsFeatureCollection = featureJSON.readFeatureCollection(districtsInputStream);

            InputStream precinctsInputStream = new BufferedInputStream(precinctsFile.getInputStream());
            precinctsFeatureCollection = featureJSON.readFeatureCollection(precinctsInputStream);
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.singletonMap("message", "Something was wrong with the upload files"));
        }

         if(stateRepository.existsByNameAndYear(stateName, year)) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.singletonMap("message", "A record of this state and year already exists."));
        }
        if(stateFeatureCollection.size() != 1) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    Collections.singletonMap("message", "The state feature collection has more than one feature."));
        }

        SimpleFeatureImpl stateFeature = (SimpleFeatureImpl) stateFeatureCollection.toArray()[0];
        State state = new State(stateFeature);
        state.setYear(year);

        if(!stateName.equals(state.getName())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    Collections.singletonMap("message",
                            "Feature state name (" + state.getName() + ") does not match given state name (" + stateName + ")."));
        }

        Map<String, CongressionalDistrict> fipsToDistrictsMap = state.addDistricts(districtsFeatureCollection);
        if(fipsToDistrictsMap == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    Collections.singletonMap("message", "An error occurred while parsing the districts feature collection.")
            );
        }

        // TODO: Fix border alignment before enabling this
        Map<String, Precinct> geoIdToPrecinctsMap = state.addPrecincts(precinctsFeatureCollection, fipsToDistrictsMap);
        if(geoIdToPrecinctsMap == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                    Collections.singletonMap("message", "An error occurred while parsing the precincts feature collection.")
            );
        }

        Map<String, List<Precinct>> countyNamesToPrecinctsMap = new HashMap<>(state.getCongressionalDistricts().size() * 6);
        Map<String, Integer> countyNameToTotalPopulationMap = new HashMap<>(countyNamesToPrecinctsMap.size());

        // use case 23
        for(CongressionalDistrict district : state.getCongressionalDistricts()) {
            for(Precinct precinct : district.getPrecincts()) {
                precinct.updateBorderPrecinctStatus();

                int totalCountyPopulation = 0;
                if(countyNamesToPrecinctsMap.get(precinct.getCountyName()) == null) {
                    countyNamesToPrecinctsMap.put(precinct.getCountyName(), new ArrayList<>());
                }
                if(countyNameToTotalPopulationMap.get(precinct.getCountyName()) != null) {
                    totalCountyPopulation = countyNameToTotalPopulationMap.get(precinct.getCountyName());
                }
                countyNamesToPrecinctsMap.get(precinct.getCountyName()).add(precinct);
                countyNameToTotalPopulationMap.put(precinct.getCountyName(), totalCountyPopulation + precinct.getDemographic().getTotalPopulation());
            }
        }


        try {
            Reader reader = new BufferedReader(new InputStreamReader(demographicsFile.getInputStream()));
            // skip header line
            CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).build();

            String[] records;
            while((records = csvReader.readNext()) != null) {
                String county = records[0];
                int totCountyPop = new Integer(records[1]);
                int whitePopulation = new Integer(records[2]);
                int blackPopulation = new Integer(records[3]);
                int nativeAmericanPopulation = new Integer(records[4]);
                int asianPopulation = new Integer(records[5]);
                int pacificIslanderPopulation = new Integer(records[6]);
                int otherRacePopulation = new Integer(records[7]);
                int twoOrMoreRacesPopulation = new Integer(records[8]);

                List<Precinct> countyPrecincts = countyNamesToPrecinctsMap.get(county);
                if(countyPrecincts == null) {
                    LOGGER.error("Could not find any precincts for CSV County name " + county);
                    continue;
                }

                int totalCountyPopulation = countyNameToTotalPopulationMap.get(county);
                if(county == null) {
                    LOGGER.error("Could not find total county population from CSV county name " + county);
                    continue;
                }

                for(Precinct countyPrecinct : countyPrecincts) {
                    // the total population here is from the geojson
                    double precinctPopulationShare = (double)countyPrecinct.getDemographic().getTotalPopulation() / totalCountyPopulation;
                    Demographic precinctDemographic = countyPrecinct.getDemographic();

                    precinctDemographic.setWhitePopulation( (int)Math.round(whitePopulation * precinctPopulationShare) )
                                       .setBlackPopulation( (int)Math.round(blackPopulation * precinctPopulationShare) )
                                       .setAmericanIndianPopulation( (int)Math.round(nativeAmericanPopulation * precinctPopulationShare) )
                                       .setAsianPopulation( (int)Math.round(asianPopulation * precinctPopulationShare) )
                                       .setPacificIslanderPopulation( (int)Math.round(pacificIslanderPopulation * precinctPopulationShare) )
                                       .setTwoOrMorePopulation( (int)Math.round(twoOrMoreRacesPopulation * precinctPopulationShare) )
                                       .setOtherPopulation( (int)Math.round(otherRacePopulation * precinctPopulationShare) );

                    precinctDemographic.updateTotalPopulation();

                }
            }

            csvReader.close();
        } catch (IOException e) {
            LOGGER.error("Couldn't read demographics file.");
            e.printStackTrace();
        }

        for(CongressionalDistrict district : state.getCongressionalDistricts()) {
            district.updateDemographics();
        }


        stateRepository.save(state);

        return ResponseEntity.status(HttpStatus.OK).body(Collections.singletonMap("message", stateName + " was successfully added."));
    }



}
