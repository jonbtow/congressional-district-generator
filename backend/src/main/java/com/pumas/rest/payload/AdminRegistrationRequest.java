package com.pumas.rest.payload;

import com.pumas.model.auth.Admin;

import java.io.Serializable;

public class AdminRegistrationRequest implements Serializable {

    private static final long serialVersionUID = 669883800130997040L;

    private String firstName;
    private String lastName;
    private String email;
    private String password;

    public AdminRegistrationRequest(String firstName, String lastName, String email, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public AdminRegistrationRequest setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public AdminRegistrationRequest setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public AdminRegistrationRequest setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public AdminRegistrationRequest setPassword(String password) {
        this.password = password;
        return this;
    }
}
