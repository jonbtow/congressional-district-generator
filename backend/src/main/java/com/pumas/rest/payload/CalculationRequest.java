package com.pumas.rest.payload;

import com.pumas.algorithm.CalculationRequestType;
import com.pumas.algorithm.UserPreferences;
import com.pumas.model.Constraint;
import com.pumas.model.Measure;

import java.io.Serializable;
import java.util.Set;

public class CalculationRequest implements Serializable {

    private static final long serialVersionUID = 6694988268762738240L;

    private CalculationRequestType requestType = CalculationRequestType.START;

    private String stateName;
    private int year;

    private double schwartzbergCompactnessWeight = 0.0;
    private double reockCompactnessWeight = 0.0;
    private double polsbyPopperCompactnessWeight = 0.0;
    private double convexHullCompactnessWeight = 0.0;
    private double efficiencyGapWeight = 0.0;
    private double racialFairnessWeight = 0.0;

    // TODO booleans here should be renamed to isContigious, etc.
    private boolean equalPopulationConstraint = false;
    private boolean contiguityConstraint = false;
    private boolean preserveCommunitiesConstraint = false;

    private Set<String> unmovablePrecincts;
    private Set<String> unmovableDistricts;

    public CalculationRequest(CalculationRequestType requestType, String stateName, int year, double schwartzbergCompactnessWeight, double reockCompactnessWeight, double polsbyPopperCompactnessWeight, double convexHullCompactnessWeight, double efficiencyGapWeight, double racialFairnessWeight, boolean equalPopulationConstraint, boolean contiguityConstraint, boolean preserveCommunitiesConstraint, Set<String> unmovablePrecincts, Set<String> unmovableDistricts) {
        this.requestType = requestType;
        this.stateName = stateName;
        this.year = year;
        this.schwartzbergCompactnessWeight = schwartzbergCompactnessWeight;
        this.reockCompactnessWeight = reockCompactnessWeight;
        this.polsbyPopperCompactnessWeight = polsbyPopperCompactnessWeight;
        this.convexHullCompactnessWeight = convexHullCompactnessWeight;
        this.efficiencyGapWeight = efficiencyGapWeight;
        this.racialFairnessWeight = racialFairnessWeight;
        this.equalPopulationConstraint = equalPopulationConstraint;
        this.contiguityConstraint = contiguityConstraint;
        this.preserveCommunitiesConstraint = preserveCommunitiesConstraint;
        this.unmovablePrecincts = unmovablePrecincts;
        this.unmovableDistricts = unmovableDistricts;
    }

    public String getStateName() {
        return stateName;
    }

    public CalculationRequest setStateName(String stateName) {
        this.stateName = stateName;
        return this;
    }

    public double getSchwartzbergCompactnessWeight() {
        return schwartzbergCompactnessWeight;
    }

    public CalculationRequest setSchwartzbergCompactnessWeight(double schwartzbergCompactnessWeight) {
        this.schwartzbergCompactnessWeight = schwartzbergCompactnessWeight;
        return this;
    }

    public double getReockCompactnessWeight() {
        return reockCompactnessWeight;
    }

    public CalculationRequest setReockCompactnessWeight(double reockCompactnessWeight) {
        this.reockCompactnessWeight = reockCompactnessWeight;
        return this;
    }

    public double getPolsbyPopperCompactnessWeight() {
        return polsbyPopperCompactnessWeight;
    }

    public CalculationRequest setPolsbyPopperCompactnessWeight(double polsbyPopperCompactnessWeight) {
        this.polsbyPopperCompactnessWeight = polsbyPopperCompactnessWeight;
        return this;
    }

    public double getConvexHullCompactnessWeight() {
        return convexHullCompactnessWeight;
    }

    public CalculationRequest setConvexHullCompactnessWeight(double convexHullCompactnessWeight) {
        this.convexHullCompactnessWeight = convexHullCompactnessWeight;
        return this;
    }

    public double getEfficiencyGapWeight() {
        return efficiencyGapWeight;
    }

    public CalculationRequest setEfficiencyGapWeight(double efficiencyGapWeight) {
        this.efficiencyGapWeight = efficiencyGapWeight;
        return this;
    }

    public boolean isContiguityConstraint() {
        return contiguityConstraint;
    }

    public CalculationRequest setContiguityConstraint(boolean contiguityConstraint) {
        this.contiguityConstraint = contiguityConstraint;
        return this;
    }

    public boolean isPreserveCommunitiesConstraint() {
        return preserveCommunitiesConstraint;
    }

    public CalculationRequest setPreserveCommunitiesConstraint(boolean preserveCommunitiesConstraint) {
        this.preserveCommunitiesConstraint = preserveCommunitiesConstraint;
        return this;
    }

    public int getYear() {
        return year;
    }

    public CalculationRequest setYear(int year) {
        this.year = year;
        return this;
    }

    public double getRacialFairnessWeight() {
        return racialFairnessWeight;
    }

    public CalculationRequest setRacialFairnessWeight(double racialFairnessWeight) {
        this.racialFairnessWeight = racialFairnessWeight;
        return this;
    }

    public boolean isEqualPopulationConstraint() {
        return equalPopulationConstraint;
    }

    public CalculationRequest setEqualPopulationConstraint(boolean equalPopulationConstraint) {
        this.equalPopulationConstraint = equalPopulationConstraint;
        return this;
    }

    public CalculationRequestType getRequestType() {
        return requestType;
    }

    public CalculationRequest setRequestType(CalculationRequestType requestType) {
        this.requestType = requestType;
        return this;
    }

    public Set<String> getUnmovablePrecincts() {
        return unmovablePrecincts;
    }

    public CalculationRequest setUnmovablePrecincts(Set<String> unmovablePrecincts) {
        this.unmovablePrecincts = unmovablePrecincts;
        return this;
    }

    public Set<String> getUnmovableDistricts() {
        return unmovableDistricts;
    }

    public CalculationRequest setUnmovableDistricts(Set<String> unmovableDistricts) {
        this.unmovableDistricts = unmovableDistricts;
        return this;
    }

    public UserPreferences getUserPreferences() {
        UserPreferences userPreferences = new UserPreferences(this.getStateName());
        userPreferences.setYear(this.getYear());

        if(this.getSchwartzbergCompactnessWeight() != 0) {
            userPreferences.addWeight(Measure.SCHWARTZBERG_COMPACTNESS, this.getSchwartzbergCompactnessWeight());
        }
        if(this.getReockCompactnessWeight() != 0) {
            userPreferences.addWeight(Measure.REOCK_COMPACTNESS, this.getReockCompactnessWeight());
        }
        if(this.getPolsbyPopperCompactnessWeight() != 0) {
            userPreferences.addWeight(Measure.POLSBY_POPPER_COMPACTNESS, this.getPolsbyPopperCompactnessWeight());
        }
        if(this.getConvexHullCompactnessWeight() != 0) {
            userPreferences.addWeight(Measure.CONVEX_HULL_COMPACTNESS, this.getConvexHullCompactnessWeight());
        }
        if(this.getEfficiencyGapWeight() != 0) {
            userPreferences.addWeight(Measure.EFFICIENCY_GAP, this.getEfficiencyGapWeight());
        }
        if(this.getRacialFairnessWeight() != 0) {
            userPreferences.addWeight(Measure.RACIAL_FAIRNESS, this.getRacialFairnessWeight());
        }

        if(this.isEqualPopulationConstraint()) {
            userPreferences.addConstraint(Constraint.EQUAL_POPULATION, this.isEqualPopulationConstraint());
        }
        if(this.isContiguityConstraint()) {
            userPreferences.addConstraint(Constraint.CONTIGUITY, this.isContiguityConstraint());
        }
        if(this.isPreserveCommunitiesConstraint()) {
            userPreferences.addConstraint(Constraint.PRESERVE_POLITICAL_COMMUNITY, this.isPreserveCommunitiesConstraint());
        }

        if(this.getUnmovableDistricts() != null) {
            userPreferences.setUnmovablePrecincts(this.getUnmovablePrecincts());
        }
        if(this.getUnmovablePrecincts() != null) {
            userPreferences.setUnmovableDistricts(this.getUnmovableDistricts());
        }

        return userPreferences;
    }

}
