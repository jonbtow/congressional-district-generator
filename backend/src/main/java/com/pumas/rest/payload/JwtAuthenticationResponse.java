package com.pumas.rest.payload;

import java.io.Serializable;

/**
 * Created by Jaspreet on 4/8/2018.
 */
public class JwtAuthenticationResponse implements Serializable {

    private static final long serialVersionUID = 1250166508152483573L;

    private final String token;
    private final String email;

    public JwtAuthenticationResponse(String token, String email) {
        this.token = token;
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public String getEmail() {
        return email;
    }
}
