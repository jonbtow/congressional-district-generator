package com.pumas.rest.payload;

import com.pumas.model.auth.GeneralUser;

import java.io.Serializable;

public class UserRegistrationRequest implements Serializable {

    private static final long serialVersionUID = 7808639016075964201L;

    private String email;
    private String password;

    public UserRegistrationRequest(String firstName, String lastName, String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public UserRegistrationRequest setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public UserRegistrationRequest setPassword(String password) {
        this.password = password;
        return this;
    }
}
