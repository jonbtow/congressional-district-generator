package com.pumas.security;

/**
 * Created by Jaspreet on 4/8/2018.
 */
public class AuthenticationException extends RuntimeException {
    public AuthenticationException(String message, Throwable cause) {
        super(message, cause);
    }
}
