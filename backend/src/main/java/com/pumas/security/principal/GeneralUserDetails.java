package com.pumas.security.principal;

import com.pumas.model.auth.GeneralUser;
import com.pumas.model.auth.Role;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Identifies general user authentication principal for Spring Security.
 */
public class GeneralUserDetails extends GeneralUser implements UserDetails {

    public GeneralUserDetails(GeneralUser generalUser) {
        super(generalUser);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        final Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        grantedAuthorities.add(new SimpleGrantedAuthority(Role.RoleType.ROLE_USER.toString()));
        return grantedAuthorities;
    }

    @Override
    public String getUsername() {
        return super.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

}
