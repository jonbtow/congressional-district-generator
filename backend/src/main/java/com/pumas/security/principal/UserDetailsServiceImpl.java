package com.pumas.security.principal;

import com.pumas.model.auth.Admin;
import com.pumas.model.auth.GeneralUser;
import com.pumas.repositories.AdminRepository;
import com.pumas.repositories.GeneralUserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * Created by Jaspreet on 4/8/2018.
 */
@Component
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private GeneralUserRepository generalUserRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        GeneralUser generalUser = generalUserRepository.findByEmail(email);

        if(generalUser == null) {

            Admin admin = adminRepository.findByEmail(email);
            if(admin == null) {
                LOGGER.error("username not found");
                throw new UsernameNotFoundException("Invalid user");
            } else {
                return new AdminDetails(admin);
            }

        } else {
            return new GeneralUserDetails(generalUser);
        }

    }

}
