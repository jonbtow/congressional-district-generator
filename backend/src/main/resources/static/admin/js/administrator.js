$(function () {

    var urlAdmins = "http://localhost:8090/api/accounts/admins";
    var urlAdmin = "http://localhost:8090/api/accounts/admin";


    $("#adminGrid").jsGrid({
        height: "auto",
        width: "95%",
        inserting: true,
        editing: true,
        sorting: true,
        autoload: true,
        paging: true,
        pageSize: 10,
        controller: {
            loadData: function (filter) {
                return $.ajax({
                    type: "GET",
                    url: "http://localhost:8090/api/accounts/admins",
                    data: filter
                });
            },
            insertItem: function (item) {
                console.log(item);
                return $.ajax({
                    type: "POST",
                    contentType: "application/json",
                    url: "http://localhost:8090/api/accounts/admin",
                    data: JSON.stringify(item),
                    success: function(data)
                    {
                        $("#adminGrid").jsGrid("loadData");
                    }
                });
            },
            updateItem: function (item) {
                return $.ajax({
                    type: "POST",
                    contentType: "application/json",
                    url: "http://localhost:8090/api/accounts/admin/modify",
                    data: JSON.stringify(item),
                    success: function(data)
                    {
                        $("#adminGrid").jsGrid("loadData");
                    }
                });
            },
            deleteItem: function (item) {
                return $.ajax({
                    type: "POST",
                    url: "http://localhost:8090/api/accounts/admin/modify",
                    data: JSON.stringify({'userId':item.id}),
                    contentType: "application/json",
                    success: function(data)
                    {
                        $("#adminGrid").jsGrid("loadData");
                    }
                });
            }
        },
        fields: [
            {name: "email", title: "EMAIL", type: "text", width: 200},
            {name: "password", title: "PASSWORD", type: "text",  width: 50},
            { type: "control" }
        ]
    });

});