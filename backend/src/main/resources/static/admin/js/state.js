$(function () {

    var urlStates = "http://localhost:8090/api/states/all/names";
    var urlAdmin = "http://localhost:8090/api/accounts/admin";


    $("#stateGrid").jsGrid({
        height: "auto",
        width: "95%",
        inserting: false,
        sorting: true,
        autoload: true,
        paging: true,
        pageSize: 10,
        controller: {
            loadData: function (filter) {

                var stateData = [];
                var deferred = $.Deferred();
                $.ajax({
                    type: "GET",
                    url: urlStates,
                    data: filter,
                    success: function (data)
                    {
                        var d = data;
                        console.log(data);
                        for(var k in d){
                            temp = {
                                'state': k,
                                'year': d[k] + ""
                            };
                            stateData.push(temp);
                        }
                        //stateData = JSON.stringify(stateData);
                        console.log(stateData);
                        deferred.resolve(stateData);
                    }
                });
                return deferred.promise();
            },
            insertItem: function (item) {
                console.log(item);
                return $.ajax({
                    type: "POST",
                    contentType: "application/json",
                    url: urlAdmin,
                    data: JSON.stringify(item),
                    success: function (data) {
                        $("#stateGrid").jsGrid("loadData");
                    }
                });
            }
            },
        fields: [
            {name: "state", title: "STATE", type: "text", width: 150},
            {name: "year", title: "YEAR", type: "text",  width: 50}

        ]
        });

    $('#state-form').on("submit", upload);

    function upload() {


        var formData = new FormData($('#state-form'));
        var xhr = new XMLHttpRequest();
        xhr.open('POST', "http://localhost:8090/api/states", true);
        xhr.send(formData);

        $('#message').show();


    }

});