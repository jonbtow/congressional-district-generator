
$(function () {

    var jsonPromise = $.Deferred();
    $.ajax({
        type: "GET",
        url: "http://localhost:8090/api/saved/all/redistrictings",
        success: function (data) {
            jsonPromise.resolve(data);
        }
    });

    jsonPromise.done(function (json) {
        // For a pie chart
        console.log(JSON.stringify(json));
        var iowa = 0;
        var lou = 0;
        var penn = 0;
        $.each(json, function(i, item) {
            if(json[i].stateName == "Iowa")
                iowa = iowa+1;
            if(json[i].stateName == "Pennsylvania")
                penn = penn+1;
            if(json[i].stateName == "Louisiana")
                lou = lou+1;
        });

        data = {
            labels : ["Iowa", "Pennsylvania", "Louisiana"],
            datasets: [{
                backgroundColor: [
                    '#ff6384',
                    '#36a2eb',
                    '#cc65fe',
                ],
                data: [iowa, penn, lou]
            }],

            // These labels appear in the legend and in the tooltips when hovering different arcs
            labels: [
                'Iowa',
                'Pennsylvania',
                'Louisiana'
            ]
        };
        var ctx = document.getElementById('piechart1').getContext('2d');
        var pie = new Chart(ctx,{
            type: 'doughnut',
            data: data,
            options: {}
        });

        data2 = {
            labels : ["Iowa", "Pennsylvania", "Louisiana"],
            datasets: [{
                label: "Users",
                backgroundColor: [
                    '#ff6384',
                    '#36a2eb',
                    '#cc65fe',
                ],
                data: [iowa, penn, lou]
            }],


        };

        var ctx1 = document.getElementById('piechart2').getContext('2d');
        var pie2 = new Chart(ctx1,{
            type: 'bar',
            data: data2,
            options: {}
        });
    })

});