$(function () {

    var urlUsers = "http://localhost:8090/api/accounts/users";

    var urlUser = "http://localhost:8090/api/accounts/user/delete";


        $("#userGrid").jsGrid({
            height: "auto",
            width: "95%",
            inserting: true,
            editing: true,
            sorting: true,
            autoload: true,
            paging: true,
            pageSize: 10,
            controller: {
                loadData: function (filter) {
                    return $.ajax({
                        type: "GET",
                        url: urlUsers,
                        data: filter
                    });
                },
                insertItem: function (item) {
                    console.log(item);
                    return $.ajax({
                        type: "POST",
                        contentType: "application/json",
                        url: "http://localhost:8090/api/accounts/user",
                        data: JSON.stringify(item),
                        success: function(data)
                        {
                            $("#userGrid").jsGrid("loadData");
                        }
                    });
                },
                updateItem: function (item) {
                    return $.ajax({
                        type: "POST",
                        contentType: "application/json",
                        url: "http://localhost:8090/api/accounts/user/modify",
                        data: JSON.stringify(item),
                        success: function(data)
                        {
                            $("#userGrid").jsGrid("loadData");
                        }
                    });
                },
                deleteItem: function (item) {
                    console.log("xxxxxxxxxx");
                    return $.ajax({
                        type: "POST",
                        url: "http://localhost:8090/api/accounts/user/delete/"+item.id,
                        success: function(data)
                        {
                            console.log("rrrr");
                            $("#userGrid").jsGrid("loadData");
                        }
                    });
                }
            },
            fields: [
                {name: "email", title: "EMAIL", type: "text", width: 200},
                {name: "password", title: "PASSWORD", type: "text",  width: 50},
                { type: "control" }
            ]
        });

});