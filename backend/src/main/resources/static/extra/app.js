var stompClient = null;



function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
}

function login() {
    loginInfo = {
		email: $("#email").val(),
		password: $("#password").val()
	};

	$.ajax({
	  type: "POST",
	  url: "http://localhost:8090/api/auth/login",
	  data: JSON.stringify(loginInfo),
	  success: function(auth) { connect(auth) },
	  contentType: "application/json"
	})
}

function connect(auth) {
    var socket = new SockJS('/calculations?Authorization=Bearer ' + auth.token);
    stompClient = Stomp.over(socket);
	console.log("over");
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/user/queue/calculations', function (event) {
            showGreeting(JSON.stringify(JSON.parse(event.body)));

        });
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendName(command, weight) {
    stompClient.send("/app/calculations", {}, JSON.stringify({'requestType': command,
    "stateName":"Iowa",
    "year":2010,
    "reockCompactnessWeight":weight
    }));
}

function clearTable() {
    $("#greetings").html("");
}

function showGreeting(message) {
        $("#greetings").append("<tr><td>"+message+"</td></tr>");
    }

$(function () {

	
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { login(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#start" ).click(function() { sendName("START", 50); });
    $( "#pause" ).click(function() { sendName("PAUSE"); });
    $( "#resume" ).click(function() { sendName("RESUME"); });
    $( "#stop" ).click(function() { sendName("STOP"); });
});