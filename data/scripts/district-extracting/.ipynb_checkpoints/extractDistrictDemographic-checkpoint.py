import json

def extractDistrictDemographic(districtID_map, cdFile_str, stateName_str, year):
    '''
    -stateFile_str: the geojson file of ALL states
    -stateName_str: the name of the desired state, first char uppercase
                    e.g. "Iowa"
    -year: year the data came from
    '''
    with open(cdFile_str) as json_file:
        json_data = json.load(json_file)

    features = json_data['features']
    districts = {}
    districts['type'] = 'FeatureCollection'
    
    prop = {}
    prop['type'] = 'name'
    properties = {}
    properties['name'] = 'urn:ogc:def:crs:OGC:1.3:CRS84'    
    prop['properties'] = properties
    districts['crs'] = prop
        
    districts['features'] = []
    d = {}
    for feature in features:
        properties = feature['properties']
        if properties['GEOID10'] in districtID_map.keys():
            newProps = {}
            # Population
            newProps['STATE'] = stateName_str
            newProps['CDFP'] = properties['GEOID10'][2:]
            newProps['NAME'] = properties['NAMELSAD10']
            newProps['YEAR'] = year
            newProps['GEOID'] = properties['GEOID10']
            newProps['INTPTLAT'] = properties['INTPTLAT10']
            newProps['INTPTLON'] = properties['INTPTLON10']
            dm = districtID_map[properties['GEOID10']]
            newProps['REPRESENTATIVE'] = dm['REPRESENTATIVE']
            newProps['TOTAL_POPULATION'] = properties['DP0010001']
            # Election
            newProps['REP_VOTES'] = dm['REP_VOTES']
            newProps['DEM_VOTES'] = dm['DEM_VOTES']
            newProps['OTHER_VOTES'] =dm['OTHER_VOTES']     
            # Economic 
            newProps['MEDIAN_INCOME'] = dm['MEDIAN_INCOME']
            newProps['MEAN_INCOME'] = dm['MEAN_INCOME']
            # Race
            newProps['WHITE_ALONE'] = properties['DP0090001']
            newProps['BLACK_ALONE'] = properties['DP0090002']
            newProps['AMERICAN_INDIAN_ALASKAN_ALONE'] = properties['DP0090003']
            newProps['ASIAN_ALONE'] = properties['DP0090004']
            newProps['PACIFIC_ISLANDER_ALONE'] = properties['DP0090005']
            newProps['OTHER_ALONE'] = properties['DP0090006']
            newProps['LENG'] = properties['Shape_Leng']
            newProps['AREA'] = properties['Shape_Area']

            feats = {}
            feats['type']= 'Feature'
            feats['properties'] = newProps
            feats['geometry'] = feature['geometry']
            districts['features'].append(feats)
            
    cdFile = stateName_str+'-District' + '-' + str(year) + '.geojson'
    with open(cdFile, 'w') as stateDistrictFile:
        json.dump(districts, stateDistrictFile)

def extractAll(stateNameList, fileName, year):
    for state in stateNameList:
        extractDistrictDemographic(fileName, state, year)

#extractAll(['Iowa', 'Louisiana', 'Pennsylvania'], 'State-2010.geojson', 2010)

extractDistrictDemographic(
    {           
    '1901': {'REP_VOTES': '156,980',
             'DEM_VOTES': '227,310',
             'OTHER_VOTES': '7,525',
             'REPRESENTATIVE': 'Bruce Braley',
             'MEDIAN_INCOME':'$57,402',      
             'MEAN_INCOME':'$75,270'},
    '1902': {'REP_VOTES': '159,959',
             'DEM_VOTES': '219,565',
             'OTHER_VOTES': '7,017',
             'REPRESENTATIVE': 'Dave Loebsack',
             'MEDIAN_INCOME':'$52,150',
             'MEAN_INCOME':'$68,401'},
    '1903': {'REP_VOTES': '173,967',
             'DEM_VOTES': '197,112',
             'OTHER_VOTES': '6,684',
             'REPRESENTATIVE': 'Leonard Boswell',
             'MEDIAN_INCOME':'$62,323',
             'MEAN_INCOME':'$82,158'},
    '1904': {'REP_VOTES': '191,473',
             'DEM_VOTES': '184,953',
             'OTHER_VOTES': '5,631',
             'REPRESENTATIVE': 'Tom Latham',
             'MEDIAN_INCOME':'$53,335',
             'MEAN_INCOME':'$68,431'},
    '1905': {'REP_VOTES': '128,363', 
             'DEM_VOTES': '63,160', 
             'OTHER_VOTES': '3,716',
             'REPRESENTATIVE': 'Steve King',
             'MEDIAN_INCOME':'$63,335',
             'MEAN_INCOME':'$70,200',
     }
}, './original/CD111.geojson', 'Iowa', 2010)


extractDistrictDemographic({       
    '4201': {'REP_VOTES': '64,135',
         'DEM_VOTES': '245,500',
         'OTHER_VOTES': '0',
         'REPRESENTATIVE': 'Bob Brady',
         'MEDIAN_INCOME':'$45,025',
         'MEAN_INCOME':'$64,554'},
    '4202': {'REP_VOTES': '33,390',
         'DEM_VOTES': '347,855',
         'OTHER_VOTES': '0',
         'REPRESENTATIVE': 'Chaka Fattah',
             'MEDIAN_INCOME':'$41,320',
             'MEAN_INCOME':'$70,186'},
    '4203': {'REP_VOTES': '169,166',
         'DEM_VOTES': '149,873',
         'OTHER_VOTES': '0',
         'REPRESENTATIVE': 'Mike Kelly',
               
             'MEDIAN_INCOME':'$51,289',
             'MEAN_INCOME':'$68,983'},
    '4204': {'REP_VOTES': '172,940',
         'DEM_VOTES': '144,854',
         'OTHER_VOTES': '0',
         'REPRESENTATIVE': 'Jason Altmire',
               
             'MEDIAN_INCOME':'$61,333',
             'MEAN_INCOME':'$76,333'},
    '4205': {'REP_VOTES': '160,134',
         'DEM_VOTES': '146,536',
         'OTHER_VOTES': '0',
         'REPRESENTATIVE': 'Glenn Thompson',
               
             'MEDIAN_INCOME':'$48,410',
             'MEAN_INCOME':'$48,410'},
    '4206': {'REP_VOTES': '161,570',
         'DEM_VOTES': '186,460',
         'OTHER_VOTES': '0',
         'REPRESENTATIVE': 'Jim Gerlach',
               
             'MEDIAN_INCOME':'$82,139',
             'MEAN_INCOME':'$108,952'},
    '4207': {'REP_VOTES': '179,384',
         'DEM_VOTES': '192,275',
         'OTHER_VOTES': '0',    
         'REPRESENTATIVE': 'Vacant',
          'MEDIAN_INCOME':'$85,881',
          'MEAN_INCOME':'$115,233'},
    '4208': {'REP_VOTES': '198,505',
         'DEM_VOTES': '170,472',
         'OTHER_VOTES': '0',
         'REPRESENTATIVE': 'Brian Fitzpatrick',
                'MEDIAN_INCOME':'$80,199',
             'MEAN_INCOME':'$106,208'},

    '4209': {'REP_VOTES': '170,305',
         'DEM_VOTES': '120,433',
         'OTHER_VOTES': '0',
         'REPRESENTATIVE': 'Bill Shuster', 
               
             'MEDIAN_INCOME':'$47,697',
             'MEAN_INCOME':'$61,868'},
    '4210': {'REP_VOTES': '169,324',
         'DEM_VOTES': '126,925',
         'OTHER_VOTES': '0',
         'REPRESENTATIVE': 'Tom Marino',
               
             'MEDIAN_INCOME':'$52,199',
             'MEAN_INCOME':'$67,747'},
    '4211': {'REP_VOTES': '159,469',
         'DEM_VOTES': '145,211',
         'OTHER_VOTES': '0',
         'REPRESENTATIVE': 'Lou Barletta',
               
             'MEDIAN_INCOME':'$53,833',
             'MEAN_INCOME':'$69,291'},
    '4212': {'REP_VOTES': '195,077',
         'DEM_VOTES': '161,241',
         'OTHER_VOTES': '0',
         'REPRESENTATIVE': 'Keith Rothfus',
             'MEDIAN_INCOME':'$60,387',
             'MEAN_INCOME':'$81,370'},
    '4213': {'REP_VOTES': '112,243',
         'DEM_VOTES': '217,661',
         'OTHER_VOTES': '0',
         'REPRESENTATIVE': 'Brendan Boyle',
               
             'MEDIAN_INCOME':'$60,105',
             'MEAN_INCOME':'$80,675'},
    '4214': {'REP_VOTES': '116,994',
         'DEM_VOTES': '245,432',
         'OTHER_VOTES': '0',
         'REPRESENTATIVE': 'Michale Doyle',
               
             'MEDIAN_INCOME':'$44,939',
             'MEAN_INCOME':'$61,962'},
    '4215': {'REP_VOTES': '148,385',
         'DEM_VOTES': '165,836',
         'OTHER_VOTES': '0',
         'REPRESENTATIVE': 'Charlie Dent',
               
             'MEDIAN_INCOME':'$62,755',
             'MEAN_INCOME':'$83,440'},
    '4216': {'REP_VOTES': '148,489',
         'DEM_VOTES': '152,095',
         'OTHER_VOTES': '0',
         'REPRESENTATIVE': 'Lloyd Smucker',
               
             'MEDIAN_INCOME':'$57,144',
             'MEAN_INCOME':'$76,620'},
    '4217': {'REP_VOTES': '128,440',
         'DEM_VOTES': '174,446',
         'OTHER_VOTES': '0',
         'REPRESENTATIVE': 'Michael Cartwright',
               
             'MEDIAN_INCOME':'$51,635',
             'MEAN_INCOME':'$66,374'},
    '4218': {'REP_VOTES': '195,970',
         'DEM_VOTES': '155,226',
         'OTHER_VOTES': '0',    
         'REPRESENTATIVE': 'Joe Pitts',
          'MEDIAN_INCOME':'$63,825',
          'MEAN_INCOME':'$83,856'},
    '4219': {'REP_VOTES': 'Unavailable', 
         'DEM_VOTES': 'Unavailable', 
         'OTHER_VOTES': 'Unavailable',    
         'REPRESENTATIVE': 'Todd R. Platts',               
             'MEDIAN_INCOME':'$63,856',
             'MEAN_INCOME':'$74,841'},    
}, './original/CD111.geojson', 'Pennsylvania', 2010)

extractDistrictDemographic({       
    '2201': {'REP_VOTES': '157,182', 
             'DEM_VOTES': '38,416', 
             'OTHER_VOTES': '4,578',
             'REPRESENTATIVE': 'Steve Scalise',
             'MEDIAN_INCOME':'$56,938',
             'MEAN_INCOME':'$79,470'},
    '2202': {'REP_VOTES': '83,705', 
             'DEM_VOTES': '43,378', 
             'OTHER_VOTES': '2,521',
             'REPRESENTATIVE': 'Cedric Richmond',
             'MEDIAN_INCOME':'$36,798',
             'MEAN_INCOME':'$57,958'},
    '2203': {'REP_VOTES':'108,963', 
             'DEM_VOTES': '61,914', 
             'OTHER_VOTES': '0',
             'REPRESENTATIVE': 'Jeff Landry',
             'MEDIAN_INCOME':'$44,668',
             'MEAN_INCOME':'$62,828'},
    '2204': {'REP_VOTES':'44,501', 
             'DEM_VOTES': '44,151', 
             'OTHER_VOTES': '3,870',
             'REPRESENTATIVE': 'John C. Fleming',
             'MEDIAN_INCOME':'$39,748',
             'MEAN_INCOME':'$58,002'},
    '2205': {'REP_VOTES': '78,211', 
             'DEM_VOTES': '33,233', 
             'OTHER_VOTES': '3,138',
             'REPRESENTATIVE': 'Rodney Alexander',
             'MEDIAN_INCOME':'$36,718',
             'MEAN_INCOME':'$54,376'},
    '2206': {'REP_VOTES':'150,332',
             'DEM_VOTES': '125,886',
             'OTHER_VOTES': '36,198',
             'REPRESENTATIVE': 'Bill Cassidy',
             'MEDIAN_INCOME':'$59,270',
             'MEAN_INCOME':'$79,923'},
    '2207': {'REP_VOTES':'177,173', 
             'DEM_VOTES': '98,280', 
             'OTHER_VOTES': '10,846',
             'REPRESENTATIVE': 'Charles Boustany',
             'MEDIAN_INCOME':'$52,255',
             'MEAN_INCOME':'$64,300'},
}, './original/CD111.geojson', 'Louisiana', 2010)
