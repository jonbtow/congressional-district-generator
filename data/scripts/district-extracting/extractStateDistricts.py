import json

def extractStateDistricts(cdFile_str, stateName_str, stateId_str):
    
    with open(cdFile_str) as json_file:
        json_data = json.load(json_file)

    features = json_data['features']
    state = {}
    state['type'] = 'FeatureCollection'
    
    prop = {}
    prop['type'] = 'name'
    properties = {}
    properties['name'] = 'urn:ogc:def:crs:OGC:1.3:CRS84'    
    prop['properties'] = properties
    state['crs'] = prop
    
    stateDistricts = []
    for feature in features:
        properties = feature['properties']
        if properties['STATEFP'] == stateId_str:
            stateDistricts.append(feature)
            state['features'] = stateDistricts

    stateFile = stateName_str + '.geojson'
    with open(stateFile, 'w') as stateDistrictFile:
        json.dump(state, stateDistrictFile)

extractStateDistricts('CD112-2012.json', 'PA','42')
