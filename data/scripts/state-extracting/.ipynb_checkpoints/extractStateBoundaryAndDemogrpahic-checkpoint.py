import json

def extractState(stateFile_str, stateName_str, year):
    '''
    -stateFile_str: the geojson file of ALL states
    -stateName_str: the name of the desired state, first char uppercase
                    e.g. "Iowa"
    '''
    with open(stateFile_str) as json_file:
        json_data = json.load(json_file)

    features = json_data['features']
    state = {}
    state['type'] = 'FeatureCollection'
    
    prop = {}
    prop['type'] = 'name'
    properties = {}
    properties['name'] = 'urn:ogc:def:crs:OGC:1.3:CRS84'    
    prop['properties'] = properties
    state['crs'] = prop

    state['features'] = []
    feats = {}
    feats['type']='Feature'
    for feature in features:
        properties = feature['properties']
        if properties['NAME10'] == stateName_str:
            newProps = {}
            # Population
            newProps['NAME'] = properties['NAME10']
            newProps['YEAR'] = year
            newProps['GEOID'] = properties['GEOID10']
            newProps['INTPTLAT'] = properties['INTPTLAT10']
            newProps['INTPTLON'] = properties['INTPTLON10']
            newProps['STUSPS'] = properties['STUSPS10']
            newProps['TOTAL_POPULATION'] = properties['DP0010001']
            # Race
            race = {}
            race['WHITE_ALONE'] = properties['DP0090001']
            race['BLACK_ALONE'] = properties['DP0090002']
            race['AMERICAN_INDIAN_ALASKAN_ALONE'] = properties['DP0090003']
            race['ASIAN_ALONE'] = properties['DP0090004']
            race['PACIFIC_ISLANDER_ALONE'] = properties['DP0090005']
            race['OTHER_ALONE'] = properties['DP0090006']
            newProps['RACE'] = race
            newProps['LENG'] = properties['Shape_Leng']
            newProps['AREA'] = properties['Shape_Area']
            feats['properties'] = newProps
            feats['geometry'] = feature['geometry']
            state['features'].append(feats)
            break

    stateFile = stateName_str + '.geojson'
    with open(stateFile, 'w') as stateDistrictFile:
        json.dump(state, stateDistrictFile)
        
extractState('State-2010.geojson', 'Iowa', year=2010)
extractState('State-2010.geojson', 'Louisiana', year=2010)
extractState('State-2010.geojson', 'Pennsylvania', year=2010)
    
    
