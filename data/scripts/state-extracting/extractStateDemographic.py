import json

def extractStateDemographic(stateFile_str, stateName_str, year):
    '''
    -stateFile_str: the geojson file of ALL states
    -stateName_str: the name of the desired state, first char uppercase
                    e.g. "Iowa"
    -year: year the data came from
    '''
    with open(stateFile_str) as json_file:
        json_data = json.load(json_file)

    features = json_data['features']
    state = {}
    
    for feature in features:
        properties = feature['properties']
        if properties['NAME10'] == stateName_str:
            break
    # Population
    state['YEAR'] = year
    state['TOTAL_POPULATION'] = properties['DP0010001']
    # Race
    race = {}
    race['WHITE_ALONE'] = properties['DP0090001']
    race['BLACK_ALONE'] = properties['DP0090002']
    race['AMERICAN_INDIAN_ALASKAN_ALONE'] = properties['DP0090003']
    race['ASIAN_ALONE'] = properties['DP0090004']
    race['PACIFIC_ISLANDER_ALONE'] = properties['DP0090005']
    race['OTHER_ALONE'] = properties['DP0090006']
    state['RACE'] = race
    
    stateFile = stateName_str + '.json'
    with open(stateFile, 'w') as stateDistrictFile:
        json.dump(state, stateDistrictFile)

def extractAll(stateNameList, fileName, year):
    for state in stateNameList:
        extractStateDemographic(fileName, state, year)

extractAll(['Iowa', 'Louisiana', 'Pennsylvania'], 'State-2010.geojson', 2010)
    
    
