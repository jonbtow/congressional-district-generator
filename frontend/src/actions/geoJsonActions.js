import API from "../config/api";


export const SET_GEO_TYPE = 'SET_GEO_TYPE';
export const INVALIDATE_GEO_JSON = 'INVALIDATE_GEO_JSON';
export const REQUEST_GEO_JSON = 'REQUEST_GEO_JSON';
export const RECIEVE_GEO_JSON = 'RECIEVE_GEO_JSON';

const STATE_URL = 'states/all/geometry';
const DISTRICT_URL = 'state/districts/geometry';
const PRECINCT_URL = 'state/precincts/geometry';

const STATE='STATE';
const DISTRICT='DISTRICT';
const PRECINCT='PRECINCT';

export function setGeoType(geoType) {
    return {
        type: SET_GEO_TYPE,
        geoType
    };
}

export function invalidateGeoType(geoType) {
    return {
        type: INVALIDATE_GEO_JSON,
        geoType
    };
}

export function requestGeoJson(geoType) {
    return {
        type: REQUEST_GEO_JSON,
        geoType
    }
}
export function recieveGeoJson(gt, d) {
    return {
        type: RECIEVE_GEO_JSON,
        geoType: gt,
        data: d,
        receivedAt: Date.now()
    }
}

/* Async get redistricting */
export function fetchGeoJson(geoType, url, statename, thisyear) {
    /* Call get with parameters */
    return dispatch => {
        dispatch(requestGeoJson(geoType));
        return API.get(url, {
            params: {
                stateName: statename,
                year: thisyear
            }
        })
            .then(res => {
                let featureCollection = res.data;
                dispatch(recieveGeoJson(geoType, featureCollection));
            })
            .catch(error => {
                console.log('Get Map Data Error: ' + error.response);
            });
    }
}