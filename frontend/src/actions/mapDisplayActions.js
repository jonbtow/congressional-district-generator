/* Constraint Action Types */
import API from "../config/api";
import {getRedistrictingSuccess} from "./redistrictingActions";

export const SET_DISPLAY_TYPE = 'SET_DISPLAY_TYPE';
export const SET_DISPLAY_STATE = 'SET_DISPLAY_STATE';
export const SET_P_TO_CD = 'SET_P_TO_CD';

export const STATE = 'State';
export const DISTRICT = 'District';
export const PRECINCT = 'Precinct';


/*
 * Action Creators
 */
export function setDisplayType(displayType) {
    return {
        type: SET_DISPLAY_TYPE,
        displayType
    };
}
export function setDisplayState(displayState) {
    return {
        type: SET_DISPLAY_STATE,
        displayState
    };
}

export function setPToCD(pToCD) {
    return {
        type: SET_P_TO_CD,
        pToCD
    };
}



