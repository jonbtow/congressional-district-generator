import API from '../config/api';

/* Redistricting Action Types */
export const SET_STATE_NAME = 'SET_STATE_NAME';

/* Year Action Types */
export const YEAR = 'YEAR';

/* Measure Action Types */
export const WEIGH_SCHWARTZBERG_COMPACTNESS = 'WEIGH_SCHWARTZBERG_COMPACTNESS';
export const WEIGH_REOCK_COMPACTNESS = 'WEIGH_REOCK_COMPACTNESS';
export const WEIGH_POLSBY_POPPER_COMPACTNESS = 'WEIGH_POLSBY_POPPER_COMPACTNESS';
export const WEIGH_CONVEX_HULL_COMPACTNESS = 'WEIGH_CONVEX_HULL_COMPACTNESS';
export const WEIGH_EFFICIENCY_GAP = 'WEIGH_EFFICIENCY_GAP';

/* Constraint Action Types */
export const TOGGLE_CONTIGUITY = 'TOGGLE_CONTIGUITY';
export const WEIGH_RACIAL_FAIRNESS = 'WEIGH_RACIAL_FAIRNESS';
export const TOGGLE_EQUAL_POPULATION = 'TOGGLE_EQUAL_POPULATION';

export const ADD_UNMOVABLE_PRECINCT = 'ADD_UNMOVABLE_PRECINCT';
export const ADD_UNMOVABLE_DISTRICT = 'ADD_UNMOVABLE_DISTRICT';


/* Sync load redistricting */
export function getRedistrictingSuccess(geojson) {
    return {
        type: 'GET_REDISTRICTING_SUCCESS',
        geojson
    }
}

/* Async get redistricting */
export function getRedistricting(redistricting) {
    /* Call get with parameters */
    return dispatch => {
        API.get('', {
            params: {
                redistricting: redistricting
            }
        }).then(response => {
                console.log('redistricting');

                dispatch(getRedistrictingSuccess(response.data));
            })
        .catch(error => {
            throw(error)
        })
    }
}

/*
 * Action Creators
 */

/* State Action Creator */
export function setStateName(stateName) {
    return {
        type: SET_STATE_NAME,
        stateName
    }
}

/**
 *  Unmovables Action Creators
 */
export function addUnmovablePrecinct(unmovablePrecincts) {
    return {
        type: ADD_UNMOVABLE_PRECINCT,
        unmovablePrecincts
    }
}
export function addUnmovableDistrict(unmovableDistricts){
    return {
        type: ADD_UNMOVABLE_DISTRICT,
        unmovableDistricts
    }
}

/**
 *  Constraint Action Creators
 */
export function toggleContiguity(constraint) {
    return {
        type: TOGGLE_CONTIGUITY,
        constraint
    }
}

export function weighRacialFairness(weight) {
    return {
        type: WEIGH_RACIAL_FAIRNESS,
        weight
    }
}

export function toggleEqualPopulation(constraint) {
    return {
        type: TOGGLE_EQUAL_POPULATION,
        constraint
    }
}
/**
 * weight Action Creators
 */


export function weighSchwartzbergCompactness(weight) {
    return {
        type: WEIGH_SCHWARTZBERG_COMPACTNESS,
        weight
    }
}

export function weighReockCompactness(weight) {
    return {
        type: WEIGH_REOCK_COMPACTNESS,
        weight
    }
}

export function weighPolsbyPopperComapctness(weight) {
    return {
        type: WEIGH_POLSBY_POPPER_COMPACTNESS,
        weight
    }
}


export function weighEfficiencyGap(weight) {
    return {
        type: WEIGH_EFFICIENCY_GAP,
        weight
    }
}