export const SET_STOMP_CLIENT = 'SET_STOMP_CLIENT';
export const SET_GEOJSON_UPDATE = 'SET_GEOJSON_UPDATE';

/*
 * Action Creators
 */
export function setStompClient(stompClient) {
    return {
        type: SET_STOMP_CLIENT,
        stompClient
    };
}
export function setGeoJsonUpdate(geoJsonUpdate) {
    return {
        type: SET_GEOJSON_UPDATE,
        geoJsonUpdate
    };
}
