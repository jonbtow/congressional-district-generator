import {fetch, setAuthToken} from './http'
import {push} from 'react-router-redux'

// Action types handled by this reducer
const LOGIN_REQUEST = 'LOGIN_REQUEST'
const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
const LOGIN_FAILURE = 'LOGIN_FAILURE'
const LOGOUT = 'LOGOUT'
const REGISTER_REQUEST = 'REGISTER_REQUEST'

export const actionTypes = {LOGIN_REQUEST, REGISTER_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE, LOGOUT}

// Action creators
export const loginRequest = () => ({
    type: LOGIN_REQUEST,
});

export const registerRequest = () => ({
    type: REGISTER_REQUEST
});

export const loginSuccess = ({email, token}) => ({
    type: LOGIN_SUCCESS,
    payload: {email, token},
});

export const loginFailure = err => ({
    type: LOGIN_FAILURE,
    payload: err,
});

export function login({email, password, redirect}) {
    return function (dispatch) {
        dispatch(loginRequest())
        return fetch('http://localhost:8090/api/auth/login', {
            method: 'POST',
            body: JSON.stringify({email, password}),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(function (response) {
                if (!response.ok) {
                    throw new Error(response.status)
                }

                dispatch(push('/'));
                return response.json();
            })
            .then(function (body) {
                console.log('Received response : ' + body)
                setAuthToken(body.token)
                dispatch(loginSuccess(body));
                redirect("/");
            })
            .catch(function (error) {
                console.log('caught error : ' + error.message)
                dispatch(loginFailure(error.message))
            })
    }
}

export function register({email, password, redirect}) {
    return function (dispatch) {
        dispatch(registerRequest())
        return fetch('http://localhost:8090/api/accounts/user', {
            method: 'POST',
            body: JSON.stringify({email, password}),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(function (response) {
                if (!response.ok) {
                    throw new Error(response.status)
                }

                dispatch(push('/'));
                return response.json()
            })
            .then(function (body) {
                console.log('Received response : ' + body);
                setAuthToken(body.token);
                dispatch(loginSuccess(body));
                redirect('/');
            })
            .catch(function (error) {
                console.log('caught error : ' + error.message)
                dispatch(loginFailure(error.message))
            })
    }
}

export function logout({redirect}) {
    return function (dispatch) {
        setAuthToken(null)
        dispatch({
            type: LOGOUT
        });
        redirect("/");
    }
}

// Reducer
const initialAuthState = {
    email: '',
};

export function authReducer(state = initialAuthState, action) {
    switch (action.type) {
        case LOGIN_REQUEST:
            return {
                ...state,
                loginInProgress: true,
            };
        case REGISTER_REQUEST:
            return {
                ...state,
                loginInProgress: true,
            };
        case LOGIN_SUCCESS:
            return {
                ...state,
                email: action.payload.email,
                loginInProgress: false,
                loginError: false,
            };
        case LOGIN_FAILURE:
            return {
                ...state,
                loginInProgress: false,
                loginError: action.payload,
            };
        case LOGOUT:
            return {...initialAuthState};
        default:
            return state;
    }
}