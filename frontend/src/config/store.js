import {createStore, compose, applyMiddleware} from 'redux'
import thunkMiddleWare from 'redux-thunk'

import reducers from '../reducers/index'

const devCompose = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

export const store = createStore(
    reducers, devCompose(applyMiddleware(thunkMiddleWare))
)
