import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux'
import {HashRouter} from "react-router-dom";

import App from './ui/container/App';
import {store} from './config/store.js'
import registerServiceWorker from './registerServiceWorker';

import './index.css';


ReactDOM.render(
    <Provider store={store}>
    <HashRouter>
      <App />
    </HashRouter>
    </Provider>,
    document.getElementById('root')
);

registerServiceWorker();
