import {INVALIDATE_GEO_JSON, RECIEVE_GEO_JSON, REQUEST_GEO_JSON, SET_GEO_TYPE} from "../actions/geoJsonActions";

import {US} from '../data-stub/US-STATE-2010-12';

const initialGeoJson = {
    geoType: '',
    data: US,
};

export function geoJsonReducer(state = initialGeoJson, action) {
    switch (action.type) {
        case SET_GEO_TYPE:
            return {
                ...state,
                geoType: action.geoType,
            }
        case RECIEVE_GEO_JSON:
            return {
                ...state,
                geoType: action.geoType,
                data: action.data,
            };
        default:
            return state
    }
}

