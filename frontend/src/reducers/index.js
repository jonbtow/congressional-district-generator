import {combineReducers} from 'redux';

import {authReducer} from '../auth/authReducer';
import {redistrictingReducer} from './redistrictingReducer';
import {mapDisplayReducer} from './mapDisplayReducer';
import {socketReducer} from './socketReducer';
import {geoJsonReducer} from './geoJsonReducer';

const reducers = combineReducers({
    auth: authReducer,
    redistricting: redistrictingReducer,
    mapDisplay: mapDisplayReducer,
    socket: socketReducer,
    geoJson: geoJsonReducer,
})

export default reducers;