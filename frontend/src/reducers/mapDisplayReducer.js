import {SET_DISPLAY_STATE, SET_DISPLAY_TYPE, SET_P_TO_CD, SET_GEO_JSON, STATE} from '../actions/mapDisplayActions'


const initialMapPreference = {
    displayState: '',
    displayType: STATE,
    pToCD: {},
};


export function mapDisplayReducer(state = initialMapPreference, action) {
    switch (action.type) {
        case SET_DISPLAY_TYPE:
            return {
                ...state,
                displayType: action.displayType,
            };
        case SET_DISPLAY_STATE:
            return {
                ...state,
                displayState: action.displayState,
            };
        case SET_P_TO_CD:
            return {
                ...state,
                pToCD: action.pToCD,
            };
        default:
            return state
    }
}
