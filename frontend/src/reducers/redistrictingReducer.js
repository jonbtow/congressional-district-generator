import {
    SET_STATE_NAME,
    TOGGLE_CONTIGUITY,
    WEIGH_RACIAL_FAIRNESS,
    TOGGLE_EQUAL_POPULATION,
    WEIGH_SCHWARTZBERG_COMPACTNESS,
    WEIGH_REOCK_COMPACTNESS,
    WEIGH_POLSBY_POPPER_COMPACTNESS,
    WEIGH_CONVEX_HULL_COMPACTNESS,
    WEIGH_EFFICIENCY_GAP,
    ADD_UNMOVABLE_DISTRICT,
    ADD_UNMOVABLE_PRECINCT,
} from '../actions/redistrictingActions'

const initialRedistrictingState = {
    stateName: '',

    schwartzbergCompactnessWeight: 0,
    reockCompactnessWeight : 0,
    polsbyPopperCompactnessWeight: 0,
    efficiencyGapWeight: 0,
    racialFairnessWeight: 0,
    contiguityConstraint: false,

    equalPopulationConstraint: false,

    unmovablePrecincts: [],
    unmovableDistricts: [],
};

export function redistrictingReducer(state = initialRedistrictingState, action) {
    switch (action.type) {
        case SET_STATE_NAME:
            return {
                ...state,
                stateName: action.stateName,
            };
        case ADD_UNMOVABLE_DISTRICT:
            return{
                ...state,
                unmovableDistricts: [...state.unmovableDistricts, action.unmovableDistricts],
            };
        case ADD_UNMOVABLE_PRECINCT:
          return {
              ...state,
              unmovablePrecincts: [...state.unmovablePrecincts, action.unmovablePrecincts],
          };
        case TOGGLE_EQUAL_POPULATION:
            return {
                ...state,
                equalPopulationConstraint: action.constraint,
            };
        case WEIGH_SCHWARTZBERG_COMPACTNESS:
            return {
                ...state,
                schwartzbergCompactnessWeight: action.weight,
            };
        case WEIGH_REOCK_COMPACTNESS:
            return {
                ...state,
                reockCompactnessWeight: action.weight,
            };
        case WEIGH_POLSBY_POPPER_COMPACTNESS:
            return {
                ...state,
                polsbyPopperCompactnessWeight: action.weight,
            };
        case WEIGH_EFFICIENCY_GAP:
            return {
                ...state,
                efficiencyGapWeight: action.weight,
            };
        case TOGGLE_CONTIGUITY:
            return {
                ...state,
                contiguityConstraint: action.constraint,
            };
        case WEIGH_RACIAL_FAIRNESS:
            return {
                ...state,
                racialFairnessWeight: action.weight,
            };
        default:
            return state
    }
}

 
