import {SET_GEOJSON_UPDATE, SET_STOMP_CLIENT} from "../actions/socketActions";

const initSocketState ={
    stompClient: null,
    geoJsonUpdate: {},
};

export function socketReducer(state = initSocketState, action) {
    switch (action.type) {
        case SET_STOMP_CLIENT:
            return {
                ...state,
                stompClient: action.stompClient,
            };
        case SET_GEOJSON_UPDATE:
            return {
                ...state,
                geoJsonUpdate: action.geoJsonUpdate
            };
        default:
            return state;
    }
}