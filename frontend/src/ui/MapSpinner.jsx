import {MapControl} from 'react-leaflet';
import L from 'leaflet';
import '../../scripts/leaflet.zoomhome';


class MapSpinner extends MapControl {

    createLeafletElement() {
        var usCenter = {
            lat: 37.0902,
            lng:  -95.7129,
            zoom: 4,
        };
        var zoom = L.Control.zoomHome({
            homeCoordinates: {
                lat: usCenter.lat,
                lng: usCenter.lng
            },
            homeZoom: 4
        });
        return zoom;
    }
}

export default MapSpinner;