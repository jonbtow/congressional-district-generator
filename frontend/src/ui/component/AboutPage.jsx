import React from 'react';

import {Page} from './Page';

import './style/Pages.css';


export class AboutPage extends React.Component {
    render() {
        return (
            <Page title={"About"}>
                <p> A Gerrymander is a voting district that is designed
                    to serve some political purpose. The name refers to both a salamander
                    and Eldridge Gerry, whose newly created voting district about 200 years
                    ago was said to resemble a salamander.
                </p>
                <p> Within the past 10 years, databases
                    for voter characterization as well as tools for precise map drawing have
                    made it possible to create congressional districts that favor the party
                    responsible for the creation of the districts. Redistricting is done in
                    states where census data requires a change in the number of delegates in
                    the state, and the 2010 census triggered redistricting in a number of
                    states.
                    Many of these redistricting efforts resulted in a shift in the political
                    representation in the states. As the realization of the impact of these changes
                    has grown, various technical approaches to the issue have been proposed, some
                    as quantitative measures of the presence of Gerrymandering, others as legal
                    challenges to redistricting, and yet others as draft bills in Congress to
                    minimize the effect of future redistricting.
                </p>
                <p>
                    This web application allows for the generation of congressional district
                    boundaries without any political influence.
                </p>
            </Page>
        );
    }
}
