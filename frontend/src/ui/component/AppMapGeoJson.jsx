import { GeoJSON } from 'react-leaflet';
import PropTypes from 'prop-types';

/* Updatable GeoJSON component for leaflet */
export class AppMapGeoJson extends GeoJSON {
  componentWillReceiveProps(prevProps) {
    if (prevProps.data !== this.props.data) {
      this.leafletElement.clearLayers();
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.data !== this.props.data) {
      this.leafletElement.addData(this.props.data);
    }
  }
}


AppMapGeoJson.propTypes = {
  data: PropTypes.array.isRequired,
};