import React from 'react';
import PropTypes from 'prop-types'


class ConstraintToggle extends React.Component {

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        const target = e.target;
        const checked = target.checked;
        this.props.onConstraintChange(checked);
    }

    render() {
        return (
            <div className="form-row pt-1 pb-1">
                <div className="col">
                    <div className="input-group bg-white  ">
                        <div className="input-group-prepend  pb-1">
                            <div className="input-group-text mt-2">
                                <label className="switch ">
                                    <input type="checkbox"
                                           className="form-check-input"
                                           checked={this.props.checked}
                                           onChange={this.handleChange}
                                    />
                                    <span className="slider round"></span>
                                </label>
                            </div>
                        </div>


                        <label className="mt-2 pt-1">
                            {this.props.name}
                        </label>
                    </div>
                </div>
            </div>
        );
    }
}

ConstraintToggle.propTypes = {
    name: PropTypes.string.isRequired,
    onConstraintChange: PropTypes.func.isRequired,
    checked: PropTypes.bool.isRequired,
}

export default ConstraintToggle
