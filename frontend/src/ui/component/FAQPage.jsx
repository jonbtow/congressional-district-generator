import React from 'react';

import {Page} from './Page';
import {FAQTopic} from './FAQTopic';

import './style/Pages.css';


export class FAQPage extends React.Component {
    render() {
        return (
            <Page title={"Help"}>
                <p>
                    The generation process of District Generator combines smaller districts
                    (e.g., polling districts) so that the
                    resulting congressional districts adhere to the goals or constraints identified
                    by Jacobson & Carson 1. Listed below are some broad definitions of the constraints
                    that can be applied in redistricting a state's congressional districts via the District
                    Generator application.
                </p>
                <FAQTopic name="Compactness">
                    One of the "traditional" redistricting principles, low compactness is considered to be a sign
                    of potential gerrymandering by courts, state law and the academic literature. More often than
                    not, though, compactness is ill-defined by the "I know it when I see it" standard. Geographers,
                    mathemeticians and political scientists have devised countless measures of compactness, each
                    representing a different conception, and some of these have found their way into law.
                    Used in this software are the following: <br/>
                    <ul style={{paddingTop: '10px'}}>
                        <li style={{ marginTop:'10px'}}><b>Reock</b>:
                    One of the first and most conceptually simple measures of
                    a district’s compactness was proposed by political scientist
                    E.C. Reock and takes its name from him. His method consists
                    of comparing the area of the district to the area of the
                    minimum spanning circle that can enclose the district
                        </li>
                        <li><b>Polsby-Popper</b>:
                            is perhaps the most common measure of compactness.
                            It represents the ratio of the area of a district to the
                            area of a circle with the same perimeter
                        </li>
                        <li>
                            <b>Schwartzberg</b>:
                            is the ratio
                            of the perimeter of a district to the perimeter of a circle with
                            equal area
                        </li>
                    </ul>
                </FAQTopic>
                <FAQTopic name="Contiguity">
                    Like compactness, contiguity is considered one of the "traditional" redistricting principles.
                    Most redistricting statutes mandate that districts be contiguous-- that is, they are a single,
                    unbroken shape. Two areas touching at their corners are typically not considered contiguous.
                    An obvious exception would be the inclusion of islands in a coastal district.
                </FAQTopic>
                <FAQTopic name="Equal Population">
                    All districts must have equal population or up to a ten percentage
                    point deviation, under certain circumstances.
                </FAQTopic>
                <FAQTopic name="Partisan Fairness (Efficiency Gap)">
                    The efficiency gap has recently been touted as a general partisan fairness measure.
                    The efficiency gap seeks to capture the difference in wasted votes between two parties in an election.
                    If a simple majority is needed to win an election, then every vote cast beyond 50% for the
                    winning candidate is a wasted vote. In addition, every vote cast for the losing candidate is a
                    wasted vote since losing votes also do not translate into an election win and might have been
                    parceled to a different district where it could have more efficiently been translated into a winning
                    seat.
                </FAQTopic>
            </Page>
        );
    }
}
