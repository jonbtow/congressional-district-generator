import React from 'react';
import PropTypes from 'prop-types'


export class FAQTopic extends React.Component {
    render() {
        return (
            <div className="row">
                <div className="col text-left p-3">
                    <h2 style={{color: '#3D88EC'}}>{this.props.name}</h2>
                    {this.props.children}
                </div>
            </div>
        );
    }
}

FAQTopic.propTypes = {
    name: PropTypes.string.isRequired,
};
