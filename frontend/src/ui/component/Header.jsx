import React, {Component} from 'react';
import {Nav} from './Nav';
import './style/Header.css';


export class Header extends Component {
    render() {
        return (
            <div className="container-fluid   dg-header">
                <div className="row ">
                    <div className="col ">
                        <Nav email={this.props.email} onLogout={this.props.onLogout}/>
                    </div>
                </div>
            </div>
        );

    }
}