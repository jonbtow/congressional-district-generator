import React from 'react';
import PropTypes from "prop-types";

class IconInputGroup extends React.Component {

    render() {
        return(
            <div className="input-group">
                <div className="input-group-prepend ">
                    <span className="input-group-text" id="inputGroupPrepend3">
                        <i className={this.props.icon} style={{color: '#378AE5'}}> </i>
                    </span>
                </div>
                {this.props.children}
            </div>
        );
    }
}

IconInputGroup.propTypes = {
    icon: PropTypes.string,
}

export default IconInputGroup