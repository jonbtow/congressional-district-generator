import {MapControl} from 'react-leaflet';
import {STATE, DISTRICT, PRECINCT} from '../../actions/mapDisplayActions';

import L from 'leaflet';

class MapInfo extends MapControl {

    createLeafletElement() {
        var info = L.control();

        info.onAdd = function (map) {
            this._div = L.DomUtil.create('div', 'info');
            this.update();
            return this._div;
        };

        // method that we will use to update the control based on feature properties passed
        info.update = function(props, displayType) {
            switch(displayType) {
                case STATE:
                    this._div.innerHTML = MapInfo.stateDisplay(props);
                    break;
                case DISTRICT:
                    this._div.innerHTML = MapInfo.districtDisplay(props);
                    break;
                case PRECINCT:
                    this._div.innerHTML = MapInfo.precinctDisplay(props);
                    break;
                default:
                    break;
            }
        };

        this.leafletElement = info;
        return info;
    }

    static stateDisplay(props) {
        let race = '';
        if (props && props.RACE ) {
            race =
                ' White: ' + (props.RACE.WHITE_ALONE ? props.RACE.WHITE_ALONE.toLocaleString() : 0) + '<br/>' +
                ' Black: ' +(props.RACE.BLACK_ALONE ? props.RACE.BLACK_ALONE.toLocaleString() : 0)  +'<br/>' +
                ' Asian: ' + (props.RACE.ASIAN_ALONE ? props.RACE.ASIAN_ALONE.toLocaleString() : 0) +'<br/>' +
                ' Native: ' + (props.RACE.AMERICAN_INDIAN_ALASKAN_ALONE ? props.RACE.AMERICAN_INDIAN_ALASKAN_ALONE.toLocaleString() : 0)   +'<br/>' +
                ' Other: ' +  (props.RACE.OTHER_ALONE ? props.RACE.OTHER_ALONE.toLocaleString() : 0) +'<br/>'
        }
        let html =
            (props ?
                '<h3>' + props.NAME  + (props.YEAR ? ' (' + props.YEAR + ')': '')+ '<br/></h3>'+
                (props.TOTAL_POPULATION !== undefined ? '<b>Demographic</b></br>Total Pop. ' + props.TOTAL_POPULATION + '' : '') +
                '<br/>' + race +
                '<br/>'
                : 'Hover over a highlighted region.');
        return html;
    }

    static districtDisplay(props) {
        let race = '';
        if (props && props.RACE ) {
            race =
                ' White: ' + (props.RACE.WHITE_ALONE ? props.RACE.WHITE_ALONE.toLocaleString() : 0) + '<br/>' +
                ' Black: ' +(props.RACE.BLACK_ALONE ? props.RACE.BLACK_ALONE.toLocaleString() : 0)  +'<br/>' +
                ' Asian: ' + (props.RACE.ASIAN_ALONE ? props.RACE.ASIAN_ALONE.toLocaleString() : 0) +'<br/>' +
                ' Native: ' + (props.RACE.AMERICAN_INDIAN_ALASKAN_ALONE ? props.RACE.AMERICAN_INDIAN_ALASKAN_ALONE.toLocaleString() : 0)   +'<br/>' +
                ' Other: ' +  (props.RACE.OTHER_ALONE ? props.RACE.OTHER_ALONE.toLocaleString() : 0);
        }
        let d ='';
        let representative = '';
        let elect = '';
        let econ = '';
        if (props) {
            d = '<h3>' + props.NAME + ' (' + props.YEAR + ')</h3>';
            representative = '<b>District Rep:</b> ' + props.REPRESENTATIVE +'';
            elect = '<b>' + props.YEAR + ' Election: </b>' +
            '<br/>Republican: ' + props.REP_VOTES +
                '<br/>Democrat: ' + props.DEM_VOTES +
                '<br/>Other: ' + props.OTHER_VOTES
            ;
            econ = '<br/> <b>Economy:</b>' +
                '<br/>Median Income: ' + props.MEDIAN_INCOME +
                '<br/>Mean Income: ' + props.MEAN_INCOME
            ;
        }
        let html =
            (props ?
                d +
                '<b>Demographic</b></br>Total Pop. ' + (props.TOTAL_POPULATION ? props.TOTAL_POPULATION.toLocaleString() : 0) +
                '<br/>' + race +
                '<br/>' + elect +
                '<br/>' + representative +
                '<br/>' + econ
                : 'Hover over a district.');
        return html;
    }

    static precinctDisplay(props) {
        let d ='';

        if (props) {
            d = '<h3>' + props.NAME + ' (2010)</h3>';
        }
        let html =
            (props ?
                d +
                '<br/>'
                : 'Hover over a district.');
        return html;
    }


}

export default MapInfo;