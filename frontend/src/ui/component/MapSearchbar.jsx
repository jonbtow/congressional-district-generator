import {MapControl} from 'react-leaflet'
import {GeoSearchControl, EsriProvider} from 'leaflet-geosearch';

class MapSearchbar extends MapControl {
    createLeafletElement() {
        return GeoSearchControl({
            provider: new EsriProvider(),
            style: 'button',
            showMarker: true,
            showPopup: false,
            autoClose: true,
            retainZoomLevel: false,
            animateZoom: true,
            keepResult: false,
            searchLabel: 'Search',
        });
    }
}

export default MapSearchbar;