import React from 'react';
import PropTypes from "prop-types";

import './style/RedistrictingComponent.css';


class MeasureSlider extends React.Component {

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        const target = e.target;
        const value = Number(target.value);
        this.props.onWeightChange(value);
    }

    render() {
        return (
            <div className="form-row pt-1 pb-1">
                <div className="col">
                    <div className="input-group bg-white ">
                        <div className="input-group-prepend w-100 pl-2 form-row ">
                            <div className="input-group-text col-sm-10">
                                {this.props.name}
                            </div>
                            <div className="input-group-text col-sm-2">
                                <span className="weight">{this.props.weight.toString()}</span>
                            </div>
                        </div>
                        <div className="w-100 pl-2 form-row pt-0">
                            <div className="col">
                                <input
                                    className="sliderMeasure"
                                    id="typeinp"
                                    type="range"
                                    min="0" max="100"
                                    value={this.props.weight}
                                    onChange={this.handleChange}
                                    step="10"
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


MeasureSlider.propTypes = {
    name: PropTypes.string.isRequired,
    weight: PropTypes.number.isRequired,
    onWeightChange: PropTypes.func.isRequired,
}

export default MeasureSlider