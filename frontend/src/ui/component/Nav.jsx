import React, {Component} from 'react';
import {NavLink} from "react-router-dom";
import PropTypes from "prop-types";

import LogoutButton from '../container/LogoutButton';

import './style/Nav.css';


export class Nav extends Component {
    render() {
        return (
            <nav className="navbar w-100 navbar-dark sticky-top p-0">
                <NavLink className="navbar-brand" to="/">
                    District Generator
                </NavLink>


                <button className="navbar-toggler " type="button"
                        data-toggle="collapse" data-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false"
                        aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>


                <div className="collapse navbar-collapse " id="navbarNav">
                    <ul className="nav navbar-nav ml-auto">
                        <li className="" data-toggle="collapse" data-target=".navbar-collapse.show">
                            <NavLink className="nav-link " to="/about">
                                About
                            </NavLink>
                        </li>
                        <li className="" data-toggle="collapse" data-target=".navbar-collapse.show">
                            <NavLink className="nav-link" to="/help">
                                Help
                            </NavLink>
                        </li>
                        <li className="" data-toggle="collapse" data-target=".navbar-collapse.show">
                            <NavLink className="nav-link" to="/team">
                                Team
                            </NavLink>
                        </li>
                        {this.props.email === '' ? (
                            <div>
                                <li className="" data-toggle="collapse" data-target=".navbar-collapse.show">
                                    <NavLink className="nav-link " to="/register">
                                        Register
                                    </NavLink>
                                </li>
                                <li className="" data-toggle="collapse" data-target=".navbar-collapse.show">
                                    <NavLink className="nav-link " to="/login">
                                        Login
                                    </NavLink>
                                </li>
                            </div>
                        ) : (
                            <div>
                                <li className="" data-toggle="collapse" data-target=".navbar-collapse.show">
                                    <NavLink className="nav-link " to="/account">
                                        Account
                                    </NavLink>
                                </li>
                                <li className="" data-toggle="collapse" data-target=".navbar-collapse.show">
                                        <LogoutButton onSubmit={this.props.onLogout}/>
                                </li>
                            </div>
                        )}
                    </ul>
                </div>
            </nav>
        );
    }
}

Nav.propTypes = {
  onLogout: PropTypes.func.isRequired,
};
