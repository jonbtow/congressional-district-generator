import React from 'react';

import {PageTitle} from './PageTitle.jsx'

import './style/Pages.css';


export class Page extends React.Component {
    render() {
        return (
            <div className={"page bg-white"}>
                <div className={"row pb-1 redistricting-control " + this.props.bgColor}>
                    <div className={"col"}>
                        <PageTitle title={this.props.title}/>
                        <div className="row content">
                            <div className="col ">
                                {this.props.children}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
