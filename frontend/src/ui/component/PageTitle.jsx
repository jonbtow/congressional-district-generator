import React from 'react';

import './style/Pages.css';


export class PageTitle extends React.Component {
    render() {
        return (
            <div className="row title">
                <div className="col text-left">
                    <h1> {this.props.title} </h1>
                </div>
            </div>
        );
    }
}
