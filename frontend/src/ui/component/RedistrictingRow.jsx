import React from 'react';

export class RedistrictingRow extends React.Component {

    render() {
        return (
            <div className={"input p-0"}>
                <div className={"row bg-white  pt-3 pb-3 redistricting-control bottom-border"}>
                    <div className={"col"}>
                        {this.props.children}
                    </div>
                </div>
            </div>
        );
    }
}