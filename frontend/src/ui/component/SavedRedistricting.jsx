import React from 'react';


class SavedRedistricting extends React.Component {
    constructor(props) {
        super(props);
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleDelete() {
        this.props.onDelete(this.props.savedRedistricting);
    }
    handleLoad() {
        this.props.onLoad(this.props.savedRedistricting.stateYear,
            this.props.savedRedistricting.stateName,
            this.props.savedRedistricting.mapId
            );
    }
    render() {
        return (
            <tr>
                <td>{this.props.savedRedistricting.stateYear}</td>
                <td>{this.props.savedRedistricting.stateName}</td>
                <td>{this.props.savedRedistricting.mapId}</td>
                <td>
                    <button type={"submit"} className={"btn bg-white account-btn"} onClick={this.handleDelete}>
                        <i className="fas fa-trash-alt"></i>
                    </button>
                </td>
                <td>
                    <button type={"submit"} className={"btn bg-white account-btn-gr"} onClick={this.handleLoad}>
                        <i className="fas fa-cloud-upload-alt"></i>
                    </button>
                </td>
            </tr>
        )
    }
}

export default SavedRedistricting;