import React, {Component} from 'react';
import {Switch, Route} from "react-router-dom";

import RegisterPage from '../container/RegisterPage';
import LoginPage from '../container/LoginPage';
import AccountPage from '../container/AccountPage';

import {AboutPage} from './AboutPage';
import {FAQPage} from './FAQPage';
import {TeamPage} from './TeamPage';
import RedistrictingForm from '../container/RedistrictingForm';

import '../container/style/Sidebar.css';


export class Sidebar extends Component {
    render() {
        return (
            <div className="container-fluid h-100 ">
                <div className="row h-100 ">
                    <div className="col">
                        <div className="dg-sidebar d-flex flex-column  w-100 ">
                            <Switch>
                                <Route exact path='/' component={RedistrictingForm}/>
                                <Route path='/about' component={AboutPage}/>
                                <Route path='/help' component={FAQPage}/>
                                <Route path='/team' component={TeamPage}/>
                                <Route path='/register' render={() => <RegisterPage onSubmit={this.props.onRegister}/>}/>
                                <Route path='/login' render={() => <LoginPage onSubmit={this.props.onLogin}/>}/>
                                <Route path='/account' render={() => <AccountPage/>}/>
                            </Switch>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
