import React from 'react';
import PropTypes from "prop-types";

export class SubmitButton extends React.Component {
    render() {
        return (
                <button type="submit"
                        disabled={!this.props.isFormValid()}
                        onClick={this.props.onClick}
                        className="btn btn-primary col-sm-12"
                >
                    {this.props.text}
                </button>
        );
    }
}

SubmitButton.propTypes = {
    isFormValid: PropTypes.func.isRequired,
    isLoggedIn: PropTypes.bool.isRequired,
    onClick: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired,
}