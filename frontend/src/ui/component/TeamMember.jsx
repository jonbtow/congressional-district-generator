import React from 'react';
import PropTypes from "prop-types";


export class TeamMember extends React.Component {
    render() {
        return (
            <div className="row pl-4 pr-4 pb-4 text-left">
                <div className="col ">
                    <h3> {this.props.name} </h3>
                    ({this.props.devRole})
                    <a href="https://placeholder.com">
                        <img className="rounded img-fluid pt-2"
                             alt="Team Member"
                             src="http://via.placeholder.com/250x250"/>
                    </a>
                </div>
            </div>
        );
    }
}

TeamMember.propTypes = {
    name: PropTypes.string.isRequired,
    devRole: PropTypes.string.isRequired,
}