import React from 'react';

import {TeamMember} from './TeamMember';
import {Page} from './Page';

import './style/Pages.css';


export class TeamPage extends React.Component {
    render() {
        return (
            <Page title={"Team"}>
                <TeamMember name="Jonathan Tow" devRole="Lead Designer"/>
                <TeamMember name="Jaspreet Singh Nahal" devRole="Lead Programmer"/>
                <TeamMember name="Soumya Das" devRole="Product Manager"/>
                <TeamMember name="Youri Paul" devRole="Database Designer"/>
            </Page>
        );
    }
}

