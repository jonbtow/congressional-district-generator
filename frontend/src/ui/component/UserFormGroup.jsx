import React from 'react';

import '../component/style/Pages.css';

export class UserFormGroup extends React.Component {
    render() {
        return (
            <div className="form-group row">
                <div className="col-sm-2"></div>
                <div className="col-sm-8 text-center">
                    {this.props.children}
                </div>
                <div className="col-sm-2"></div>
            </div>
        )
    }
}