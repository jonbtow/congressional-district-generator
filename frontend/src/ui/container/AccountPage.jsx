import React from 'react';
import {connect} from "react-redux";

import {Page} from '../component/Page';
import SavedRedistricting from '../component/SavedRedistricting';
import API from '../../config/api';

import ReactTable from "react-table";

import '../component/style/Pages.css';
import './style/Accounts.css';
import {recieveGeoJson} from "../../actions/geoJsonActions";

const DELETE_REDISTRICTING = '/saved/redistricting';
const LOAD_REDISTRICTING = '/saved/redistricting';
const SAVED_REDISTRICTINGS = '/saved/redistrictings';

class AccountPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            userRedistrictings: [{
                date: '5-19-2018',
                state: 'Iowa',
                file: 'redistricting-ia-193204.js'
            }, {
                date: '5-19-2018',
                state: 'Iowa',
                file: 'redistricting-ia-193204.js'
            }],
        };
        this.onDelete = this.onDelete.bind(this);
    }

    componentDidMount() {
        if (this.props.email !== null || this.props.email !== '') {
            let e = this.props.email;
            console.log(e);
            API.get(SAVED_REDISTRICTINGS, {
                params: {
                    email: e,
                }
            })
                .then(res => {
                    let allRedistrictings = res.data;
                    this.setState({
                        userRedistrictings: allRedistrictings,
                    });
                })
                .catch(error => {
                    console.log('Get Map Data Error: ' + error.response);
                });

        }
    }

    /* Deletes saved redistricting from users account */
    onDelete(redistricting) {
        API.delete(DELETE_REDISTRICTING)
            .then(res => {
                let allStatesObject = res.data;
                this.setStateSelectOptions(allStatesObject);
            })
            .catch(error => {
                console.log('Get Map Data Error: ' + error.response);
            });
        //return null;
    }

    /* Loads saved redistricting from the users account */
    onLoad(y, s, id) {
        API.get(LOAD_REDISTRICTING, {
            params: {
                stateName: s,
                year: y,
                savedMapId: id,
            }
        })
            .then(res => {
                let allRedistrictings = res.data;
                this.props.onLoadGeo();

            })
            .catch(error => {
                console.log('Get Map Data Error: ' + error.response);
            });
    }

    render() {

        const sr = {
            state: '',
            state: '',
            file: ''
        };
        let savedRedistrictings = [];
        console.log(this.state.userRedistrictings);
        if (this.state.userRedistrictings.length != 0) {
            savedRedistrictings = this.state.userRedistrictings.map(r =>
                <SavedRedistricting key={Math.random()} savedRedistricting={r} onLoad={this.onLoad} onDelete={this.onDelete}/>
            );
        } else {
            savedRedistrictings =
                <tr>
                    <th scope="col"></th>
                    <th scope="col">Save a map to view here.</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
        }
        //var savedRedistrictings = <SavedRedistricting onDelete={this.onDelete} savedRedistricting={sr}/>
        /*this.props.user.map(user =>
                   <SavedRedistricting key={user._links.self.href} user={user.savedRedistricting} onLoad={this.onLoad} onDelete={this.onDelete}/>
               );*/

        return (
            <Page title={"Account"}>
                <div className={"row mb-4"}>
                    <div className={"col"}>
                        Welcome, {this.props.email}.
                    </div>
                </div>


                <div className={"row "}>
                    <div className={"col"}>
                        <div className={"pt-4 pb-0"}>
                            <div className="p-0 card-header text-left">
                                <h3 className={"p-0"}>Saved Maps</h3>
                            </div>
                            <div className={"table-responsive "}>
                                <table className="table table-bordered bg-white bottom-border ">
                                    <thead>
                                    <tr>
                                        <th scope="col">Year</th>
                                        <th scope="col">State</th>
                                        <th scope="col">File ID</th>
                                        <th scope="col"></th>
                                        <th scope="col"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {savedRedistrictings}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </Page>
        )
    }
}
const mapStateToProps = (state) => ({
    email: state.auth.email,
});

const mapDispatchToProps = (dispatch) => ({
    onLoadGeo: (geo) => dispatch(recieveGeoJson('PRECINCT', geo)),
});

AccountPage = connect(
    mapStateToProps,
    mapDispatchToProps
)(AccountPage);

export default AccountPage;

