import React, { Component } from 'react';
import {connect} from 'react-redux';
import { withRouter } from 'react-router';
import {NotificationContainer} from 'react-notifications';
import 'react-notifications/lib/notifications.css';

import './style/App.css';

import  AppMap  from './AppMap';
import { Sidebar } from '../component/Sidebar';
import { Header } from '../component/Header';
import {register, login, logout} from '../../auth/authReducer'



export class App extends Component {

    render() {
        return (
            <div className="App container-fluid h-100 ">
              <div className="row h-100 ">
                <div className="col-md-4 d-flex flex-column side p-0">
                    <Header email={this.props.email} onLogout={this.props.onLogout} />
                    <Sidebar onLogin={this.props.onLogin}
                             onRegister={this.props.onRegister}
                             email={this.props.email} />
                </div>


                <div className="col-md-8 p-0 ">
                    <NotificationContainer/>
                    <AppMap/>
                </div>
              </div>
            </div>
    	);
    }
}


const mapStateToProps = ({auth: { email, loginInProgress }}) => ({
  email,
  loginInProgress
});

function mapDispatchToProps(dispatch) {
  return {
        onLogin: credentials => dispatch(login(credentials)),
        onRegister: credentials => dispatch(register(credentials)),
        onLogout: cred => dispatch(logout(cred)),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
