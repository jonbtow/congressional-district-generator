import React from 'react';
import {connect} from "react-redux";

import {toggleContiguity, toggleRacialFairness, toggleEqualPopulation} from "../../actions/redistrictingActions"

import ConstraintToggle from '../component/ConstraintToggle.jsx';

import '../component/style/RedistrictingComponent.css';


class ConstraintToggleContainer extends React.Component {
    render() {
        return (
            <div className="">
                <div className={"row "}>
                    <div className={"pt-3 pl-4 col-10"}>
                        Constraints
                    </div>
                    <div className={"col-2"}>
                        <button type="button"
                                className="btn collapse-btn bottom-border"
                                data-toggle="collapse"
                                data-target="#constraints"
                                aria-expanded="false"
                                aria-controls="constraints"
                                disabled={this.props.isLoggedIn}
                        >
                        <i className="fas fa-ellipsis-v"></i>
                        </button>
                    </div>
                </div>
                <div className="collapse" id="constraints">
                    <div className="form-group p-0 mt-1" style={{borderRadius: 4}}>
                        <ConstraintToggle name="Equal Population"
                                          onConstraintChange={this.props.onToggleEqualPopulation}
                                          checked={this.props.equalPopulationConstraint}/>
                        <ConstraintToggle name="Contiguity"
                                          onConstraintChange={this.props.onToggleContiguity}
                                          checked={this.props.contiguityConstraint}/>
                    </div>
                </div>
            </div>
        );
    }
}


const mapStateToProps = (state) => ({
    equalPopulationConstraint: state.redistricting.equalPopulationConstraint,
    contiguityConstraint: state.redistricting.contiguityConstraint,
})

const mapDispatchToProps = (dispatch) => ({
    onToggleContiguity: (checked) => dispatch(toggleContiguity(checked)),
    onToggleEqualPopulation: (checked) => dispatch(toggleEqualPopulation(checked)),
})

ConstraintToggleContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ConstraintToggleContainer)

export default ConstraintToggleContainer
