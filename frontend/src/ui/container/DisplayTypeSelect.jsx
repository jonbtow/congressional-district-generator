import React from 'react';
import {connect} from 'react-redux';

import {setDisplayType, STATE, DISTRICT, PRECINCT} from '../../actions/mapDisplayActions';
import IconInputGroup from '../component/IconInputGroup';
import {fetchGeoJson} from "../../actions/geoJsonActions";


const options = [
    {value: '', label: 'View a districting'},
    {value: STATE, label: 'State'},
    {value: DISTRICT, label: 'Congressional District'},
    {value: PRECINCT, label: 'Precinct'},
];

const STATE_URL = 'states/all/geometry';
const DISTRICT_URL = 'state/districts/geometry';
const PRECINCT_URL = 'state/precincts/geometry';


class DisplayTypeSelect extends React.Component {

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.getSelectOption = this.getSelectOption.bind(this);
    }

    handleChange(e) {
        e.preventDefault();

        const target = e.target;

        const displayType = target.value;
        this.props.onSelectDisplayType(displayType);
        let restUrl = '';
        switch (displayType) {
            case STATE:
                restUrl = STATE_URL;
                break;
            case DISTRICT:
                restUrl = DISTRICT_URL;
                break;
            case PRECINCT:
                restUrl = PRECINCT_URL;
                break;
            default:
                restUrl = STATE_URL;
        }
        this.props.onSetGeoJson(displayType, restUrl, this.props.displayState, 2010);
    }

    getSelectOption(option) {
        if (this.props.displayType === '' && option.value === '') {
            return <option value={option.value} key={option.value} disabled={true} selected={true}> {option.label} </option>
        } else if (this.props.displayType === option.value) {
            return <option value={option.value} key={option.value} selected={true}> {option.label} </option>
        } else if (option.value === '') {
            return <option value={option.value} key={option.value} disabled={true} selected={true}> {option.label} </option>
        } else {
            return <option value={option.value} key={option.value}> {option.label} </option>
        }
    }

    render() {
        return (
                <IconInputGroup icon="fas fa-map">
                    <select className=" form-control chooseState"
                            onChange={this.handleChange}>
                        {
                            options.map((option) => {
                                return this.getSelectOption(option)
                            })
                        }
                    </select>
                </IconInputGroup>
        )
    }
}


const mapStateToProps = (state) => ({
    displayType: state.mapDisplay.displayType,
    displayState: state.mapDisplay.displayState
});

const mapDispatchToProps = (dispatch) => ({
    onSelectDisplayType: (districtingType) => dispatch(setDisplayType(districtingType)),
    onSetGeoJson: (gt, url, stateName, year) => dispatch(fetchGeoJson(gt, url, stateName, year)),
});

DisplayTypeSelect = connect(
    mapStateToProps,
    mapDispatchToProps
)(DisplayTypeSelect);

export default DisplayTypeSelect;