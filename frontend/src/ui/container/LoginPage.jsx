import React from 'react';
import {withRouter} from 'react-router';
import {NotificationManager} from 'react-notifications';

import {NavLink} from "react-router-dom";

import {Page} from '../component/Page';
import {UserFormGroup} from "../component/UserFormGroup";

import IconInputGroup from '../component/IconInputGroup'

import '../component/style/Pages.css';
import './style/Accounts.css';
import {token} from "../../auth/http";
import SockJS from "sockjs-client";
import Stomp from "stompjs";
import {setGeoJsonUpdate, setStompClient} from "../../actions/socketActions";
import {connect} from "react-redux";

class LoginPage extends React.Component {

    constructor() {
        super();
        this.state = {
            email: '',
            password: ''
        };
        this.fun = this.fun.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.handleRedirect = this.handleRedirect.bind(this);
        this.loginSuccessAlert = this.loginSuccessAlert.bind(this);
    }

    fun(e) {
        console.log('fun\n' +JSON.parse(e.body));
        let body = JSON.parse(e.body);
        this.props.onUpdateGeoJson(body);
    }

    onChange(field, val) {
        this.setState({[field]: val});
    }

    onSubmit(e) {
        this.props.onSubmit({
            email: this.state.email,
            password: this.state.password,
            redirect: this.handleRedirect
        });
    }

    handleRedirect(url) {
        this.props.history.push(url);
        var socket = new SockJS('http://localhost:8090/calculations?Authorization=Bearer ' + token);
        let stompClient = Stomp.over(socket);
        var self = this;
        stompClient.connect({}, function (frame) {
            console.log('Connected: ' + frame);
            stompClient.subscribe('/user/queue/calculations', self.fun);
        });
        this.props.onStomp(stompClient);
        this.loginSuccessAlert();
    }

    loginSuccessAlert() {
        const msg = 'Welcome ' + this.state.email;
        const title = 'Login Succes';
        const timeOutMs = 1700;
        NotificationManager.success(msg, title, timeOutMs);
    }

    render() {
        return (
            <Page title={"Login"}>
                <form onSubmit={(e)=>{e.preventDefault(e)}}
                      className={"input-container pt-4"}>
                    <UserFormGroup>
                        <IconInputGroup icon="fas fa-user">
                            <input type="email"
                                   className="form-control"
                                   id="inputEmail3"
                                   aria-describedby="emailHelp"
                                   placeholder='Email' name="email"
                                   onChange={e => this.onChange('email', e.target.value)}
                                   disabled={this.props.disabled}
                                   required={true}
                            />
                        </IconInputGroup>
                    </UserFormGroup>
                    <UserFormGroup>
                        <IconInputGroup icon="fas fa-lock">
                            <input type="password"
                                   className="form-control"
                                   id="inputPassword3"
                                   placeholder="Password"
                                   name="password"
                                   onChange={e => this.onChange('password', e.target.value)}
                                   disabled={this.props.disabled}
                                   required={true}
                            />
                        </IconInputGroup>
                    </UserFormGroup>
                    <UserFormGroup>
                        <button type="submit"
                                className="btn mt-2 btn-primary"
                                onClick={e => this.onSubmit(e)}
                                disabled={this.props.disabled}>
                            Login
                        </button>
                    </UserFormGroup>
                    <UserFormGroup>
                        <div className={'text-muted '}>
                            New around here? <NavLink className=" dropdown-item" to="/register">Sign up</NavLink>
                        </div>
                    </UserFormGroup>
                </form>
            </Page>
        );
    }
}

const mapStateToProps = (state) => ({
    stompClient: state.socket.stompClient,
    redistricting: state.redistricting,
});

const mapDispatchToProps = (dispatch) => ({
    onStomp: (stomp) => dispatch(setStompClient(stomp)),
    onUpdateGeoJson: (geo) => dispatch(setGeoJsonUpdate(geo)),
});

LoginPage = connect(
    mapStateToProps,
    mapDispatchToProps
)(LoginPage);


export default withRouter(LoginPage);
