import React from 'react';
import { withRouter } from 'react-router';

import '../component/style/Pages.css';
import './style/Accounts.css';
import {NotificationManager} from "react-notifications";
import {addUnmovableDistrict, addUnmovablePrecinct} from "../../actions/redistrictingActions";
import {connect} from "react-redux";


class LogoutButton extends React.Component {

    constructor() {
        super();
        this.handleRedirect = this.handleRedirect.bind(this);
        this.logoutSuccessAlert = this.logoutSuccessAlert.bind(this);
    }

    onSubmit(e) {
        e.preventDefault();
        this.props.onSubmit({
            redirect: this.handleRedirect
        });
        if (this.props.stompClient !== null) {
            this.props.stompClient.disconnect();
        }
        this.logoutSuccessAlert();
    }

    handleRedirect(url) {
       this.props.history.push(url);
    }

    logoutSuccessAlert() {
        const msg = 'Thanks for using District Generator';
        const title = 'Logout Success';
        const timeOutMs = 1700;
        NotificationManager.success(msg, title, timeOutMs);
    }

    render() {
        return (
            <button type="submit"
                    className={"btn logout-btn"}
                    onClick={e => this.onSubmit(e)}
            >
		        Logout
		    </button>
    );
    }
}
const mapStateToProps = (state) => ({
    stompClient: state.socket.stompClient,
});


LogoutButton = connect(
    mapStateToProps,
    null
)(LogoutButton);


export default withRouter(LogoutButton);
