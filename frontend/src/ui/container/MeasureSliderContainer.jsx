import React from 'react';
import {connect} from "react-redux";

import {
    weighSchwartzbergCompactness, weighReockCompactness,
    weighPolsbyPopperComapctness, weighConvexHullCompactness, weighEfficiencyGap, weighRacialFairness
} from "../../actions/redistrictingActions";

import MeasureSlider from '../component/MeasureSlider';

import '../component/style/RedistrictingComponent.css';


class MeasureSliderContainer extends React.Component {

    render() {
        return (
            <div className="">
                <div className={"row "}>
                    <div className={"pt-3 pl-4 col-10"}>
                        Measures
                    </div>
                    <div className={"col-2"}>
                        <button className="btn collapse-btn bottom-border "
                                type="button"
                                data-toggle="collapse"
                                data-target="#measures"
                                aria-expanded="false"
                                aria-controls="measures"
                                disabled={this.props.isLoggedIn}
                        >
                            <i className="fas fa-ellipsis-v"></i>
                        </button>
                    </div>
                </div>

                {/* Measures */}
                <div className="collapse" id="measures">
                    <div className="form-group p-0 " style={{'borderRadius': 4}}>
                        {/* Partisan Fairness */}
                        <div className="form-row pt-2">
                            <div className="col">
                                <MeasureSlider name='Efficiency Gap'
                                               weight={this.props.efficiencyGapWeight}
                                               onWeightChange={this.props.weighEfficiencyGap}/>
                            </div>
                        </div>
                        {/* Compactness */}
                        <div className="form-row ">
                            <div className="col">
                                <MeasureSlider name="Schwartzberg"
                                               weight={this.props.schwartzbergCompactnessWeight}
                                               onWeightChange={this.props.weighSchwartzbergCompactness}/>
                                <MeasureSlider name='Reock'
                                               weight={this.props.reockCompactnessWeight}
                                               onWeightChange={this.props.weighReockCompactness}/>
                                <MeasureSlider name='Polsby-Popper'
                                               weight={this.props.polsbyPopperCompactnessWeight}
                                               onWeightChange={this.props.weighPolsbyPopperComapctness}/>
                                <MeasureSlider name='Racial-Fairness'
                                               weight={this.props.racialFairnessWeight}
                                               onWeightChange={this.props.weighRacialFairness}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


const mapStateToProps = (state) => ({
    equalPopulationWeight: state.redistricting.equalPopulationWeight,
    schwartzbergCompactnessWeight: state.redistricting.schwartzbergCompactnessWeight,
    reockCompactnessWeight: state.redistricting.reockCompactnessWeight,
    polsbyPopperCompactnessWeight: state.redistricting.polsbyPopperCompactnessWeight,
    efficiencyGapWeight: state.redistricting.efficiencyGapWeight,
    racialFairnessWeight: state.redistricting.racialFairnessWeight,
});

const mapDispatchToProps = (dispatch) => ({
    weighSchwartzbergCompactness: (weight) => dispatch(weighSchwartzbergCompactness(weight)),
    weighReockCompactness: (weight) => dispatch(weighReockCompactness(weight)),
    weighPolsbyPopperComapctness: (weight) => dispatch(weighPolsbyPopperComapctness(weight)),
    weighEfficiencyGap: (weight) => dispatch(weighEfficiencyGap(weight)),
    weighRacialFairness: (checked) => dispatch(weighRacialFairness(checked)),

});

MeasureSliderContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(MeasureSliderContainer);

export default MeasureSliderContainer
