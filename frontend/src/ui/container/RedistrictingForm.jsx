import React from 'react';
import {connect} from "react-redux";


import StateSelect from './StateSelect';
import DisplayTypeSelect from './DisplayTypeSelect';
import MeasureSliderContainer from './MeasureSliderContainer';
import ConstraintToggleContainer from './ConstraintToggleContainer';
import RedistrictingRemoteControl from './RedistrictingRemoteControl';
import StatsPanel from './StatsPanel';

import {RedistrictingRow} from '../component/RedistrictingRow'
import {SubmitButton as CalculateSubmitButton} from '../component/SubmitButton';

import '../component/style/Pages.css';
import {setStompClient} from "../../actions/socketActions";
import {PRECINCT} from "../../actions/mapDisplayActions";


class RedistrictingForm extends React.Component {

    constructor(props) {
        super(props);
        this.isFormValid = this.isFormValid.bind(this);
        this.onSubmitRedistricting = this.onSubmitRedistricting.bind(this);
    }

    componentDidMount() {

    }

    isFormValid() {
        const isStateSelected = this.props.redistricting.stateName !== '';
        const isLoggedIn = this.props.email !== '';
        const isPrecinctView = this.props.displayType === PRECINCT;
        return isStateSelected && isLoggedIn && isPrecinctView;
    }

    onSubmitRedistricting(e) {
        e.preventDefault();
        this.props.stompClient.send("/app/calculations", {}, JSON.stringify({'requestType': 'START',
            year: 2010,
            ...this.props.redistricting
        }));
    }

    render() {
        return (
            <form onSubmit={(e)=>{e.preventDefault(e)}} className={"page input-container p-0"}>
                <div className={"input p-0"}>
                    <RedistrictingRow> <StateSelect/> </RedistrictingRow>
                    <RedistrictingRow> <DisplayTypeSelect/> </RedistrictingRow>
                    <RedistrictingRow> <MeasureSliderContainer isLoggedIn={this.props.email === ''}/> </RedistrictingRow>
                    <RedistrictingRow> <ConstraintToggleContainer isLoggedIn={this.props.email === ''}/> </RedistrictingRow>
                    <RedistrictingRow> <StatsPanel/> </RedistrictingRow>
                    <RedistrictingRow> <RedistrictingRemoteControl isLoggedIn={this.props.email === ''}/> </RedistrictingRow>
                    <RedistrictingRow>
                        <CalculateSubmitButton isFormValid={this.isFormValid}
                                               text={"Calculate"}
                                               onClick={this.onSubmitRedistricting}
                                               isLoggedIn={this.props.email === ''}
                        />
                    </RedistrictingRow>
                </div>
            </form>
        );
    }
}


const mapStateToProps = (state) => ({
    stompClient: state.socket.stompClient,
    displayType: state.mapDisplay.displayType,
    redistricting: state.redistricting,
    email: state.auth.email,
});

const mapDispatchToProps = (dispatch) => ({
    onStomp: (stomp) => dispatch(setStompClient(stomp))
});

RedistrictingForm = connect(
    mapStateToProps,
    mapDispatchToProps
)(RedistrictingForm);

export default RedistrictingForm

