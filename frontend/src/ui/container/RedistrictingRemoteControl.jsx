import React from 'react';
import {NotificationManager} from 'react-notifications';

import '../component/style/RedistrictingComponent.css';
import './style/Redistricting.css';
import {connect} from "react-redux";
import {
    toggleContiguity,
    togglePreserveCommunities,
    weighRacialFairness,
    weighEfficiencyGap,
    toggleEqualPopulation, weighPolsbyPopperComapctness,
    weighReockCompactness,
    weighSchwartzbergCompactness
} from "../../actions/redistrictingActions";
import FontAwesome from 'react-fontawesome';
import {setDisplayState, setDisplayType, STATE} from "../../actions/mapDisplayActions";
import {setGeoJsonUpdate} from "../../actions/socketActions";

class RedistrictingRemoteControl extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isResumed: true,
        }
        this.onClickSave = this.onClickSave.bind(this);
        this.onClickResume = this.onClickResume.bind(this);
        this.onClickPause = this.onClickPause.bind(this);
        this.onClickReset = this.onClickReset.bind(this);
        this.onClickStop = this.onClickStop.bind(this);
    }

    onClickReset(e) {
        e.preventDefault();
        this.props.onToggleContiguity(false);
        this.props.weighRacialFairness(0);
        this.props.toggleEqualPopulation(false);

        this.props.weighSchwartzbergCompactness(0);
        this.props.weighReockCompactness(0);
        this.props.weighPolsbyPopperComapctness(0);
        this.props.weighEfficiencyGap(0);
        this.props.onMapReset(STATE);

        this.props.resetGeoJsonUpdate({});
    };
    onClickPause(e) {
        e.preventDefault();
        this.setState({
            isResumed:false,
        });
        this.props.stompClient.send("/app/calculations", {}, JSON.stringify({'requestType': 'PAUSE'}));
    }
    onClickResume(e) {
        e.preventDefault();
        this.setState({
            isResumed:true,
        });
        this.props.stompClient.send("/app/calculations", {}, JSON.stringify({'requestType': 'RESUME'}));
    }
    onClickStop(e) {
        e.preventDefault();
        this.props.stompClient.send("/app/calculations", {}, JSON.stringify({'requestType': 'STOP'}));
        const msg = 'You have stopped redistricting';
        const title = 'Redistricting Stopped';
        const timeOutMs = 1700;
        NotificationManager.warning(msg, title, timeOutMs);
    }
    onClickSave(e) {
        e.preventDefault();
        this.props.stompClient.send("/app/calculations", {}, JSON.stringify({'requestType': 'SAVE'}));
        const msg = 'Your redistricting is available in your Account page.';
        const title = 'Redistricting Saved';
        const timeOutMs = 1700;
        NotificationManager.success(msg, title, timeOutMs);
    }

    render() {
        return (
            <div className="">
                <div className={"row "}>
                    <div className={"pt-3 pl-4 col-10"}>
                        Controls
                    </div>
                    <div className={"col-2"}>
                        <button className="btn collapse-btn bottom-border "
                                type="button"
                                data-toggle="collapse"
                                data-target="#controls"
                                aria-expanded="false"
                                aria-controls="measures"
                        >
                            <i className="fas fa-ellipsis-v"></i>
                        </button>
                    </div>
                </div>


                <div className="collapse show" id="controls">
                    <div className="form-group p-0 " style={{'borderRadius': 4}}>
                        <div className="form-row">
                            <div className="col ml-auto">
                                {this.state.isResumed ?
                                <button className="btn bg-white remote-btn"
                                        title={"Pause redistricting algorithm"}
                                        onClick={this.onClickPause}
                                        disabled={this.props.isLoggedIn}
                                        key={Math.random()}
                                >
                                    <FontAwesome name='pause'/>

                                </button>
                                :
                                   <button className="btn bg-white remote-btn"
                                        title={"Resume redistricting algorithm"}
                                        onClick={this.onClickResume}
                                        disabled={this.props.isLoggedIn}
                                           key={Math.random()}
                                   >
                                     <FontAwesome name='play'/>

                                   </button>
                                }
                            </div>
                            <div className="col mr-auto">
                                <button className="btn bg-white remote-btn"
                                        title={"Stop redistricting algorithm"}
                                        onClick={this.onClickStop}
                                        disabled={this.props.isLoggedIn}
                                >
                                    <i className="fas fa-stop"></i>
                                </button>
                            </div>
                            <div className="col mr-auto">
                                <button className="btn bg-white remote-btn"
                                        title={"Reset preferences"}
                                        onClick={this.onClickReset}
                                        disabled={this.props.isLoggedIn}
                                >
                                    <i className="fas fa-redo-alt"></i>
                                </button>
                            </div>

                            <div className="col mr-auto">
                                <button className="btn bg-white remote-btn"
                                        title="Save redistricting"
                                        onClick={this.onClickSave}
                                        disabled={this.props.isLoggedIn}
                                >
                                    <i className="fas fa-save"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    stompClient: state.socket.stompClient,
    redistricting: state.redistricting,
    mapDisplay: state.mapDisplay,
});

const mapDispatchToProps = (dispatch) => ({
    onToggleContiguity: (checked) => dispatch(toggleContiguity(checked)),
    weighRacialFairness: (checked) => dispatch(weighRacialFairness(checked)),

    toggleEqualPopulation: (weight) => dispatch(toggleEqualPopulation(weight)),
    weighSchwartzbergCompactness: (weight) => dispatch(weighSchwartzbergCompactness(weight)),
    weighReockCompactness: (weight) => dispatch(weighReockCompactness(weight)),
    weighPolsbyPopperComapctness: (weight) => dispatch(weighPolsbyPopperComapctness(weight)),
    weighEfficiencyGap: (weight) => dispatch(weighEfficiencyGap(weight)),

    onMapReset: (x) => dispatch(setDisplayType(x)),
    resetGeoJsonUpdate: (update) => dispatch(setGeoJsonUpdate(update)),

});

RedistrictingRemoteControl = connect(
    mapStateToProps,
    mapDispatchToProps
)(RedistrictingRemoteControl);

export default RedistrictingRemoteControl;