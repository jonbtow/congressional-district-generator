import React from 'react';
import {withRouter} from 'react-router';
import {NavLink} from "react-router-dom";
import {NotificationManager} from 'react-notifications';

import {Page} from '../component/Page';
import {UserFormGroup} from "../component/UserFormGroup";
import IconInputGroup from '../component/IconInputGroup'

import '../component/style/Pages.css';
import './style/Accounts.css';
import {token} from "../../auth/http";
import SockJS from "sockjs-client";
import Stomp from 'stompjs';
import {connect} from "react-redux";


class RegisterPage extends React.Component {

    constructor() {
        super();
        this.state = {
            email: '',
            password: ''
        };
        this.handleRedirect = this.handleRedirect.bind(this);
        this.registerSuccessAlert = this.registerSuccessAlert.bind(this);
    }

    onChange(field, val) {
        this.setState({[field]: val});
    }

    onSubmit(e) {
        e.preventDefault();
        this.props.onSubmit({
            email: this.state.email,
            password: this.state.password,
            redirect: this.handleRedirect
        });
    }

    handleRedirect(url) {

        this.props.history.push(url);
        var socket = new SockJS('http://localhost:8090/calculations?Authorization=Bearer ' + token);
        let stompClient = Stomp.over(socket);
        stompClient.connect({}, function (frame) {
            console.log('Connected: ' + frame);
            stompClient.subscribe('/user/queue/calculations', this.fun);
        });
        this.props.onStomp(stompClient);
        this.loginSuccessAlert();
        this.registerSuccessAlert();
    }

    registerSuccessAlert() {
        const msg = 'Welcome ' + this.state.email;
        const title = 'Registration Succes';
        const timeOutMs = 1700;
        NotificationManager.success(msg, title, timeOutMs);
    }

    render() {
        return (
            <Page title="Register">
                <form onSubmit={(e)=>{e.preventDefault(e)}} className={" input-container pt-4"}>
                    <UserFormGroup>
                        <IconInputGroup icon="fas fa-envelope">
                            <input type="email"
                                   className="form-control"
                                   id="userEmail"
                                   placeholder="Email"
                                   name="email"
                                   onChange={e => this.onChange('email', e.target.value)}
                                   disabled={this.props.disabled}
                            />
                        </IconInputGroup>
                    </UserFormGroup>
                    <UserFormGroup>
                        <IconInputGroup icon="fas fa-lock">
                            <input type="password"
                                   className="form-control"
                                   id="inputPasswordRegister"
                                   placeholder="Password"
                                   name="password"
                                   onChange={e => this.onChange('password', e.target.value)}
                                   disabled={this.props.disabled}
                            />
                        </IconInputGroup>
                    </UserFormGroup>
                    <UserFormGroup>
                        <IconInputGroup icon="fas fa-lock">
                            <input type="password"
                                   className="form-control"
                                   id="confirmPasswordRegister"
                                   placeholder="Confirm Password"
                            />
                        </IconInputGroup>
                    </UserFormGroup>


                    <UserFormGroup>
                        <button type="submit"
                                className="btn btn-primary mt-2"
                                onClick={e => this.onSubmit(e)}
                                disabled={this.props.disabled}>Register
                        </button>
                    </UserFormGroup>
                    <UserFormGroup>
                        <div className={'text-muted '}>
                        Alread have an account? <NavLink className=" dropdown-item" to="/login">Sign in</NavLink>
                        </div>
                    </UserFormGroup>
                </form>
            </Page>
        );
    }
}

const mapStateToProps = (state) => ({
    stompClient: state.socket.stompClient,
});


RegisterPage = connect(
    mapStateToProps,
    null
)(RegisterPage);

export default withRouter(RegisterPage);