import React from 'react';
import {connect} from 'react-redux';
import API from '../../config/api';

import {setStateName} from '../../actions/redistrictingActions';
import {setDisplayState, STATE} from "../../actions/mapDisplayActions";

import IconInputGroup from '../component/IconInputGroup';
import {fetchGeoJson} from "../../actions/geoJsonActions";

const STATE_NAMES = 'states/all/names';
const STATE_URL = 'states/all/geometry';

class StateSelect extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            options: [],
        };
        this.handleChange = this.handleChange.bind(this);
        this.setStateSelectOptions = this.setStateSelectOptions.bind(this);
        this.getSelectOption = this.getSelectOption.bind(this);
    }

    componentDidMount() {
        API.get(STATE_NAMES)
            .then(res => {
                let allStatesObject = res.data;
                this.setStateSelectOptions(allStatesObject);
            })
            .catch(error => {
                console.log('Get Map Data Error: ' + error.response);
            });
    }

    setStateSelectOptions(allStatesObject) {
        let stateNames = Object.keys(allStatesObject);
        let defaultOption = {value: '', label: 'Choose a state', key: ''};
        let stateOptions = [defaultOption];
        for (var s of stateNames) {
            let stateOptionObject = {
                value: s,
                label: s + ' (' +  allStatesObject[s].toString() + ')',
                key: s + allStatesObject[s].toString(),
            };
            stateOptions.push(stateOptionObject);
        }
        this.setState({
            options: stateOptions
        });
    }



    handleChange(e) {
        e.preventDefault();
        const target = e.target;
        const value = target.value;
        this.props.setRedistrictingState(value);
        this.props.setDisplayState(value);
        this.props.onSetGeoJson(this.props.displayType, STATE_URL, value, 2010);
    }

    getSelectOption(option) {
        if (this.props.selectedState === '' && option.value === '') {
            return <option value={option.value} key={option.key} disabled={true} selected={true}> {option.label} </option>;
        } else if (this.props.selectedState === option.value) {
            return <option value={option.value} key={option.key} selected={true}> {option.label} </option>;
        } else if (option.value === '') {
            return <option value={option.value} key={option.key} disabled={true} selected={true}> {option.label} </option>;
        } else {
            return <option value={option.value} key={option.key}> {option.label} </option>;
        }
    }

    render() {
        if (this.state.options.length === 0) {
            /* Loading render ...*/
            return (
                <IconInputGroup icon="fas fa-map-marker-alt">
                    <select className="form-control pt-2 pl-3 ">
                        <option value={'loading'} key={'Loading'}> Choose a state</option>
                    </select>
                </IconInputGroup>
            );
        } else {
            /*  Render real UI ... */
            return (
                <IconInputGroup icon="fas fa-map-marker-alt">
                    <select className="form-control pt-2 pl-3 " onChange={this.handleChange}>
                        {
                            this.state.options.map((option) => {
                                return this.getSelectOption(option)
                            })
                        }
                    </select>
                </IconInputGroup>
            );
        }
    }
}


const mapStateToProps = (state) => ({
    selectedState: state.redistricting.stateName,
    displayType: state.mapDisplay.displayType,
});

const mapDispatchToProps = (dispatch) => ({
    setRedistrictingState: (value) => dispatch(setStateName(value)),
    setDisplayState: (value) => dispatch(setDisplayState(value)),
    onSetGeoJson: (gt, url, stateName, year) => dispatch(fetchGeoJson(gt, url, stateName, year)),
});

StateSelect = connect(
    mapStateToProps,
    mapDispatchToProps
)(StateSelect);

export default StateSelect;
