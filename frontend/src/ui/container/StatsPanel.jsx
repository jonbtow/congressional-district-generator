import React from 'react';
import {connect} from "react-redux";

import ReactTable from "react-table";

import '../component/style/RedistrictingComponent.css';


class StatsPanel extends React.Component {

    shouldComponentUpdate(nextProps, nextState) {
        if (this.props.geoJsonUpdate !== nextProps.geoJsonUpdate) {
            return true;
        }
        return false;
    }

    render() {
        let og = [];
        let n = [];

        if (this.props.geoJsonUpdate && this.props.geoJsonUpdate.messageType == 'DONE') {
            og = this.props.geoJsonUpdate.originalFipsToScoreMap != undefined ? Object.keys(this.props.geoJsonUpdate.originalFipsToScoreMap).map(key =>
                <tr key={Math.random()}>
                    <td key={Math.random()}  className="">{key}</td>
                    <td key={Math.random()}  className="">{this.props.geoJsonUpdate.originalFipsToScoreMap[key]}</td>
                </tr>) : <tr><td></td><td></td></tr>;

                n = this.props.geoJsonUpdate.districtFipsToScoreMap != undefined ? Object.keys(this.props.geoJsonUpdate.districtFipsToScoreMap).map(key =>
                    <tr key={Math.random()}>
                        <td  key={Math.random()}  className="">{key}</td>
                        <td  key={Math.random()}  className="">{this.props.geoJsonUpdate.districtFipsToScoreMap[key]}</td>
                    </tr>) : <tr><td></td><td></td></tr>;

        }
        let ds = this.props.geoJsonUpdate.districtFipsToScoreMap != undefined ? Object.keys(this.props.geoJsonUpdate.districtFipsToScoreMap).map(key =>
            <tr key={Math.random()}>
                <td className="">{key}</td>
                <td className="">{this.props.geoJsonUpdate.districtFipsToScoreMap[key]}</td>
            </tr>) :  <tr><td></td><td></td></tr>;
        return (
            <div className="">
                <div className={"row "}>
                    <div className={"pt-3 pl-4 col-10"}>
                        Statistics
                    </div>
                    <div className={"col-2"}>
                        <button type="button"
                                className="btn collapse-btn bottom-border"
                                data-toggle="collapse"
                                data-target="#stats"
                                aria-expanded="false"
                                aria-controls="stats"
                                disabled={this.props.isLoggedIn}
                        >
                            <i className="fas fa-ellipsis-v"></i>
                        </button>
                    </div>
                </div>
                <div className="collapse" id="stats">
                    <div className="form-group p-0 mt-1" style={{borderRadius: 4}}>
                        <div className={"card"}>
                        <table className="table table-light text-left">
                            <thead>
                            <tr>
                                <th className="w-25" style={{color: '#3D88EC'}}>Algorithm</th>
                                <th className="w-75"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr key={Math.random()}>
                                <td key={Math.random()}>Status</td>
                                <td key={Math.random()} className={"align-middle"}>
                                    { this.props.geoJsonUpdate.messageType === 'LOADING' ?
                                        <div>
                                        <div className="my-spinner">
                                            <div className="rect1"></div>
                                            <div className="rect2"></div>
                                            <div className="rect3"></div>
                                            <div className="rect4"></div>
                                            <div className="rect5"></div>
                                        </div>
                                        </div>
                                        : this.props.geoJsonUpdate.messageType}
                                    </td>
                            </tr>
                            <tr >
                                <td key={Math.random()} className="">Unmovable Districts</td>
                                <td key={Math.random()} className="">{this.props.redistricting.unmovableDistricts}</td>
                            </tr>
                            <tr >
                                <td key={Math.random()} className="">Unmovable Precincts</td>
                                <td key={Math.random()}  className="">{this.props.redistricting.unmovablePrecincts}</td>
                            </tr>

                            {ds[0]}
                            </tbody>
                        </table>
                        </div>

                        <div className={"card mt-3"}>
                            <table className="table table-light text-left">
                                <thead>
                                <tr key={Math.random()} >
                                    <th key={Math.random()}  className="w-25" style={{color: '#3D88EC'}}>Old District</th>
                                    <th key={Math.random()}  className="w-75">Cost</th>
                                </tr>
                                </thead>
                                <tbody>
                                {og}
                                </tbody>
                            </table>
                        </div>

                        <div className={"card mt-3"}>
                            <table className="table table-light text-left">
                                <thead>
                                <tr>
                                    <th className="w-25" style={{color: '#3D88EC'}}>Redistrict</th>
                                    <th className="w-75">Cost</th>
                                </tr>
                                </thead>
                                <tbody>
                                {n}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


const mapStateToProps = (state) => ({
    geoJsonUpdate: state.socket.geoJsonUpdate,
    redistricting: state.redistricting,
});

const mapDispatchToProps = (dispatch) => ({
});

StatsPanel = connect(
    mapStateToProps,
    mapDispatchToProps
)(StatsPanel);

export default StatsPanel;
